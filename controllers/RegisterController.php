<?php
use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;
class RegisterController extends ControllerBase
{
	public function initialize()
    {
        $random = rand(0,99999);
    	$this->assets
    	//BEGIN GLOBAL MANDATORY STYLES//
        	->addCss('public/assets/global/plugins/font-awesome/css/font-awesome.min.css')
        	->addCss('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')
					->addCss('public/assets/global/plugins/bootstrap/css/bootstrap.min.css')
        	->addCss('public/assets/global/plugins/uniform/css/uniform.default.css')
        	->addCss('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')
        //END GLOBAL MANDATORY STYLES//
        //BEGIN PAGE LEVEL PLUGINS//
        	->addCss('public/assets/global/plugins/select2/css/select2.min.css')
        	->addCss('public/assets/global/plugins/select2/css/select2-bootstrap.min.css')
            ->addCss('public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')
            ->addCss('public/assets/global/plugins/typeahead/typeahead.css')
            ->addCss('public/assets/global/plugins/select2/css/select2.min.css')
            ->addCss('public/assets/global/plugins/select2/css/select2-bootstrap.min.css')
            ->addCss('public/assets/global/plugins/bootstrap-select/css/bootstrap-select.css')
            ->addCss('public/assets/global/plugins/jquery-multi-select/css/multi-select.css')
        //END PAGE LEVEL PLUGINS//
        //BEGIN THEME GLOBAL STYLES //
        	->addCss('public/assets/global/css/components.min.css')
        	->addCss('public/assets/global/css/plugins.min.css')
        //END THEME GLOBAL STYLES//
        //BEGIN PAGE LEVEL STYLES//
        	->addCss('public/assets/pages/css/login.min.css')
            ->addCss('public/assets/pages/css/layout.min.css')
            ->addCss('public/assets/pages/css/custom.min.css');
        //END PAGE LEVEL STYLES//
        //BEGIN AUTO COMPLETE STYLES//
            // ->addCss('public/awesomplete/prism/prism.css')
            // ->addCss('public/awesomplete/awesomplete.css')
            // ->addCss('public/awesomplete/stylecss');
        //END AUTO COMPLETE STYLES//


        $this->assets
        //Config Project//
            ->addJs('public/js/config.js')
        //End Config Project//
        //BEGIN CORE PLUGINS//
        	->addJs('public/assets/global/plugins/jquery.min.js')
        	->addJs('public/assets/global/plugins/bootstrap/js/bootstrap.min.js')
        	->addJs('public/assets/global/plugins/js.cookie.min.js')
        	->addJs('public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')
        	->addJs('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')
        	->addJs('public/assets/global/plugins/jquery.blockui.min.js')
        	->addJs('public/assets/global/plugins/uniform/jquery.uniform.min.js')
        	->addJs('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')
        //END CORE PLUGINS//
        //<!-- BEGIN PAGE LEVEL PLUGINS -->
       		->addJs('public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')
       		->addJs('public/assets/global/plugins/jquery-validation/js/additional-methods.min.js')
       		->addJs('public/assets/global/plugins/select2/js/select2.full.min.js')
            ->addJs('public/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')
            ->addJs('public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')
            ->addJs('public/assets/global/plugins/bootstrap-typeahead/bootstrap3-typeahead.min.js')
            ->addJs('public/assets/global/plugins/typeahead/handlebars.min.js?'.$random)
            ->addJs('public/assets/global/plugins/typeahead/typeahead.bundle.min.js?'.$random)
            ->addJs('public/assets/global/plugins/select2/js/select2.full.min.js?'.$random)
            ->addJs('public/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')
            ->addJs('public/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')
        //<!-- END PAGE LEVEL PLUGINS -->
        // <!-- BEGIN THEME GLOBAL SCRIPTS -->
        	->addJs('public/assets/global/scripts/app.min.js')
        // <!-- END THEME GLOBAL SCRIPTS -->
        // <!-- BEGIN PAGE LEVEL SCRIPTS -->
            ->addJs('public/assets/pages/scripts/form-wizard.js?'.$random)
        	->addJs('public/assets/pages/scripts/login.min.js')
            ->addJs('public/assets/pages/scripts/components-typeahead.js?'.$random)
            ->addJs('public/assets/pages/scripts/components-bootstrap-tagsinput.js?'.$random)
            ->addJs('public/assets/pages/scripts/components-select2.min.js?'.$random)
            ->addJs('public/assets/pages/scripts/components-bootstrap-select.min.js')
            ->addJs('public/assets/pages/scripts/components-multi-select.min.js')
        // <!-- END PAGE LEVEL SCRIPTS -->
        // <!-- BEGIN THEME LAYOUT SCRIPTS --> //
            ->addJs('public/assets/layouts/layout/scripts/layout.min.js')
            ->addJs('public/assets/layouts/layout/scripts/demo.min.js')
            ->addJs('public/assets/layouts/global/scripts/quick-sidebar.min.js');
        // <!-- END THEME LAYOUT SCRIPTS --> //
        // <!-- BEGIN Angular --> //
            // ->addJs('public/js/angular/angular.min.js')
            // ->addJs('public/js/angular/angular-route.min.js')
            // ->addJs('public/js/angular/angular-sanitize.min.js')
            // ->addJs('public/js/register/script.js');
        // <!-- END Angular --> //
        //BEGIN AUTO COMPLETE SCRIPTS//
            // ->addJs('public/awesomplete/awesomplete.js')
            // ->addJs('public/awesomplete/index.js')
            // ->addJs('public/awesomplete/prism/prism.js');
        //END AUTO COMPLETE SCRIPTS//
        $facebookUid = $this->session->get("facebookUid");
        if($facebookUid == Null)
        {
            $this->response->redirect('');
        }
        $userData = $this->session->get("userData");
        if(count($userData) != 0)
        {
            $this->response->redirect('main');
        }
        $this->view->baseUrl = $this->baseUrl();
    }

    public function indexAction()
    {
    	$this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
    	$this->view->getTitle = "Register";
      $this->view->logo_locate = "public/img/eng_logo.png";
      $studentId = $this->session->get("studentId");
	    $init = 2535;
	    $selectyear = [];
	    $current = date("Y") + 543;
	    for($count = $init; $count < $current; $count++) {
	   	   array_push($selectyear,$count);
	       }
			$this->view->selyear = $selectyear;
      $year = substr($studentId,0,2);

        $firebase = $this->connect_firebase();
        //alumnus Data
        $alumniData = (array)json_decode($firebase->get('alumnus/'.$year,array('print' => 'pretty','orderBy' => '"studentId"','equalTo' => '"'.$studentId.'"')));
        foreach ($alumniData as $key => $value) {
            $this->view->fname = $value->fname;
            $this->view->lname = $value->lname;
            $this->view->gender = $value->gender;
            $this->view->entryYear = $value->year;
        }
        //Get Job
        $listJob = json_decode($firebase->get("jobs",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
        $listJobs = array();
        foreach ($listJob as $key => $value) {
            array_push($listJobs, array("key" => $key, "name" => $value->name));
        }
        //Get Company Work
        $listCompanyWork = json_decode($firebase->get("companyWorks",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
        $listCompanyWorks = array();
        foreach ($listCompanyWork as $key => $value) {
            array_push($listCompanyWorks, array("key" => $key, "name" => $value->name));
        }
        $this->view->listJobs = $listJobs;
        $this->view->listCompanyWorks = $listCompanyWorks;
        $advisor = $this->getAdvisorList();
        $this->view->advisor = $advisor;


    }
    public function addUserAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $firebase = $this->connect_firebase();
        $post = $this->request->getPost();
        $lastUpdate = time();
        //Get Facebook UID
        $facebookUid = $this->session->get("facebookUid");

        //Account
        $studentId = $this->session->get("studentId");
        $uid = hash('sha256', $studentId);
        // $password = $post['password'];
        // $salt = $this->generateRandomString(40);
        // $newPassword = $this->hash_salt_generate($password,$salt);
        $stdyear = substr($studentId,0,2); //new
        $engGear = (int)substr($studentId,0,2)-12;
        if($post['alumniType'] == "CPE")
        {
            $gen = $engGear - 22;
        }else if($post['alumniType'] == "ISNE")
        {
            $gen = $engGear - 43;
        }else if($post['alumniType'] == "MCPE")
        {
            $gen = "ยังไม่ได้ระบุ";
        }else if ($post['alumniType'] == "PHDCPE")
        {
            $gen = "ยังไม่ได้ระบุ";
        }else
        {
            $gen = "ยังไม่ได้ระบุ";
        }
        //Profile
        $advisor = $post['advisor'];
        $address = $post['address'];
        $city = $post['city'];
        $companyName = $post['companyName'];
        $email = $post['email'];
        $entryYear = $post['entryYear'];
        $fname = $post['fname'];
        $efname = $post['efname'];
        $gender = $post['gender'];
        $graduateYear = $post['graduateYear'];
        $lname = $post['lname'];
        $elname = $post['elname'];
        $nickname = $post['nickname'];
        $phone = $post['phone'];
        $alumniType = $post['alumniType'];

        //Company Profile
        $companyAddress = $post['companyAddress'];
        $companyCity = $post['companyCity'];
        $companyPhone = $post['companyPhone'];
        $companyWebsite = $post['companyWebsite'];
        $companyFacebook = $post['companyfbook'];
        $companyWork = $post['companyWork'];
        $userWork = $post['userWork'];
        $startWork = $post['startWork'];
        $endWork = $post['endWork'];

        //Other
        if($post['OtherUserWork'] != ""){
            $OtherUserWork = $post['OtherUserWork'];
             $firebase->push("other/jobs",array("name" => $OtherUserWork));
        }
        if($post['OtherCompanyWork'] != ""){
            $OtherCompanyWork = $post['OtherCompanyWork'];
            $firebase->push("other/companyWorks",array("name" => $OtherCompanyWork));
        }

        //Account Data
        $accountData = array(
                "studentId" => $studentId,
                "advisor" => $advisor,
                "address" => $address,
                "city" => $city,
                "email" => $email,
                "entryYear" => $entryYear,
                "fname" => $fname,
                "efname" => $efname,
                "nickname" => $nickname,
                "gender" => $gender,
                "graduateYear" => $graduateYear,
                "lname" => $lname,
                "elname" => $elname,
                "phone" => $phone,
                "lastUpdate" => $lastUpdate,
                "alumniType" => $alumniType,
                "gen" => $gen,
                "facebookUid" => $facebookUid,
                "disable" => true,
                "isRegistered" => true, //new
                "imgProfile" => "https://firebasestorage.googleapis.com/v0/b/cpe-alumni.appspot.com/o/Pictures%2Fdefault%2Fasd78f8adsfsfas8d7.png?alt=media&token=cdb98aff-f36c-4562-89d8-d2325551578a"
            );

        //Add User in Firebase

        $firebase->set('alumnus/'.$stdyear.'/'.$uid, $accountData); //change
        //FacebookAccount
        $facebookData = array(
            "facebookUid" => $facebookUid,
            "studentId" => $studentId
            );
        $firebase->set('facebookAccount/'.$uid, $facebookData); //change

        //Add Education
        $firebase->push('alumnus/'.$stdyear.'/'.$uid."/education",array( //change
                    "course" => "ตรี",
                    "major" => "วิศวกรรมคอมพิวเตอร์",
                    "university" => "มหาวิทยาลัยเชียงใหม่",
                    "startStudy" => $entryYear,
                    "endStudy" => $graduateYear
                ));

        //Check Company
        $companyToCheck = (array)json_decode($firebase->get("companies/", array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$companyName.'"')));
        if(count($companyToCheck)==0)
        {
            //Company Data
            $companyData = array(
                "address" => $companyAddress,
                "city" => $companyCity,
                "name" => $companyName,
                "phone" => $companyPhone,
                "website" => $companyWebsite,
                "facebook" => $companyFacebook,
                "work" => $companyWork,
                "lastUpdate" => $lastUpdate
            );
            //Add Company in Firebase
            $companyId = json_decode($firebase->push('companies/', $companyData))->name;
            $firebase->push('alumnus/'.$stdyear.'/'.$uid."/companyWork",array("companyId" => $companyId,"companyName" => $companyName,"userWork" => $userWork,"startWork" => $startWork,"endWork" => $endWork));
        }else   //change-----------^
        {
            foreach ($companyToCheck as $key => $value) {
                $firebase->push('alumnus/'.$stdyear.'/'.$uid."/companyWork",array("companyId" => $key,"companyName" => $companyName,"userWork" => $userWork,"startWork" => $startWork,"endWork" => $endWork));
            }   //change-----------^
        }
        $userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid)); //change
        $this->session->set("userData",$userData);


        return $this->response->redirect('main/home');

    }
    public function checkStudentIdAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $firebase = $this->connect_firebase();

        $studentId = $post['studentId'];
        $stdyear = substr($studentId, 0,2);
        $studentIdData = json_decode($firebase->get("alumnus/".$stdyear,array('print' => 'pretty','orderBy' => '"studentId"','equalTo' => '"'.$studentId.'"')));
        foreach ($studentIdData as $key => $value) {
            echo json_encode($value);
        }
    }
    public function checkCompanyAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $firebase = $this->connect_firebase();

        $companyName = $post['name'];
        $companData = json_decode($firebase->get("companies/", array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$companyName.'"')));
        foreach ($companData as $key => $value) {
            echo json_encode($value);
        }
        // echo $companyName;

    }

    public function listCompanyAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $firebase = $this->connect_firebase();
        $listCompany = array();
        $companData = json_decode($firebase->get("companies/", array('print' => 'pretty')));
        foreach ($companData as $key => $value) {
            array_push($listCompany, $value->name);
        }
        echo json_encode($listCompany);
    }
    public function listCompanyWorkAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $firebase = $this->connect_firebase();
        $listCompanyWork = array();
        $companData = json_decode($firebase->get("companyWorks/", array('print' => 'pretty')));
        foreach ($companData as $key => $value) {
            array_push($listCompanyWork, array("key"=>$key,"name"=>$value->name));
        }
        echo json_encode($listCompanyWork);
    }


    public function addCompanyWorkAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $firebase = $this->connect_firebase();
        $list = array("name"=>"Other");
        $firebase->push("companyWorks",$list);
    }
    public function addJobAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $firebase = $this->connect_firebase();
        $list = array("name"=>"Other");
        // $firebase->push("jobs",$list);
    }



    public function testAction()
    {
        $firebase = $this->connect_firebase();
        $dir = dirname(__FILE__);
        $file = fopen($dir."/alumni_cpe_test01.csv","r");
        $i = 0;
        $alumnis = array();
        while(! feof($file))
        {
            $data = fgetcsv($file);
            $student_id = $data[0];
            $year = substr($student_id,0,2);
            $fname = $data[1];
            $lname = $data[2];
            $gender = $data[4];
            echo $student_id." : ".$fname." ".$lname." เพศ ".$gender." : ".$year."<br>";
            $profile = array("studentId" => $student_id,"fname" => $fname,"lname" => $lname,"gender" => $gender,"year" => "25".$year);
            // $firebase->push('alumnis/'.$year, $profile);

        }
        fclose($file);

    }

}
