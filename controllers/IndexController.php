<?php
use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;
class IndexController extends ControllerBase
{
	public function initialize()
    {

    	$this->assets
    	//BEGIN GLOBAL MANDATORY STYLES//
        	->addCss('public/assets/global/plugins/font-awesome/css/font-awesome.min.css')
        	->addCss('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')
        	->addCss('public/assets/global/plugins/bootstrap/css/bootstrap.min.css')
        	->addCss('public/assets/global/plugins/uniform/css/uniform.default.css')
        	->addCss('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')
        //END GLOBAL MANDATORY STYLES//
        //BEGIN PAGE LEVEL PLUGINS//
        	->addCss('public/assets/global/plugins/select2/css/select2.min.css')
        	->addCss('public/assets/global/plugins/select2/css/select2-bootstrap.min.css')
        //END PAGE LEVEL PLUGINS//
        //BEGIN THEME GLOBAL STYLES //
        	->addCss('public/assets/global/css/components.min.css')
        	->addCss('public/assets/global/css/plugins.min.css')
        //END THEME GLOBAL STYLES//
        //BEGIN PAGE LEVEL STYLES//
        	->addCss('public/assets/pages/css/login.min.css');
        //END PAGE LEVEL STYLES//


        $this->assets
        //BEGIN CORE PLUGINS//
        	->addJs('public/assets/global/plugins/jquery.min.js')
        	->addJs('public/assets/global/plugins/bootstrap/js/bootstrap.min.js')
        	->addJs('public/assets/global/plugins/js.cookie.min.js')
        	->addJs('public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')
        	->addJs('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')
        	->addJs('public/assets/global/plugins/jquery.blockui.min.js')
        	->addJs('public/assets/global/plugins/uniform/jquery.uniform.min.js')
        	->addJs('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')
        //END CORE PLUGINS//
        //<!-- BEGIN PAGE LEVEL PLUGINS -->
       		->addJs('public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')
       		->addJs('public/assets/global/plugins/jquery-validation/js/additional-methods.min.js')
       		->addJs('public/assets/global/plugins/select2/js/select2.full.min.js')
        //<!-- END PAGE LEVEL PLUGINS -->
        // <!-- BEGIN THEME GLOBAL SCRIPTS -->
        	->addJs('public/assets/global/scripts/app.min.js')
        // <!-- END THEME GLOBAL SCRIPTS -->
        // <!-- BEGIN PAGE LEVEL SCRIPTS -->
        	->addJs('public/assets/pages/scripts/login.min.js');
        // <!-- END PAGE LEVEL SCRIPTS -->



    }

    public function indexAction()
    {
    	$this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $this->view->getTitle = "Welcome";
        $this->view->logo_locate = "public/img/eng_logo.png";
        $this->view->LoginTextError = $this->session->get("LoginTextError");
        if($this->session->get("userData") == Null)
        {

        }else
        {
            return $this->response->redirect('main/home');
        }

    }

		/* for handling 404 not found */
		public function errorAction()
		{
			 $this->view->webtitle = "ไม่พบหน้าที่ต้องการ ";
			 $this->assets
			 ->addCss('public/css/tableconfig.css')
			 ->addCss('public/assets/pages/css/error.min.css');
		}


}
