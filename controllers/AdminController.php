<?php
use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Url;

class AdminController extends ControllerBase {

  	function initialize()
  	{
    	$this->view->setTemplateAfter('adminnavbar');
    	$this->assets
		/*<!-- BEGIN GLOBAL MANDATORY STYLES -->*/
		->addCss('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all')
		->addCss('public/assets/global/plugins/font-awesome/css/font-awesome.min.css')
		->addCss('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')
		->addCss('public/assets/global/plugins/bootstrap/css/bootstrap.min.css')
		->addCss('public/assets/global/plugins/uniform/css/uniform.default.css')
		->addCss('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')
		/*<!-- END GLOBAL MANDATORY STYLES -->*/
		/*<!-- BEGIN PAGE LEVEL PLUGINS -->*/
		->addCss('public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')
		->addCss('public/assets/global/plugins/morris/morris.css')
		->addCss('public/assets/global/plugins/fullcalendar/fullcalendar.min.css')
		->addCss('public/assets/global/plugins/jqvmap/jqvmap/jqvmap.css')
		->addCss('public/assets/global/plugins/bootstrap-select/css/bootstrap-select.css')
        ->addCss('public/assets/global/plugins/jquery-multi-select/css/multi-select.css')
        ->addCss('public/assets/global/plugins/typeahead/typeahead.css')
		/*<!-- END PAGE LEVEL PLUGINS -->*/
		/*<!-- BEGIN THEME GLOBAL STYLES -->*/
		->addCss('public/assets/global/css/components.min.css')
		->addCss('public/assets/global/css/plugins.min.css')
		/*<!-- END THEME GLOBAL STYLES -->*/
		/*<!-- BEGIN THEME LAYOUT STYLES -->*/
		->addCss('public/assets/layouts/layout3/css/layout.min.css')
		->addCss('public/assets/layouts/layout3/css/themes/default.min.css')
		->addCss('public/assets/layouts/layout3/css/custom.min.css');
		/*<!-- END THEME LAYOUT STYLES -->*/

		$this->assets
		//Config Project//
		->addJs('public/js/config.js')
		//End Config Project//
		//<!-- BEGIN CORE PLUGINS -->
		->addJs('public/assets/global/plugins/jquery.min.js')
		->addJs('public/assets/global/plugins/bootstrap/js/bootstrap.min.js')
		->addJs('public/assets/global/plugins/js.cookie.min.js')
		->addJs('public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')
		->addJs('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')
		->addJs('public/assets/global/plugins/jquery.blockui.min.js')
		->addJs('public/assets/global/plugins/uniform/jquery.uniform.min.js')
		->addJs('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')
		//<!-- END CORE PLUGINS -->
		//<!-- BEGIN PAGE LEVEL PLUGINS -->
		->addJs('public/assets/global/plugins/moment.min.js')
		->addJs('public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')
		->addJs('public/assets/global/plugins/morris/morris.min.js')
		->addJs('public/assets/global/plugins/morris/raphael-min.js')
		->addJs('public/assets/global/plugins/counterup/jquery.waypoints.min.js')
		->addJs('public/assets/global/plugins/counterup/jquery.counterup.min.js')
		->addJs('public/assets/global/plugins/fullcalendar/fullcalendar.min.js')
		->addJs('public/assets/global/plugins/flot/jquery.flot.min.js')
		->addJs('public/assets/global/plugins/flot/jquery.flot.resize.min.js')
		->addJs('public/assets/global/plugins/flot/jquery.flot.categories.min.js')
		->addJs('public/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')
		->addJs('public/assets/global/plugins/jquery.sparkline.min.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')
		->addJs('public/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')
    	->addJs('public/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')
    	->addJs('public/assets/global/plugins/typeahead/handlebars.min.js')
		->addJs('public/assets/global/plugins/typeahead/typeahead.bundle.min.js')
		//<!-- END PAGE LEVEL PLUGINS -->
		//<!-- BEGIN THEME GLOBAL SCRIPTS -->
		->addJs('public/assets/global/scripts/app.min.js')
		//<!-- END THEME GLOBAL SCRIPTS -->
		//<!-- BEGIN PAGE LEVEL SCRIPTS -->
		->addJs('public/assets/pages/scripts/dashboard.min.js')
		->addJs('public/assets/pages/scripts/components-typeahead.js')
		//<!-- END PAGE LEVEL SCRIPTS -->
		//<!-- BEGIN THEME LAYOUT SCRIPTS -->
		->addJs('public/assets/layouts/layout3/scripts/layout.min.js')
		->addJs('public/assets/layouts/layout3/scripts/demo.min.js')
		->addJs('public/assets/layouts/global/scripts/quick-sidebar.min.js');
		//<!-- END THEME LAYOUT SCRIPTS -->
		$adminData = $this->session->get("adminData");

		if(count($adminData) == 0)
		{
			$this->response->redirect('');
		}
		$this->view->username = $adminData->username;
		$this->view->baseUrl = $this->baseUrl();
		//asdadasdadasdasd
		//asdasdasdasdasdad
  }

  function indexAction()
	{
		$this->view->disable();
		$this->response->redirect('admin/dashboard');
	}


  	public function dashboardAction()
	{
		$this->view->webtitle = "สถิติ";
		$t = time();
		$rand =  rand(0,$t);

		$this->assets
		->addJs('public/js/highchart/code/highcharts.src.js')
		->addJs('public/js/chartfile-h.js')
		->addJs('public/js/alumnusstat.js');

		$firebase = $this->connect_firebase();

		/*get amount of alumni*/
    	$bd_cpe = 0; $bd_cpes = 0;$md_cpe = 0; $phd_cpe = 0;$bd_isne = 0;
    	$i = 0;
		$alumnuslist = array();
		$jobsfield = array();
		$regionData = array();
    	$alluser = 0;
    	$allstd = 0;
    	$userDisbled = 0;
   	 	$usersData = json_decode($firebase->get('alumnus/'));
   	 	foreach ($usersData as $key => $value) {
   	 	  $gradstd[$i] = new StdClass();
   	 	  array_push($alumnuslist, $value);
   	 	  //echo "CPE : ".($i+1)." / Student : ".count((array)$alumnuslist[$i]);
   	 	  $gradstd[$i]->year = ($i+2535);
   	 	  $gradstd[$i]->cpe = count((array)$alumnuslist[$i]);
   	 	  $gradstd[$i]->isne = 0;

   	 	  $i++;
   	 	  //var_dump($value);
   	 	  foreach ($value as $key => $stdinfo) {
   	 	    $year = substr($stdinfo->studentId,0,2);
   	 	    if($year > 48)
   	 	    {
   	 	      $degree  = substr($stdinfo->studentId,3,2);
   	 	      if($degree == 61) {
   	 	        $id = substr($stdinfo->studentId,5,1);
   	 	        if($id == 0)        $bd_cpe++;
   	 	        else if($id == 1)   $bd_isne++;
   	 	        else                $bd_cpes++;
   	 	      }
   	 	      else if($degree == 63)  $md_cpe++;
   	 	      else                    $phd_cpe++;
   	 	    }else $bd_cpe++;

   	 	    if(count((array)$stdinfo) > 5){
				array_push($regionData, $stdinfo->city);
				$data = $stdinfo->companyWork;
				if(count((array)$data) != 0)
				{
					foreach ($data as $companykey => $value) {
						foreach ($value->userWork as $jobskey => $jobsname) {
							$jobs = json_decode($firebase->get("jobs/".$jobsname, array('print' => 'pretty')));
							array_push($jobsfield, $jobs->name);
						}
					}
				}
				if($stdinfo->isRegistered){
					$alluser++;
				}
				if(!$stdinfo->disable){
					$userDisbled++;
				}
			}
   	 	  }
   	 	}

    	$jobsfield = array_count_values($jobsfield);
    	$regionData = array_count_values($regionData);
    	//get company category
    	//get companyId
   	 	$company = array();
   	 	$companyWorksname = array();
		$companiesData = json_decode($firebase->get("companies/"));
	   	// $std = json_decode($firebase->get("companies/",array('print' => 'pretty')));
   	 	foreach ($companiesData as $key => $value) {
   	 	  array_push($company, $value);
   	 	  foreach ($value->work as $key => $workkey) {
    	    $worksname = json_decode($firebase->get("companyWorks/".$workkey, array('print' => 'pretty')));
    	    array_push($companyWorksname, $worksname->name);
    	  }
   	 	}
	
    	$companyWorksname = array_count_values($companyWorksname);

    	$adminData = json_decode($firebase->get("admins/", array("print" => "pretty")));
    	$admin = count((array)$adminData);

    	$this->view->admin = $admin;
    	$this->view->user = $alluser;
    	$this->view->disable = $userDisbled;

		$this->view->stdamount = json_encode($gradstd);
		$this->view->workfield = json_encode($jobsfield);
		$this->view->region = json_encode($regionData);
		$this->view->companywork = json_encode($companyWorksname);

    	$this->view->bd_cpe = $bd_cpe;
    	$this->view->bd_cpes = $bd_cpes;
    	$this->view->md_cpe = $md_cpe;
    	$this->view->phd_cpe = $phd_cpe;
    	$this->view->bd_isne = $bd_isne;

	}

  // public function getuserAction()
  // {
  //   $firebase = $this->connect_firebase();

  //   $disableduser = 0;

  //   $usersData = json_decode($firebase->get('users/', array("print" => "pretty")));
  //   $alluser = count((array)$usersData);

  //   $adminData = json_decode($firebase->get("admins/", array("print" => "pretty")));
  //   $admin = count((array)$adminData);

  //   foreach ($usersData as $key => $value) {
  //     if($value->disable == false) {
  //       $disableduser++;
  //     }
  //   }

  //   var_dump('admin: '.$admin." / user: ".$alluser." / disabled: ".$disableduser);
  //   exit();
  // }

  /* User Section */
  public function userAction()
  {
    $this->view->webtitle = "รายชื่อผู้ใช้งาน";
    $this->view->topic_header = "รายชื่อผู้ใช้งาน";
    $this->view->tableattr = ["","รหัสนักศึกษา","ชื่อ-นามสกุล","เกียร์","อาจารย์ที่ปรึกษา","สถานที่ทำงาน","ประเภทของงาน","Facebook",""];
    //view user
    $this->assets
		->addCss('public/assets/global/plugins/datatables/datatables.min.css')
		->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
		->addCss('public/css/tableconfig.css');

		$this->assets
		->addJs('public/assets/global/scripts/datatable.js')
		->addJs('public/assets/global/plugins/datatables/datatables.min.js')
		->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
		->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');
    $firebase = $this->connect_firebase();
	$usersRegisted = json_decode($firebase->get('facebookAccount/'));
	$usersData = array();
	foreach ($usersRegisted as $uid => $value) {
		$stdyear = substr($value->studentId,0,2);
		$userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid));
		array_push($usersData, $userData);
	}
	$advisorAll = (array)json_decode($firebase->get("advisor/"));
	$companyWorkAll = (array)json_decode($firebase->get("companies/"));
	$jobAll = (array)json_decode($firebase->get("jobs/"));
	$userDatasView = $this->processToUserDatasView($allUid,$usersData,$advisorAll,$companyWorkAll,$jobAll);



  $this->view->userDatasView = $userDatasView;

  }

  public function useraddAction()
  {
    $this->assets
		->addCss('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')
		->addCss('public/assets/pages/css/profile-2.min.css');

		$this->assets
		->addJs('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')
  	->addJs('http://maps.google.com/maps/api/js?sensor=false')
  	->addJs('public/assets/global/plugins/gmaps/gmaps.min.js');

    $this->view->webtitle = "เพิ่มข้อมูลนักศึกษาเก่า";
    $firebase = $this->connect_firebase();
    $advisor = json_decode($firebase->get("advisor",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
    $this->view->advisor = $advisor;
    //add user data

  }

  public function useraddedAction()
  {
  	$post = $this->request->getPost();
  	$firebase = $this->connect_firebase();
  	$uid = hash('sha256', $post['studentId']);
  	$stdyear = substr($post['studentId'],0,2);
  	$userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid));
  	if($userData->isRegistered == Null)
	{
  		if($post['studentId']== "" || $post['fname'] == "" || $post['lname'] == "" )
  		{
  			$this->view->disableLevel(
    	    	    View::LEVEL_ACTION_VIEW
    	    	);
    	    	echo "<h2>กรุณากรอกรหัสนักศึกษา ชื่อและนามสกุลให้ครบ</h2> <a href='userAdd'>กลับ</a> ";
  		}else
  		{
			foreach ($post as $key => $value) {
				if($key=="fbook")
					$key="facebook";
				$firebase->set('alumnus/'.$stdyear.'/'.$uid."/".$key,$value);
			}
			$firebase->set('alumnus/'.$stdyear.'/'.$uid."/isRegistered",true);
			$firebase->set('alumnus/'.$stdyear.'/'.$uid."/lastUpdate",$lastUpdate);
			$firebase->set('alumnus/'.$stdyear.'/'.$uid."/imgProfile","https://firebasestorage.googleapis.com/v0/b/alumnieng.appspot.com/o/Pictures%2Fasd78f8adsfsfas8d7.png?alt=media&token=050ee0dd-8c77-49b7-913b-7424444a1a65");
			$firebase->set('facebookAccount/'.'/'.$uid,array("studentId" => $post['studentId']));
			$this->addAdminlog("เพิ่มข้อมูลผู้ใช้");

			$this->response->redirect('admin/user');
		}
	}else
	{
		$this->view->webtitle = $post['studentId'];
		$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>รหัสนักศึกษานี้ มีอยู่ในระบบแล้ว</h2> <a href='user'>กลับ</a>";
	}

  }
  public function adminListAction()
  {
  	$this->view->webtitle = "รายชื่อผู้ดูแลระบบทั้งหมด";
  	$this->view->topic_header = "รายชื่อผู้ดูแลระบบทั้งหมด";
  	$this->view->tableattr = ["","ชื่อ-นามสกุล","username",""];
  	    //view user
    $this->assets
		->addCss('public/assets/global/plugins/datatables/datatables.min.css')
		->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
		->addCss('public/css/tableconfig.css');

		$this->assets
		->addJs('public/assets/global/scripts/datatable.js')
		->addJs('public/assets/global/plugins/datatables/datatables.min.js')
		->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
		->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');
	$firebase = $this->connect_firebase();
	$adminData = json_decode($firebase->get("admins"));
	$adminLogData = json_decode($firebase->get("adminLog"));
	foreach ($adminLogData as $key => $value) {
		$fname = json_decode($firebase->get("admins/".$key."/fname"));
		$lname = json_decode($firebase->get("admins/".$key."/lname"));
		$adminLogData->$key->fname = $fname;
		$adminLogData->$key->lname = $lname;
	}
	$this->view->adminData = $adminData;
	$this->view->adminLogData = $adminLogData;
  }

  public function addAdminAction()
  {
  	$this->view->webtitle = "เพิ่มผู้ดูแลระบบ";
  	$this->view->topic_header = "เพิ่มผู้ดูแลระบบ";
  	$this->view->tableattr = ["","ชื่อ-นามสกุล","username",""];
	$firebase = $this->connect_firebase();

  }
  public function addedAdminAction()
  {
	$firebase = $this->connect_firebase();
	$post = $this->request->getPost();
	if($post['fname'] =="" || $post['lname'] =="" || $post['username'] =="" || $post['password']=="" || $post['confirmpassword']=="")
	{
		$this->view->disableLevel(
	 	    View::LEVEL_ACTION_VIEW
	 	);

	 	$this->view->webtitle = "Error";
    if($post['password'] != $post['confirmpassword'])
    {
      echo "<h2>รหัสผ่านที่ยืนยันไม่ตรงกัน</h2>";
    }
	 	echo "<h2>กรุณากรอกข้อมูลให้ครบ</h2>  <a href='addedAdmin'>กลับ</a>";
	}else
	{

		$uid = hash('sha256', $post['username']);
		if(count(json_decode($firebase->get("admins/".$uid))) == 0)
		{

			$password = $post['password'];
        	$salt = $this->generateRandomString(40);
        	$newPassword = $this->hash_salt_generate($password,$salt);
        	$adminData = array(
        		"fname"=>$post['fname'],
        		"lname"=>$post['lname'],
        		"username"=>$post['username'],
        		"password"=>$newPassword,
        		"salt"=>$salt
        		);
			$firebase->set("admins/".$uid,$adminData);
			$this->view->webtitle = "Error";
			$this->addAdminlog("เพิ่ม ".$post['username']." เป็นผู้ดูแลระบบ");
        	$this->response->redirect('admin/adminList');

		}else
		{
			$this->view->disableLevel(
	 	    View::LEVEL_ACTION_VIEW
	 		);
	 		$this->view->webtitle = "Error";
	 		echo "<h2>username ซ้ำ</h2>  <a href='addedAdmin'>กลับ</a>";
		}

	}

  }

  public function adminEditAction()
  {
	$get = $this->request->get();
	$this->view->webtitle = $get['id'];
	$firebase = $this->connect_firebase();
	$uid = $get['id'];
	$adminData = json_decode($firebase->get("admins/".$uid));
	if($adminData == Null)
	{
		$this->view->disableLevel(
		    View::LEVEL_ACTION_VIEW
		);
		echo "<h2>ไม่พบบุคคลที่ท่านค้นหา</h2>";
	}else
	{
		$this->view->webtitle = "แก้ไขข้อมูลของผู้ดูแลระบบ";
		$this->view->topic_header = "แก้ไขข้อมูลของผู้ดูแลระบบ";
		$this->view->fname = $adminData->fname;
		$this->view->lname = $adminData->lname;
		$this->session->set("editAdminId",$uid);
	}

  }

  public function editedAdminAction()
  {
    $firebase = $this->connect_firebase();
  	$post = $this->request->getPost();
  	$uid = $this->session->get("editAdminId");
  	foreach ($post as $key => $value) {
  		$firebase->set("admins/".$uid."/".$key,$value);
  	}
  	$this->addAdminlog("แก้ไขข้อมูลของผู้ดูแลระบบ");
    $this->response->redirect('admin/adminList');

  }

  public function adminDelteAction()
  {
	$get = $this->request->get();
	$this->view->webtitle = $get['id'];
	$firebase = $this->connect_firebase();
	$uid = $get['id'];
	$adminData = json_decode($firebase->get("admins/".$uid));
	if($adminData == Null)
	{
		$this->view->disableLevel(
		    View::LEVEL_ACTION_VIEW
		);
		echo "<h2>ไม่พบบุคคลที่ท่านค้นหา</h2>";
	}else
	{
		$firebase->delete("admins/".$uid);
		$this->addAdminlog("ลบข้อมูลของผู้ดูแลระบบ");
		$this->response->redirect('admin/adminList');
	}

  }

  public function resetPasswordAction()
  {
  	$this->view->webtitle = "รีเช็ตรหัสผ่าน";
  	$this->view->topic_header = "รีเช็ตรหัสผ่าน";

  }

  public function resetPassAction()
  {
  	$firebase = $this->connect_firebase();
	$post = $this->request->getPost();
	if($post['password']=="" || $post['cpassword']=="")
	{
		$this->view->disableLevel(
	 	    View::LEVEL_ACTION_VIEW
	 	);
	 	$this->view->webtitle = "Error";
	 	echo "<h2>กรุณากรอกข้อมูลให้ครบ</h2>  <a href='resetPassword'>กลับ</a>";
	}else
	{

		$adminData = $this->session->get("adminData");
		$uid = hash('sha256', $adminData->username);
		if($post['password'] == $post['cpassword'])
		{

			$password = $post['password'];
        	$salt = $this->generateRandomString(40);
        	$newPassword = $this->hash_salt_generate($password,$salt);

			$firebase->set("admins/".$uid."/password",$newPassword);
			$firebase->set("admins/".$uid."/salt",$salt);

        	$this->response->redirect('admin/dashboard');

		}else
		{
			$this->view->disableLevel(
	 	    View::LEVEL_ACTION_VIEW
	 		);
	 		$this->view->webtitle = "Error";
	 		echo "<h2>รหัสผ่านไม่ตรงกัน</h2>  <a href='resetPassword'>กลับ</a>";
		}

	}

  }
  public function userProfileAction()
  {
  		$get = $this->request->get();
        $this->view->webtitle = $get['studentId'];
        $firebase = $this->connect_firebase();
        $uid = hash('sha256', $get['studentId']);
        $stdyear = substr($get['studentId'],0,2);
		$userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid));

		if(count((array)$userData) <= 5)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบุคคลที่ท่านค้นหา</h2>";
		}else
		{
		$this->assets
		->addCss('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')
		->addCss('public/assets/pages/css/profile-2.min.css');

		$this->assets
		->addJs('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
    	// ->addJs('http://maps.google.com/maps/api/js?sensor=false')
    	// ->addJs('public/assets/global/plugins/gmaps/gmaps.min.js');

    	$engGear = (int)substr($userData->studentId,0,2)-12;
    	$this->view->webtitle = $userData->studentId;
		$this->view->topic = $userData->studentId;
		$this->view->fname = $userData->fname;
		$this->view->lname = $userData->lname;
		$this->view->efname = $userData->efname;
		$this->view->elname = $userData->elname;
		$this->view->nickname = $userData->nickname;
		$this->view->gender = $userData->gender;
		$this->view->address = $userData->address;
		$this->view->city = $userData->city;
		$this->view->phone = $userData->phone;
		$this->view->facebookUid = $userData->facebookUid;
		$this->view->studentId = $userData->studentId;
		$this->view->entryYear = $userData->entryYear;
		$this->view->graduateYear = $userData->graduateYear;
		$this->view->advisor = $userData->advisor;
		$this->view->advisorName = json_decode($firebase->get("advisor/".$userData->advisor."/name"));
		$this->view->imgProfile = $userData->imgProfile;
		$this->view->alumniType = $userData->alumniType;
		$this->view->engGear = $engGear;
    $this->view->gen = $userData->gen;
		if($userData->education == null)
			$this->view->education = array();
		else
			$this->view->education = $userData->education;
		if($userData->certificate == null)
			$this->view->certificate = array();
		else
			$this->view->certificate = $userData->certificate;

		$userJob = array();
		foreach ($userData->companyWork as $key => $value) {

			$companyData = json_decode($firebase->get("companies/".$value->companyId));

			$userWork = array();
			foreach ($value->userWork as $subKey => $subValue) {
				$jobData = json_decode($firebase->get("jobs/".$subValue));
				array_push($userWork, $jobData->name);
			}
			$userJob[$value->endWork] = array("companyName" => $companyData->name,"companyId" => $value->companyId,"startWork" => $value->startWork,"endWork" => $value->endWork,"jobs" => $userWork,"companyProfileKey" => $key);
			// array_push($userJob, array("companyName" => $companyData->name,"companyId" => $value->companyId,"startWork" => $value->startWork,"endWork" => $value->endWork,"jobs" => $userWork,"companyProfileKey" => $key));

		}
		krsort($userJob);
		$this->view->userJob = $userJob;
		}
  }

  public function usereditAction()
  {
  		$get = $this->request->get();
        $this->view->webtitle = $get['studentId'];
        $firebase = $this->connect_firebase();
        $uid = hash('sha256', $get['studentId']);
        $stdyear = substr($get['studentId'],0,2);
		$userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid));
		if($userData == Null)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบุคคลที่ท่านค้นหา</h2>";
		}else
		{
			$this->session->set("editUserProfileId",$get['studentId']);
		$this->assets
		->addCss('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')
		->addCss('public/assets/pages/css/profile-2.min.css')
    ->addCss('public/assets/global/plugins/datatables/datatables.min.css')
		->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
		->addCss('public/css/tableconfig.css');

		$this->assets
		->addJs('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')
    ->addJs('public/assets/global/scripts/datatable.js')
		->addJs('public/assets/global/plugins/datatables/datatables.min.js')
		->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
		->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');
    	// ->addJs('http://maps.google.com/maps/api/js?sensor=false')
    	// ->addJs('public/assets/global/plugins/gmaps/gmaps.min.js');

    	$engGear = (int)substr($userData->studentId,0,2)-12;
  //   	$this->view->webtitle = "Edit Profile";
		// $this->view->topic = "Edit Profile";
		$this->view->fname = $userData->fname;
		$this->view->lname = $userData->lname;
		$this->view->efname = $userData->efname;
		$this->view->elname = $userData->elname;
		$this->view->nickname = $userData->nickname;
    $this->view->email = $userData->email;
		$this->view->gender = $userData->gender;
		$this->view->address = $userData->address;
		$this->view->city = $userData->city;
		$this->view->phone = $userData->phone;
		$this->view->studentId = $userData->studentId;
		$this->view->entryYear = $userData->entryYear;
		$this->view->graduateYear = $userData->graduateYear;
		$this->view->advisor = $userData->advisor;
		$this->view->advisor = $userData->advisor;
		$this->view->advisorName = json_decode($firebase->get("advisor/".$userData->advisor."/name"));
		$this->view->imgProfile = $userData->imgProfile;
		$this->view->alumniType = $userData->alumniType;
		$this->view->engGear = $engGear;
    $this->view->gen = $userData->gen;
		if($userData->education == null)
			$this->view->education = array();
		else
			$this->view->education = $userData->education;
		if($userData->certificate == null)
			$this->view->certificate = array();
		else
			$this->view->certificate = $userData->certificate;

		$userJob = array();
		foreach ($userData->companyWork as $key => $value) {

			$companyData = json_decode($firebase->get("companies/".$value->companyId));

			$userWork = array();
			foreach ($value->userWork as $subKey => $subValue) {
				$jobData = json_decode($firebase->get("jobs/".$subValue));
				array_push($userWork, $jobData->name);
			}
			$userJob[$value->endWork] = array("companyName" => $companyData->name,"companyId" => $value->companyId,"startWork" => $value->startWork,"endWork" => $value->endWork,"jobs" => $userWork,"companyProfileKey" => $key);
			// array_push($userJob, array("companyName" => $companyData->name,"companyId" => $value->companyId,"startWork" => $value->startWork,"endWork" => $value->endWork,"jobs" => $userWork,"companyProfileKey" => $key));

		}
		krsort($userJob);
		$this->view->userJob = $userJob;
		}
		$advisorList = json_decode($firebase->get("advisor",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
    	$this->view->advisorList = $advisorList;
  }

  	function editProfileAction()
	{

		// $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
		$get = $this->request->get();
		$post = $this->request->getPost();
		$this->view->webtitle = $get['studentId'];
		//Profile
        $firebase = $this->connect_firebase();
        $uid = hash('sha256', $get['studentId']);
        $stdyear = substr($get['studentId'],0,2);
		$userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid));
		$lastUpdate = time();
		if(count((array)$userData) <= 5)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบุคคลที่ท่านค้นหา</h2>";
		}else
		{
        	foreach ($post as $key => $value) {
        		if($key=="fbook")
        			$key="facebook";
        		$firebase->set('alumnus/'.$stdyear.'/'.$uid."/".$key,$value);
        	}
        	$firebase->set('alumnus/'.$stdyear.'/'.$uid."/lastUpdate",$lastUpdate);
        	$this->addAdminlog("แก้ไข้โปรไฟล์ของศิษย์เก่ารหัส ".$get['studentId']);
    		$this->session->remove("editUserProfileId");
        	$this->response->redirect('admin/userEdit?studentId='.$get['studentId']);
    	}



	}
	public function editImgProfileAction()
    {
        // $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $subImgPath = json_decode($post['url']);
        $imgUrl = $post['url1']."%".$post['url2']."&token=".$post['token'];
        $studentId = $this->session->get("editUserProfileId");
        $uid = hash('sha256', $studentId);
        $stdyear = substr($studentId,0,2);
        $firebase = $this->connect_firebase();
        $firebase->set('alumnus/'.$stdyear.'/'.$uid."/imgProfile",$imgUrl);
        $this->addAdminlog("เปลี่ยนรูปโปรไฟล์ของศิษย์เก่ารหัส ".$studentId);
        $this->session->remove("editUserProfileId");
    }
    public function addCompanyAction()
    {
        // $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $this->view->webtitle = "เพิ่มบริษัทที่เคยทำงาน";
        $this->view->topic_header = "เพิ่มบริษัทที่เคยทำงาน";
        $firebase = $this->connect_firebase();

        $this->view->listJobs = $this->getJobList();
        $this->view->listCompanyWorks = $this->getCompanyWorkList();

    }
    public function addedCompanyAction()
    {
        // $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $firebase = $this->connect_firebase();
        $post = $this->request->getPost();
        $studentId = $this->session->get("editUserProfileId");
        $uid = hash('sha256', $studentId);
        $stdyear = substr($studentId,0,2);
        $lastUpdate = time();
        //Companu Profile
        $companyName = $post['companyName'];
        $companyAddress = $post['companyAddress'];
        $companyCity = $post['companyCity'];
        $companyPhone = $post['companyPhone'];
        $companyWebsite = $post['companyWebsite'];
        $companyWork = $post['companyWork'];
        $companyFacebook = $post['companyFacebook'];
        $userWork = $post['userWork'];
        $startWork = $post['startWork'];
        $endWork = $post['endWork'];


        //Check Company
        $companyToCheck = (array)json_decode($firebase->get("companies/", array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$companyName.'"')));
        if(count($companyToCheck)==0)
        {
            //Company Data
            $companyData = array(
                "address" => $companyAddress,
                "city" => $companyCity,
                "name" => $companyName,
                "phone" => $companyPhone,
                "website" => $companyWebsite,
                "work" => $companyWork,
                "facebook" => $companyFacebook,
                "lastUpdate" => $lastUpdate
            );
            //Add Company in Firebase
            $companyId = json_decode($firebase->push('companies/', $companyData))->name;
            $firebase->push('alumnus/'.$stdyear.'/'.$uid."/companyWork",array("companyId" => $companyId,"companyName" => $companyName,"userWork" => $userWork,"startWork" => $startWork,"endWork" => $endWork));
        }else
        {
            foreach ($companyToCheck as $key => $value) {
                $firebase->push('alumnus/'.$stdyear.'/'.$uid."/companyWork",array("companyId" => $key,"companyName" => $companyName,"userWork" => $userWork,"startWork" => $startWork,"endWork" => $endWork));
            }
        }
        $this->addAdminlog("เพิ่มข้อมูลบริษัทของศิษย์เก่ารหัส ".$get['studentId']);
        $this->session->remove("editUserProfileId");
        $this->response->redirect('admin/userEdit?studentId='.$studentId);

    }

    public function editCompanyAction()
    {
    	// $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
    	$firebase = $this->connect_firebase();
    	$get = $this->request->get();
       	$studentId = $this->session->get("editUserProfileId");
        $uid = hash('sha256', $studentId);
        $stdyear = substr($studentId,0,2);
        $userCompanyWork = json_decode($firebase->get("alumnus/".$stdyear.'/'.$uid.'/companyWork/'.$get['key']));
        if($userCompanyWork == Null)
		{
			$this->view->disableLevel(
			    View::LEVEL_ACTION_VIEW
			);
			echo "<h2>ไม่พบข้อมูลบริษัท</h2>";
		}else
		{
			$this->session->set("companyEditId",$userCompanyWork->companyId);
			$this->session->set("usersCompanyWork",$get['key']);
			$companyData = json_decode($firebase->get("companies/".$userCompanyWork->companyId));
			$companyWorks = array();
			foreach ($companyData->work as $key => $value) {
				$work = json_decode($firebase->get("companyWorks/".$value));
				array_push($companyWorks, $work->name);
			}
			$jobWork = array();
			foreach ($userCompanyWork->userWork as $key => $value) {
				$job = json_decode($firebase->get("jobs/".$value));
				array_push($jobWork, $job->name);
			}
			$this->view->companyName = $companyData->name;
			$this->view->companyAddress = $companyData->address;
			$this->view->companyCity = $companyData->city;
			$this->view->userWork = $jobWork;
			// $this->view->companyWork = $companyWorks;
			$this->view->startWork = $userCompanyWork->startWork;
			$this->view->endWork = $userCompanyWork->endWork;
			$this->view->companyPhone = $companyData->phone;
			$this->view->companyWebsite = $companyData->website;
			$this->view->companyFacebook = $companyData->facebook;
			$this->view->companyWork = $companyData->work;

        	$this->view->webtitle = "แก้ไขบริษัทที่เคยทำงาน";
        	$this->view->topic_header = "เแก้ไขบริษัทที่เคยทำงาน";
       	
        	$this->view->listJobs = $this->getJobList();
        	$this->view->listCompanyWorks = $this->getCompanyWorkList();
        	$init = 2535;
			$selectyear = [];
			$current = date("Y") + 543;
			for($count = $init; $count < $current; $count++) {
				array_push($selectyear,$count);
			}
			$this->view->yearlist = $selectyear;

		}
    	
    }

    public function editedUserCompanyAction()
    {
    	$companyEditId = $this->session->get("companyEditId");
		$usersCompanyWork = $this->session->get("usersCompanyWork");
		$post = $this->request->getPost();
		$firebase = $this->connect_firebase();
		$studentId = $this->session->get("editUserProfileId");
        $uid = hash('sha256', $studentId);
        $stdyear = substr($studentId,0,2);

        $firebase->set("alumnus/".$stdyear.'/'.$uid.'/companyWork/'.$usersCompanyWork.'/endWork',$post['endWork']);
        $firebase->set("alumnus/".$stdyear.'/'.$uid.'/companyWork/'.$usersCompanyWork.'/startWork',$post['startWork']);
        $firebase->set("alumnus/".$stdyear.'/'.$uid.'/companyWork/'.$usersCompanyWork.'/userWork',$post['userWork']);

        $firebase->set("companies/".$companyEditId."/name",$post['companyName']);
        $firebase->set("companies/".$companyEditId."/address",$post['companyAddress']);
        $firebase->set("companies/".$companyEditId."/city",$post['companyCity']);
        $firebase->set("companies/".$companyEditId."/work",$post['companyWork']);
        $firebase->set("companies/".$companyEditId."/website",$post['companyWebsite']);
        $firebase->set("companies/".$companyEditId."/facebook",$post['companyFacebook']);
        $firebase->set("companies/".$companyEditId."/phone",$post['companyPhone']);

        $this->session->remove("companyEditId");
        $this->session->remove("usersCompanyWork");
        $this->response->redirect('admin/userEdit?studentId='.$studentId);

    }
    public function deleteCompanyAction()
    {
    	$firebase = $this->connect_firebase();
        $get = $this->request->get();
        $studentId = $this->session->get("editUserProfileId");
        $uid = hash('sha256', $studentId);
        $stdyear = substr($studentId,0,2);
        $firebase->delete('alumnus/'.$stdyear.'/'.$uid."/companyWork/".$get['key']);
        $this->addAdminlog("ลบข้อมูลบริษัทของศิษย์เก่ารหัส ".$get['studentId']);
        $this->session->remove("editUserProfileId");
        $this->response->redirect('admin/userEdit?studentId='.$studentId);


    }


  public function userdeleteAction()
  {
  		$firebase = $this->connect_firebase();
    	$get = $this->request->get();
		$this->view->webtitle = $get['studentId'];
		$uid = hash('sha256', $get['studentId']);
		$stdyear = substr($get['studentId'],0,2);
		$userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid));
		if(count((array)$userData) <= 5)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบุคคลที่ท่านต้องการลบ</h2>";
		}else
		{
			//Delete
			$firebase->delete('alumnus/'.$stdyear.'/'.$uid);
			$this->addAdminlog("ลบบัญชีของศิษย์เก่ารหัส ".$get['studentId']);
			$this->response->redirect('admin/user');
		}

  }

  public function userDisableAction()
  {
    	$get = $this->request->get();
		$this->view->webtitle = $get['studentId'];
		//Delete
        $firebase = $this->connect_firebase();
        $uid = hash('sha256', $get['studentId']);
        $stdyear = substr($get['studentId'],0,2);
        $userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid));
        if($userData->disable)
        {
        	$firebase->set('alumnus/'.$stdyear.'/'.$uid."/disable",false);
        	$this->addAdminlog("ปิดบัญชีของศิษย์เก่ารหัส ".$get['studentId']." ชั่วคราว");
        }else
        {
        	$firebase->set('alumnus/'.$stdyear.'/'.$uid."/disable",true);
        	$this->addAdminlog("เปิดบัญชีของศิษย์เก่ารหัส ".$get['studentId']);
        }


		$this->response->redirect('admin/user');
  }

  public function userResetAction()
  {
    	$get = $this->request->get();
		$this->view->webtitle = $get['studentId'];
		//Delete
        $firebase = $this->connect_firebase();
        $uid = hash('sha256', $get['studentId']);
        $stdyear = substr($get['studentId'],0,2);
        $userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid));
        if(count((array)$userData) <= 5)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบุคคลที่ท่านต้องการลบ</h2>";
		}else
		{
			$firebase->delete('alumnus/'.$stdyear.'/'.$uid."/facebookUid");
        	$firebase->delete('facebookAccount/'.$uid."/facebookUid",$facebookUid);
			$this->addAdminlog("รีเซ็ตบัญชี ".$get['studentId']);
			$this->response->redirect('admin/user');
		}
  }


  /* Company Section */
  public function companyAction()
  {
		$this->view->webtitle = "Company";
		$this->view->topic_header = "รายชื่อบริษัท";

		$this->assets
		->addCss('public/assets/global/plugins/datatables/datatables.min.css')
		->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
		->addCss('public/css/tableconfig.css');

		$this->assets
		->addJs('public/assets/global/scripts/datatable.js')
  		->addJs('public/assets/global/plugins/datatables/datatables.min.js')
  		->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
		->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');

		$this->view->tableattr = ["","ชื่อบริษัท","จังหวัด","ประเภทงาน","เว็บไซต์","เบอร์โทรศัพท์",""];

		$firebase = $this->connect_firebase();
		$companies = json_decode($firebase->get("companies"));
		$data = array();
		foreach ($companies as $key => $value) {
			$companyWorks = array();
			foreach ($value->work as $subKey => $subValue) {
				$companyWork = json_decode($firebase->get("companyWorks/".$subValue));
				array_push($companyWorks, array("name" => $companyWork->name,"key" => $subValue));
			}
		array_push($data, array("name" => $value->name,"phone" => $value->phone,"website" => $value->website,"city" => $value->city,"id"=>$key,"work"=>$companyWorks));

		}
		$this->view->data = $data;
  }

function companyprofileAction()
{
	$get = $this->request->get();
	$id = $get['id'];
	$firebase = $this->connect_firebase();
	$companyData = json_decode($firebase->get("companies/".$id));
	if($companyData == Null)
	{
		$this->view->webtitle = "ไม่พบบริษัทที่ที่ท่านค้นหา";
		$this->view->disableLevel(
    	    View::LEVEL_ACTION_VIEW
    	);
    	echo "<h2>ไม่พบบริษัทที่ที่ท่านค้นหา</h2>";
	}else
	{
		$this->view->webtitle = $companyData->name;
    	// $this->view->topic_header = "รายชื่อบริษัท";
    	$this->assets
        ->addCss('public/css/tagsconfig.css');


        $this->view->name = $companyData->name;
        $this->view->address = $companyData->address;
        $this->view->phone = $companyData->phone;
        $this->view->website = $companyData->website;
        $this->view->city = $companyData->city;
        $this->view->facebook = $companyData->facebook;
        $companyWorks = array();
        foreach ($companyData->work as $key => $value) {
        	$data = json_decode($firebase->get("companyWorks/".$value));
        	array_push($companyWorks, array("name" => $data->name,"id" => $value));
        }
        $this->view->work = $companyWorks;
	}


}

  public function companyaddAction()
  {
    $this->view->webtitle = "Add Company";
    //Get Company Work
    $firebase = $this->connect_firebase();
	$this->view->listCompanyWorks = $this->getCompanyWorkList();
    //add company
  }

  public function companyaddedAction()
  {
  	$post = $this->request->getPost();
  	$lastUpdate = time();
  	//Company Data
	$companyData = array(
	    "address" =>$post['companyAddress'],
	    "city" => $post['companyCity'],
	    "name" => $post['companyName'],
	    "phone" => $post['companyPhone'],
	    "website" => $post['companyWebsite'],
	    "work" => $post['companyWork'],
	    "facebook" => $post['companyFacebook'],
	    "lastUpdate" => $lastUpdate
	);

	$firebase = $this->connect_firebase();
	$firebase->push("companies/",$companyData);
	$this->addAdminlog("เพิ่มบริษัท ".$post['companyName']);
	$this->response->redirect('admin/company');
  }



  public function companyeditAction()
  {
    $this->view->webtitle = "Edit Company";

    $get = $this->request->get();
	$id = $get['id'];
	$this->session->set("companyEditId",$id);
	$firebase = $this->connect_firebase();
	$companyData = json_decode($firebase->get("companies/".$id));
	if($companyData == Null)
	{
		$this->view->webtitle = "ไม่พบบริษัทที่ที่ท่านค้นหา";
		$this->view->disableLevel(
    	    View::LEVEL_ACTION_VIEW
    	);
    	echo "<h2>ไม่พบบริษัทที่ที่ท่านค้นหา</h2>";
	}else
	{
    	//edit company
    	$this->assets
    	->addCss('public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')
    	->addCss('public/assets/global/plugins/typeahead/typeahead.css');

    	$this->assets
    	->addJs('public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')
    	->addJs('public/assets/global/plugins/typeahead/handlebars.min.js')
    	->addJs('public/assets/global/plugins/typeahead/typeahead.bundle.min.js')
    	->addJs('public/assets/pages/scripts/components-bootstrap-tagsinput.min.js');

        $this->view->companyName = $companyData->name;
        $this->view->companyAddress = $companyData->address;
        $this->view->companyCity = $companyData->city;
        $this->view->companyWork = $companyData->work;
        $this->view->companyPhone = $companyData->phone;
        $this->view->companyWebsite = $companyData->website;
        $this->view->companyFacebook = $companyData->facebook;
        $this->view->listCompanyWorks = $this->getCompanyWorkList();

	}


  }

  public function editedCompanyAction()
  {
  	$id = $this->session->get("companyEditId");
  	$this->session->remove("companyEditId");
  	$post = $this->request->getPost();
  	$lastUpdate = time();
  	//Company Data
	$companyData = array(
	    "address" =>$post['companyAddress'],
	    "city" => $post['companyCity'],
	    "name" => $post['companyName'],
	    "phone" => $post['companyPhone'],
	    "website" => $post['companyWebsite'],
	    "work" => $post['companyWork'],
	    "facebook" => $post['companyFacebook'],
	    "lastUpdate" => $lastUpdate
	);
	//Add Company in Firebase
	$firebase = $this->connect_firebase();
	foreach ($companyData as $key => $value) {
		$firebase->set('companies/'.$id."/".$key, $value);
	}
	$this->addAdminlog("แก้ไขโปรไฟล์บริษัท ".$post['companyName']);
	$this->response->redirect('admin/company');

  }

  public function companydeleteAction()
  {
  		$firebase = $this->connect_firebase();
  	    $get = $this->request->get();
		$this->view->webtitle = $get['id'];
		$companyData = json_decode($firebase->get("companies/".$get['id']));
		if($companyData == Null)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบริษัทที่ท่านต้องการลบ</h2>";
		}else
		{
			//Delete

			$firebase->delete("companies/".$get['id']);
			$this->addAdminlog("ลบข้อมูลบริษัท ".$companyData->name);
			$this->response->redirect('admin/company');
		}
  }

  /*Skill / Interested Section*/
  public function tagsAction()
  {
    $this->assets
    ->addCss('public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')
    ->addCss('public/assets/global/plugins/typeahead/typeahead.css')
    ->addCss('public/assets/global/plugins/datatables/datatables.min.css')
	->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
	->addCss('public/css/tableconfig.css');

    $this->assets
    ->addJs('public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')
    ->addJs('public/assets/global/plugins/typeahead/handlebars.min.js')
    ->addJs('public/assets/global/plugins/typeahead/typeahead.bundle.min.js')
    ->addJs('public/assets/pages/scripts/components-bootstrap-tagsinput.min.js')
    ->addJs('public/assets/global/scripts/datatable.js')
	->addJs('public/assets/global/plugins/datatables/datatables.min.js')
	->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
	->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');


    $this->view->webtitle = "Tags";
    $this->view->topic_header = "จัดการแท็กและตัวเลือก";

    $firebase = $this->connect_firebase();
    $jobs = json_decode($firebase->get("jobs",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
    $this->view->jobs = $jobs;
    $companyWork = json_decode($firebase->get("companyWorks",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
    $this->view->companyWork = $companyWork;
    $newTagJob = json_decode($firebase->get("other/jobs"));
    if(count((array)$newTagJob) != 0)
    	$this->view->newTagJob = $newTagJob;
    else
    	$this->view->newTagJob = array();
    $newTagCompanyWork = array();
    $newTagCompanyWork = json_decode($firebase->get("other/companyWorks"));
    if(count((array)$newTagCompanyWork) != 0)
    	$this->view->newTagCompanyWork = $newTagCompanyWork;
    else
    	$this->view->newTagCompanyWork = array();
    //view tags
  }

  public function advisorAction()
  {
    $this->assets
    ->addCss('public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')
    ->addCss('public/assets/global/plugins/typeahead/typeahead.css')
    ->addCss('public/assets/global/plugins/datatables/datatables.min.css')
	->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
	->addCss('public/css/tableconfig.css');

    $this->assets
    ->addJs('public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')
    ->addJs('public/assets/global/plugins/typeahead/handlebars.min.js')
    ->addJs('public/assets/global/plugins/typeahead/typeahead.bundle.min.js')
    ->addJs('public/assets/pages/scripts/components-bootstrap-tagsinput.min.js')
    ->addJs('public/assets/global/scripts/datatable.js')
	->addJs('public/assets/global/plugins/datatables/datatables.min.js')
	->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
	->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');


    $this->view->webtitle = "จัดการข้อมูลอาจารย์ที่ปรึกษา";
    $this->view->topic_header = "จัดการข้อมูลอาจารย์ที่ปรึกษา";
    $firebase = $this->connect_firebase();
    $advisor = json_decode($firebase->get("advisor",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
    $this->view->advisor = $advisor;
    
  }

  public function tagsaddAction()
  {
    $this->view->webtitle = "Add Tags";
    //add tags
  }

  public function tagseditAction()
  {
    $this->view->webtitle = "Edit Tags";
    //edit tags
  }

  public function tagsdeleteAction()
  {
    //delete tags
  }

  public function addJobListAction()
  {
  	$this->view->webtitle = "เพิ่มสายงาน";
  }
  public function addedJobListAction()
  {
  	$post = $this->request->getPost();
  	$firebase = $this->connect_firebase();
  	$firebase->push("jobs",array("name" => $post['name'],"status" => true));
  	$this->addAdminlog("เพิ่ม tag ของสายอาชีพ");
  	$this->response->redirect('admin/tags');
  }

  public function editJobListAction()
  {
  	  	$firebase = $this->connect_firebase();
  	    $get = $this->request->get();
		$this->view->webtitle = $get['id'];
		$jobs = json_decode($firebase->get("jobs/".$get['id']));
		if($jobs == Null)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบหมวดที่ท่านต้องการแก้</h2>";
		}else
		{
			$this->session->set("editJobListId",$get['id']);
			$this->view->name = $jobs->name;
		}

  }
  public function editedJobListAction()
  {
  	$post = $this->request->getPost();
  	$firebase = $this->connect_firebase();
  	$id = $this->session->get("editJobListId");
  	$firebase->set("jobs/".$id."/name",$post['name']);
  	$this->addAdminlog("แก้ไข tag ของสายอาชีพ");
  	$this->session->remove("editJobListId");
  	$this->response->redirect('admin/tags');
  }
  public function deleteJobListAction()
  {
  	  	$firebase = $this->connect_firebase();
  	    $get = $this->request->get();
		$this->view->webtitle = $get['id'];
		$jobs = json_decode($firebase->get("jobs/".$get['id']));
		if($jobs == Null)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบหมวดที่ท่านต้องการแก้</h2>";
		}else
		{
			$this->addAdminlog("ลบ tag ของสายอาชีพ");
			$firebase->set("jobs/".$get['id']."/status",false);
			$this->response->redirect('admin/tags');
		}

  }

  public function addCompanyCategoryAction()
  {
  	$this->view->webtitle = "เพิ่มธุรกิจ";
  }
  public function addedCompanyCategoryAction()
  {
  	$post = $this->request->getPost();
  	$firebase = $this->connect_firebase();
  	$firebase->push("companyWorks",array("name" => $post['name'],"status" => true));
  	$this->addAdminlog("เพิ่ม tag ของหมวดธุรกิจ");
  	$this->response->redirect('admin/tags');
  }

  public function editCompanyCategoryAction()
  {
  	  	$firebase = $this->connect_firebase();
  	    $get = $this->request->get();
		$this->view->webtitle = $get['id'];
		$companyWorks = json_decode($firebase->get("companyWorks/".$get['id']));
		if($companyWorks == Null)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบหมวดที่ท่านต้องการแก้</h2>";
		}else
		{
			$this->session->set("companyWorksId",$get['id']);
			$this->view->name = $companyWorks->name;
		}

  }
  public function editedCompanyCategoryAction()
  {
  	$post = $this->request->getPost();
  	$firebase = $this->connect_firebase();
  	$id = $this->session->get("companyWorksId");
  	$firebase->set("companyWorks/".$id."/name",$post['name']);
  	$this->addAdminlog("แก้ไข tag ของหมวดธุรกิจ");
  	$this->session->remove("companyWorksId");
  	$this->response->redirect('admin/tags');
  }

  public function deleteCompanyCategoryAction()
  {
  	  	$firebase = $this->connect_firebase();
  	    $get = $this->request->get();
		$this->view->webtitle = $get['id'];
		$companyWorks = json_decode($firebase->get("companyWorks/".$get['id']));
		if($companyWorks == Null)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบหมวดที่ท่านต้องการแก้</h2>";
		}else
		{
			$this->addAdminlog("ลบ tag ของหมวดธุรกิจ");
			$firebase->set("companyWorks/".$get['id']."/status",false);
			$this->response->redirect('admin/tags');
		}

  }

  public function addAdvisorAction()
  {
  	$this->view->webtitle = "เพิ่มอาจารย์ที่ปรึกษา";
  }
  public function addedAdvisorAction()
  {
  	$post = $this->request->getPost();
  	$firebase = $this->connect_firebase();
  	$firebase->push("advisor",array("name" => $post['name'],"status" => true));
  	$this->addAdminlog("เพิ่ม อาจารย์ที่ปรึกษา");
  	$this->response->redirect('admin/advisor');
  }
  public function editAdvisorAction()
  {
  	  	$firebase = $this->connect_firebase();
  	    $get = $this->request->get();
		$this->view->webtitle = $get['id'];
		$advisor = json_decode($firebase->get("advisor/".$get['id']));
		if($advisor == Null)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบุคคลที่ท่านค้นหา</h2> <a href='tags'>กลับ</a>";
		}else
		{
			$this->session->set("advisorId",$get['id']);
			$this->view->name = $advisor->name;
		}

  }
  public function editedAdvisorAction()
  {
  	$post = $this->request->getPost();
  	$firebase = $this->connect_firebase();
  	$id = $this->session->get("advisorId");
  	$firebase->set("advisor/".$id."/name",$post['name']);
  	$this->addAdminlog("แก้ไขชื่อของอาจารย์ที่ปรึกษา");
  	$this->session->remove("advisorId");
  	$this->response->redirect('admin/advisor');
  }
  public function deleteAdvisorAction()
  {
  	$firebase = $this->connect_firebase();
    $get = $this->request->get();
		$this->view->webtitle = $get['id'];
		$advisor = json_decode($firebase->get("advisor/".$get['id']));
		if($advisor == Null)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบุคคลที่ท่านค้นหา</h2> <a href='tags'>กลับ</a>";
		}else
		{
			$this->addAdminlog("ลบอาจารย์ที่ปรึกษา");
			$firebase->set("advisor/".$get['id']."/status",false);
			$this->response->redirect('admin/advisor');
		}

  }


  public function logoutAction()
  {
		$this->session->remove("adminData");
		$this->response->redirect('');
  }

  function getyearAction() {
    $firebase = $this->connect_firebase();
    $year = json_decode($firebase->get("alumnus/",array("shallow" => true)));
    $year = count((array)$year) + 34;
    print_r($year);
    exit();
  }

  function searchAction()
	{
		$this->view->webtitle = "ค้นหา";
		$this->view->topic_header = "ค้นหา";
		$this->view->tableattr = ["","รหัสนักศึกษา","ชื่อ-นามสกุล","ชื่อเล่น","เกียร์","บริษัทที่ทำงาน","ประเภทของงาน","Facebook"];
		$this->assets
		->addCss('public/assets/global/plugins/datatables/datatables.min.css')
		->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
		->addCss('public/css/tableconfig.css');

		$this->assets
		->addJs('public/assets/global/scripts/datatable.js')
		->addJs('public/assets/global/plugins/datatables/datatables.min.js')
		->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
		->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');
    	$this->view->job = "";
		$this->view->stdname = "";
		$this->view->stdcode = "";
		$this->view->certificate = "";
		$this->view->course = "";
		$this->view->gear = "";
		$this->view->city = "";
		$this->view->advisor = "";
		$this->view->advisorName = " ทุกคน ";

		$firebase = $this->connect_firebase();
		$year = json_decode($firebase->get("alumnus/",array("shallow" => true)));
		$year = count((array)$year) + 35;

		$yearNow = (int)date("Y",time());
		$yearNow = (string)$yearNow+543;
		$yearNow = (string)$yearNow;
		$yearStudent = substr($yearNow,2);
		$yearForSearch = array();
		for ($i=35; $i < $year; $i++) {
		array_push($yearForSearch, $i);
		}
		$this->view->yearForSearch = $yearForSearch;
		$firebase = $this->connect_firebase();
		$get = $this->request->get();
		$get['yearEnd'] = $get['yearStart'];
		$allUid = array();
		$usersAllInSearch = array();
		$check = false;
		foreach ($get as $key => $value) {
		if($key == "yearStart")
		{
			if($get['yearStart'] != null && $get['yearStart'] != "")
			{

			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInYear = $this->searchByYear($allUid,$get['yearStart'],$get['yearEnd']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInYear;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInYear);
			// $usersAllInSearch += $userInJob;
			$check = true;
			}
		}
		else if($key == "gear")
		{
			if($get['gear'] != null && $get['gear'] != "")
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInGen = $this->searchGen($get['gear']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInGen;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInGen);
			// $usersAllInSearch += $userInGen;
			$this->view->gear = $get['gear'];
			$check = true;
			}
		}
		else if($key == "type")
		{
			if($get['type'] != null && $get['type'] != "" )
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInType = $this->searchType($allUid,$get['type']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInType;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInType);
			$check = true;
			}

		}else if($key == "advisor")
		{
			if($get['advisor'] != null && $get['advisor'] != "" )
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInAdvisor = $this->searchAdvisor($allUid,$get['advisor']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInAdvisor;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInAdvisor);
			// $usersAllInSearch += $userInAdvisor;
			$this->view->advisor = "";
			// $this->view->advisorName = json_decode($firebase->get("advisor/".$get['advisor']."/"))->name;
			$this->view->advisorName = " ทุกคน ";
			$check = true;
			}
		}else if ($key == "certificate")
		{
			if($get['certificate'] != null && $get['certificate'] != "")
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInCertificate = $this->searchCertificate($allUid,$get['certificate']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInCertificate;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInCertificate);
			$check = true;
			}
		}else if ($key == "stdname")
		{
			if($get['stdname'] != null && $get['stdname'] != "")
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInStdname = $this->searchStdname($allUid,$get['stdname']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInStdname;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInStdname);
			$this->view->stdname = $get['stdname'];
			$check = true;
			}
		}
		else if ($key == "stdcode")
		{
			if($get['stdcode'] != null && $get['stdcode'] != "")
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInStdcode = $this->searchStdcode($get['stdcode'],$allUid,$get['yearStart'],$get['yearEnd']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInStdcode;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInStdcode);
			$this->view->stdcode = $get['stdcode'];
			$check = true;
			}
		}
		}
		$usersData = array();
		$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
		foreach ($usersAllInSearch as $uid => $value) {
			for ($i=$get['yearStart']; $i <= $get['yearEnd']; $i++) { 
      		 	$userData = json_decode($firebase->get('alumnus/'.$i.'/'.$uid));
      		 	if(count((array)$userData) != 0)
      		 	{
      		 		$usersData[$uid] = $userData;
      		 		break;
      		 	}
      		}
		}
		$advisorAll = (array)json_decode($firebase->get("advisor/"));
		$companyWorkAll = (array)json_decode($firebase->get("companies/"));
		$jobAll = (array)json_decode($firebase->get("jobs/"));
		$userDatasView = $this->processToUserDatasView($allUid,$usersData,$advisorAll,$companyWorkAll,$jobAll);
			$this->view->userDatasView = $userDatasView;
			$advisorList = json_decode($firebase->get("advisor",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
			$this->view->advisorList = $advisorList;

	}

     public function listCompanyAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $firebase = $this->connect_firebase();
        $listCompany = array();
        $companData = json_decode($firebase->get("companies/", array('print' => 'pretty')));
        foreach ($companData as $key => $value) {
            array_push($listCompany, $value->name);
        }
        echo json_encode($listCompany);
        exit();
    }
    public function checkCompanyAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $firebase = $this->connect_firebase();

        $companyName = $post['name'];
        $companData = json_decode($firebase->get("companies/", array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$companyName.'"')));
        foreach ($companData as $key => $value) {
            echo json_encode($value);
        }
        exit();
        // echo $companyName;

    }

    public function addEducationAction()
    {
    	$this->view->webtitle = "เพิ่มประวัติการศึกษา";
		$this->view->topic_header = "เพิ่มประวัติการศึกษา";

    }

    public function addedEducationAction()
    {
    	$firebase = $this->connect_firebase();
    	$post = $this->request->getPost();

    	if($post['course'] == "" || $post['major'] == "" || $post['university'] == "" || $post['startStudy'] == "" || $post['endStudy'] == "" )
    	{
    		$this->view->webtitle = "Error";
    		$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>กรอกข้อมูลไม่ครบ กรุณาตรวจสอบข้อมูล</h2> <a href='addEducation'>กลับ</a>";
    	}else
    	{
    		$majorData = json_decode($firebase->get("major/",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$post['major'].'"')));
    		if(count((array)$majorData) == 0)
    		{
    			$firebase->push("major",array("name" => $post['major'],"status" => true));
    		}
    		$universityData = json_decode($firebase->get("university/",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$post['university'].'"')));
    		if(count((array)$universityData) == 0)
    		{
    			$firebase->push("university",array("name" => $post['university'],"status" => true));
    		}
    		$studentId = $this->session->get("editUserProfileId");
    		$stdyear = substr($studentId,0,2);
    		$uid = hash('sha256', $studentId);
    		$firebase->push('alumnus/'.$stdyear.'/'.$uid."/education",array(
    				"course" => $post['course'],
    				"major" => $post['major'],
    				"university" => $post['university'],
    				"startStudy" => $post['startStudy'],
    				"endStudy" => $post['endStudy']
    			));
    		$this->addAdminlog("เพิ่มประวัติการศึกษาของรหัส ".$studentId);
    		$this->session->remove("editUserProfileId");
    		$this->response->redirect('admin/userEdit?studentId='.$studentId);
    	}

    }

    public function editEducationAction()
    {
    	$firebase = $this->connect_firebase();
    	$get = $this->request->get();
		$studentId = $this->session->get("editUserProfileId");
    	$uid = hash('sha256', $studentId);
    	$stdyear = substr($studentId,0,2);
        $userEducation= json_decode($firebase->get("alumnus/".$stdyear.'/'.$uid.'/education/'.$get['key']));
        if($userEducation == Null)
		{
			$this->view->disableLevel(
			    View::LEVEL_ACTION_VIEW
			);
			echo "<h2>ไม่พบข้อมูล</h2>";
		}else
		{
			$this->view->webtitle = "แก้ไขข้อมูลการศึกษา";
        	$this->view->topic_header = "แก้ไขข้อมูลการศึกษา";
        	$this->session->set("educationEditId",$get['key']);
        	$course = ['ตรี','โท','เอก'];
        	$courseOption = [];
        	foreach ($course as $key => $value) {
        		if($userEducation->course == $value)
        		{
        			array_push($courseOption, "<option value=".$value." selected>".$value."</option>");
        		}else
        		{
        			array_push($courseOption, "<option value=".$value.">".$value."</option>");
        		}
        	}
        	$init = 2535;
			$selectyear = [];
			$current = date("Y") + 543;
			for($count = $init; $count < $current; $count++) {
				array_push($selectyear,$count);
			}
			$this->view->yearlist = $selectyear;
        	$this->view->courseOption = $courseOption;
        	$this->view->major = $userEducation->major;
        	$this->view->startStudy = $userEducation->startStudy;
        	$this->view->endStudy = $userEducation->endStudy;
        	$this->view->university = $userEducation->university;
		}
    }

    public function editedEducationAction()
    {
    	$post = $this->request->getPost();
    	$educationEditId = $this->session->get("educationEditId");
    	$firebase = $this->connect_firebase();
    	$studentId = $this->session->get("editUserProfileId");
    	$uid = hash('sha256', $studentId);
    	$stdyear = substr($studentId,0,2);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/education/'.$educationEditId.'/course',$post['course']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/education/'.$educationEditId.'/endStudy',$post['endStudy']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/education/'.$educationEditId.'/major',$post['major']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/education/'.$educationEditId.'/startStudy',$post['startStudy']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/education/'.$educationEditId.'/university',$post['university']);
    	$this->session->remove("educationEditId");
    	$this->response->redirect('admin/userEdit?studentId='.$studentId);

    }

    public function deleteEducationAction()
    {
    	$firebase = $this->connect_firebase();
        $get = $this->request->get();
        $studentId = $this->session->get("editUserProfileId");
    	$uid = hash('sha256', $studentId);
    	$stdyear = substr($studentId,0,2);
        $firebase->delete('alumnus/'.$stdyear.'/'.$uid."/education/".$get['key']);
        $this->addAdminlog("ลบประวัติการศึกษาของรหัส ".$studentId);
        $this->session->remove("editUserProfileId");
        $this->response->redirect('admin/userEdit?studentId='.$studentId);

    }

    public function addCerAction()
    {
    	$this->view->webtitle = "เพิ่มประกาศนียบัตร";
		$this->view->topic_header = "เพิ่มประกาศนียบัตร";
    }

    public function addedCerAction()
    {
    	$firebase = $this->connect_firebase();
    	$post = $this->request->getPost();
    	if($post['certifier'] == "" || $post['certificate'] == "" || $post['receipt'] == "" )
    	{
    		$this->view->webtitle = "Error";
    		$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>กรอกข้อมูลไม่ครบ กรุณาตรวจสอบข้อมูล</h2> <a href='addCer'>กลับ</a>";
    	}else
    	{
    		$certificateData = json_decode($firebase->get("certificate/",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$post['certificate'].'"')));
    		if(count((array)$majorData) == 0)
    		{
    			$firebase->push("certificate",array("name" => $post['certificate'],"status" => true));
    		}
    		$certifierData = json_decode($firebase->get("certifier/",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$post['certifier'].'"')));
    		if(count((array)$certifierData) == 0)
    		{
    			$firebase->push("certifier",array("name" => $post['certifier'],"status" => true));
    		}
    		$studentId = $this->session->get("editUserProfileId");
    		$stdyear = substr($studentId,0,2);
    		$uid = hash('sha256', $studentId);
    		$firebase->push('alumnus/'.$stdyear.'/'.$uid."/certificate",array(
    				"certifier" => $post['certifier'],
    				"certificate" => $post['certificate'],
    				"receipt" => $post['receipt']
    			));

    		$this->addAdminlog("เพิ่มประกาศนียบัตรของรหัส ".$studentId);
        	$this->session->remove("editUserProfileId");
    		$this->response->redirect('admin/userEdit?studentId='.$studentId);
    	}

    }

    public function editCerAction()
    {
    	$firebase = $this->connect_firebase();
    	$get = $this->request->get();
		$studentId = $this->session->get("editUserProfileId");
    	$uid = hash('sha256', $studentId);
    	$stdyear = substr($studentId,0,2);
        $userCer= json_decode($firebase->get("alumnus/".$stdyear.'/'.$uid.'/certificate/'.$get['key']));
        if($userCer == Null)
		{
			$this->view->disableLevel(
			    View::LEVEL_ACTION_VIEW
			);
			echo "<h2>ไม่พบข้อมูล</h2>";
		}else
		{
			$this->view->webtitle = "แก้ไขข้อมูลประกาศนียบัตร";
        	$this->view->topic_header = "แก้ไขข้อมูลประกาศนียบัตร";
        	$this->session->set("cerEditId",$get['key']);
			$init = 2535;
			$selectyear = [];
			$current = date("Y") + 543;
			for($count = $init; $count < $current; $count++) {
				array_push($selectyear,$count);
			}
			$this->view->yearlist = $selectyear;
        	$this->view->certificate = $userCer->certificate;
        	$this->view->certifier = $userCer->certifier;
        	$this->view->receipt = $userCer->receipt;
		}
    }

    public function editedCerAction()
    {
    	$post = $this->request->getPost();
    	$cerEditId = $this->session->get("cerEditId");
    	$firebase = $this->connect_firebase();
    	$studentId = $this->session->get("editUserProfileId");
    	$uid = hash('sha256', $studentId);
    	$stdyear = substr($studentId,0,2);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/certificate/'.$cerEditId.'/certificate',$post['certificate']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/certificate/'.$cerEditId.'/certifier',$post['certifier']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/certificate/'.$cerEditId.'/receipt',$post['receipt']);
    	$this->session->remove("cerEditId");
    	$this->response->redirect('admin/userEdit?studentId='.$studentId);

    }

    public function deleteCerAction()
    {
    	$firebase = $this->connect_firebase();
        $get = $this->request->get();
        $studentId = $this->session->get("editUserProfileId");
    	$uid = hash('sha256', $studentId);
    	$stdyear = substr($studentId,0,2);
        $firebase->delete('alumnus/'.$stdyear.'/'.$uid."/certificate/".$get['key']);
        $this->addAdminlog("ลบประกาศนียบัตรของรหัส ".$studentId);
        $this->session->remove("editUserProfileId");
        $this->response->redirect('admin/userEdit?studentId='.$studentId);

    }

    public function addAlumnusAction()
    {

    	$this->view->webtitle = "เพิ่มศิษย์เก่าเพื่อใช้ยืนยันตัวตน";
		$this->view->topic_header = "เพิ่มศิษย์เก่าเพื่อใช้ยืนยันตัวตน";

    }
    public function addedAlumnusAction()
    {
    	$file = $_FILES['file']['tmp_name'];
    	if($_FILES['file']['type'] == "application/vnd.ms-excel")
    	{
    		if(strtolower ( explode(".", $_FILES['file']['name'])[count(explode(".", $_FILES['file']['name']))-1]) == "csv")
    		{
    		$content = file_get_contents($_FILES['file']['tmp_name']);
			$content = explode("\n", $content);
  			$firebase = $this->connect_firebase();
  			foreach ($content as $key => $value) {
  				$data = explode(",", $value);
  				$student_id = $data[0];
  				$uid = hash('sha256', $student_id);
        	    $year = substr($student_id,0,2);
        	    $fname = $data[1];
        	    $lname = $data[2];
        	    $gender = $data[4];
        	    echo $student_id." : ".$fname." ".$lname." เพศ ".$gender." : ".$year."<br>";
        	    $profile = array("studentId" => $student_id,"fname" => $fname,"lname" => $lname,"gender" => $gender,"year" => "25".$year);
        	    $firebase->set('alumnus/'.$year."/".$uid."/studentId", $student_id);
        	    $firebase->set('alumnus/'.$year."/".$uid."/fname", $fname);
        	    $firebase->set('alumnus/'.$year."/".$uid."/lname", $lname);
        	    $firebase->set('alumnus/'.$year."/".$uid."/gender", $gender);
        	    $firebase->set('alumnus/'.$year."/".$uid."/year", "25".$year);
  			}
  			$this->view->webtitle = "เพิ่มศิษย์เก่าเพื่อใช้ยืนยันตัวตน";
  			}else
  			{
  				$this->view->webtitle = "เพิ่มศิษย์เก่าเพื่อใช้ยืนยันตัวตน";
  				echo "<h2>เฉพาะไฟล์ .csv เท่านั้น</h2> <a href='addAlumnus'>กลับ</a>";
  			}
    	}else
    	{
    		$this->view->webtitle = "เพิ่มศิษย์เก่าเพื่อใช้ยืนยันตัวตน";
    		echo "<h2>เฉพาะไฟล์ .csv เท่านั้น</h2> <a href='addAlumnus'>กลับ</a>";
    	}

    }

    public function deleteNewTagJobAction()
    {
    	$firebase = $this->connect_firebase();
        $get = $this->request->get();
        if($get['id'] != null && $get['id'] != "")
        {
        	$firebase->delete('other/jobs/'.$get['id']);
        	$this->addAdminlog("ลบตัวเลือกสายงานใหม่");
        	$this->response->redirect('admin/tags');
    	}else
    	{
    		$this->response->redirect('admin/tags');
    	}

    }

    public function deleteNewTagCompanyWorkAction()
    {
    	$firebase = $this->connect_firebase();
        $get = $this->request->get();
        if($get['id'] != null && $get['id'] != "")
        {
        	$firebase->delete('other/companyWorks/'.$get['id']);
        	$this->addAdminlog("ลบตัวเลือกตัวเลือกธุรกิจใหม่");
        	$this->response->redirect('admin/tags');
        }else
        {
        	$this->response->redirect('admin/tags');
        }


    }


}
