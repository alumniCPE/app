<?php
use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;
class AuthenticationController extends ControllerBase
{
	public function initialize()
    {
        $this->assets
        //BEGIN GLOBAL MANDATORY STYLES//
            ->addCss('public/assets/global/plugins/font-awesome/css/font-awesome.min.css')
            ->addCss('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')
            ->addCss('public/assets/global/plugins/bootstrap/css/bootstrap.min.css')
            ->addCss('public/assets/global/plugins/uniform/css/uniform.default.css')
            ->addCss('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')
        //END GLOBAL MANDATORY STYLES//
        //BEGIN PAGE LEVEL PLUGINS//
            ->addCss('public/assets/global/plugins/select2/css/select2.min.css')
            ->addCss('public/assets/global/plugins/select2/css/select2-bootstrap.min.css')
        //END PAGE LEVEL PLUGINS//
        //BEGIN THEME GLOBAL STYLES //
            ->addCss('public/assets/global/css/components.min.css')
            ->addCss('public/assets/global/css/plugins.min.css')
        //END THEME GLOBAL STYLES//
        //BEGIN PAGE LEVEL STYLES//
            ->addCss('public/assets/pages/css/login.min.css');
        //END PAGE LEVEL STYLES//


        $this->assets
        //BEGIN CORE PLUGINS//
            ->addJs('public/assets/global/plugins/jquery.min.js')
            ->addJs('public/assets/global/plugins/bootstrap/js/bootstrap.min.js')
            ->addJs('public/assets/global/plugins/js.cookie.min.js')
            ->addJs('public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')
            ->addJs('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')
            ->addJs('public/assets/global/plugins/jquery.blockui.min.js')
            ->addJs('public/assets/global/plugins/uniform/jquery.uniform.min.js')
            ->addJs('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')
        //END CORE PLUGINS//
        //<!-- BEGIN PAGE LEVEL PLUGINS -->
            ->addJs('public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')
            ->addJs('public/assets/global/plugins/jquery-validation/js/additional-methods.min.js')
            ->addJs('public/assets/global/plugins/select2/js/select2.full.min.js')
        //<!-- END PAGE LEVEL PLUGINS -->
        // <!-- BEGIN THEME GLOBAL SCRIPTS -->
            ->addJs('public/assets/global/scripts/app.min.js')
        // <!-- END THEME GLOBAL SCRIPTS -->
        // <!-- BEGIN PAGE LEVEL SCRIPTS -->
            ->addJs('public/assets/pages/scripts/login.min.js');
        // <!-- END PAGE LEVEL SCRIPTS -->

    }

    // public function indexAction()
    // {
    // 	if($this->request->getPost('studentId')) {

    //         $studentId = $this->request->getPost('studentId');
    //         $stdyear = substr($studentId,0,2);  //new
    //         $password = $this->request->getPost('password');
    //         $firebase = $this->connect_firebase();
    //         $uid = hash('sha256', $studentId);
    //         $userData = json_decode($firebase->get("users/".$uid));  //change
    //         $salt = $userData->salt;
    //         $hashPassword = $userData->password;
    //         $newPasswordToCheck = $this->hash_salt_generate($password,$salt);
    //         if($newPasswordToCheck == $hashPassword)
    //         {
    //             $this->session->set("userData", $userData);
    //             return $this->response->redirect('main/home');
    //         }
    //         else
    //             return $this->response->redirect('authentication/admin');

    //     }

    // }

    public function facebookAuthAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $this->view->getTitle = "Welcome";
        $this->view->logo_locate = "public/img/eng_logo.png";
        if($this->session->get("userData") == Null)
        {

        }else
        {
            return $this->response->redirect('main/home');
        }
        // echo "string";
    }
    public function facebookPushAction()
    {
        //Push Facebook UID
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $this->view->getTitle = "Welcome";
        $this->view->logo_locate = "public/img/eng_logo.png";
        $studentId = $this->request->getPost('studentId');
        $fullname = $this->request->getPost('fullname');
        $splitFullname = explode(" ",$fullname);

        $fname = $splitFullname[0];
        $lname = $splitFullname[1];
        $uid = hash('sha256', $studentId);
        $year = substr($studentId,0,2);

        //Check StudentId In Database
        $firebase = $this->connect_firebase();
        $alumniData = (array)json_decode($firebase->get('alumnus/'.$year,array('print' => 'pretty','orderBy' => '"studentId"','equalTo' => '"'.$studentId.'"')));
        
        if(count($alumniData) == 0)
        {
            //Not alumni
            $this->session->remove("facebookUid");
            $this->session->set("LoginTextError","ไม่พบรหัสของคุณในฐานข้อมูล");
            return $this->response->redirect('');
        }else
        {
            foreach ($alumniData as $key => $value) {
                if($value->fname == $fname && $value->lname == $lname)
                {
                    $userData = json_decode($firebase->get("alumnus/".$year.'/'.$uid));
                    if($userData->facebookUid == Null)
                    {
                        if($userData->isRegistered)
                        {
                            $facebookUid = $this->session->get("facebookUid");
                            $firebase->set('alumnus/'.$year.'/'.$uid."/facebookUid",$facebookUid);
                            $firebase->set('facebookAccount/'.$uid."/facebookUid",$facebookUid);
                            $userData = json_decode($firebase->get("alumnus/".$year.'/'.$uid));
                            $this->session->set("userData", $userData);
                            return $this->response->redirect('main/home');
                        }
                        //First Time
                        $this->session->set("studentId",$studentId);
                        $this->session->remove("LoginTextError");
                        return $this->response->redirect('register');
                    }else
                    {
                        $facebookUid = $this->session->get("facebookUid");
                        if($userData->facebookUid == $facebookUid)
                        {
                            $this->session->set("userData", $userData);
                            $this->session->remove("LoginTextError");
                            return $this->response->redirect('main/home');
                        }else
                        {
                            //User in Database เคยสมัครแล้ว
                            if($userData->facebookUid == "" || $userData->facebookUid == null)
                            {
                                $firebase->set("alumnus/".$year.'/'.$uid."/facebookUid",$facebookUid);
                                $this->session->set("userData", $userData);
                                $this->session->remove("LoginTextError");
                                return $this->response->redirect('main/home');
                            }else
                            {
                                $this->session->remove("facebookUid");
                                $this->session->set("LoginTextError","รหัสนี้เคยสมัครแล้ว");
                                return $this->response->redirect('');
                            }
                        }

                    }
                }else
                {
                    //Student ID Not macth Name
                    $this->session->remove("facebookUid");
                    $this->session->set("LoginTextError","รหัสและชื่อของคุณไม่ตรงกับในฐานข้อมูล");
                    return $this->response->redirect('');
                }
            }
        }


    }

    public function backendAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $this->view->getTitle = "Welcome";
        $this->view->logo_locate = "public/img/eng_logo.png";
        $this->view->LoginTextError = $this->session->get("LoginTextError");
        if($this->session->get("adminData") == Null)
        {

        }else
        {
            return $this->response->redirect('admin');
        }
    }

    public function adminAuthAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        if($this->request->getPost('username')) {

            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            $firebase = $this->connect_firebase();
            $uid = hash('sha256', $username);
            $adminData = json_decode($firebase->get('admins/'.$uid));
            $salt = $adminData->salt;
            $hashPassword = $adminData->password;
            $newPasswordToCheck = $this->hash_salt_generate($password,$salt);

            if($newPasswordToCheck == $hashPassword)
            {
                $this->session->set("adminData", $adminData);
                return $this->response->redirect('admin');
            }
            else
                return $this->response->redirect('authentication/admin');

        }
    }

}
