<?php
use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Url;

class MainController extends ControllerBase {

	function initialize()
	{
		$this->view->setTemplateAfter('navbar');
		$this->assets
		/*<!-- BEGIN GLOBAL MANDATORY STYLES -->*/
		->addCss('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all')
		->addCss('public/assets/global/plugins/font-awesome/css/font-awesome.min.css')
		->addCss('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')
		->addCss('public/assets/global/plugins/bootstrap/css/bootstrap.min.css')
		->addCss('public/assets/global/plugins/uniform/css/uniform.default.css')
		->addCss('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')
		/*<!-- END GLOBAL MANDATORY STYLES -->*/
		/*<!-- BEGIN PAGE LEVEL PLUGINS -->*/
		->addCss('public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')
		->addCss('public/assets/global/plugins/morris/morris.css')
		->addCss('public/assets/global/plugins/fullcalendar/fullcalendar.min.css')
		->addCss('public/assets/global/plugins/jqvmap/jqvmap/jqvmap.css')
		->addCss('public/assets/global/plugins/bootstrap-select/css/bootstrap-select.css')
        ->addCss('public/assets/global/plugins/jquery-multi-select/css/multi-select.css')
        ->addCss('public/assets/global/plugins/typeahead/typeahead.css')
		/*<!-- END PAGE LEVEL PLUGINS -->*/
		/*<!-- BEGIN THEME GLOBAL STYLES -->*/
		->addCss('public/assets/global/css/components.min.css')
		->addCss('public/assets/global/css/plugins.min.css')
		/*<!-- END THEME GLOBAL STYLES -->*/
		/*<!-- BEGIN THEME LAYOUT STYLES -->*/
		->addCss('public/assets/layouts/layout3/css/layout.min.css')
		->addCss('public/assets/layouts/layout3/css/themes/default.min.css')
		->addCss('public/assets/layouts/layout3/css/custom.min.css');
		/*<!-- END THEME LAYOUT STYLES -->*/

		$this->assets
		//Config Project//
		->addJs('public/js/config.js')
		//End Config Project//
		//<!-- BEGIN CORE PLUGINS -->
		->addJs('public/assets/global/plugins/jquery.min.js')
		->addJs('public/assets/global/plugins/bootstrap/js/bootstrap.min.js')
		->addJs('public/assets/global/plugins/js.cookie.min.js')
		->addJs('public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')
		->addJs('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')
		->addJs('public/assets/global/plugins/jquery.blockui.min.js')
		->addJs('public/assets/global/plugins/uniform/jquery.uniform.min.js')
		->addJs('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')
		//<!-- END CORE PLUGINS -->
		//<!-- BEGIN PAGE LEVEL PLUGINS -->
		->addJs('public/assets/global/plugins/moment.min.js')
		->addJs('public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')
		->addJs('public/assets/global/plugins/morris/morris.min.js')
		->addJs('public/assets/global/plugins/morris/raphael-min.js')
		->addJs('public/assets/global/plugins/counterup/jquery.waypoints.min.js')
		->addJs('public/assets/global/plugins/counterup/jquery.counterup.min.js')
		->addJs('public/assets/global/plugins/fullcalendar/fullcalendar.min.js')
		->addJs('public/assets/global/plugins/flot/jquery.flot.min.js')
		->addJs('public/assets/global/plugins/flot/jquery.flot.resize.min.js')
		->addJs('public/assets/global/plugins/flot/jquery.flot.categories.min.js')
		->addJs('public/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')
		->addJs('public/assets/global/plugins/jquery.sparkline.min.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')
		->addJs('public/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')
		->addJs('public/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')
    	->addJs('public/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')
    	->addJs('public/assets/global/plugins/typeahead/handlebars.min.js')
		->addJs('public/assets/global/plugins/typeahead/typeahead.bundle.min.js')
		//<!-- END PAGE LEVEL PLUGINS -->
		//<!-- BEGIN THEME GLOBAL SCRIPTS -->
		->addJs('public/assets/global/scripts/app.min.js')
		//<!-- END THEME GLOBAL SCRIPTS -->
		//<!-- BEGIN PAGE LEVEL SCRIPTS -->
		->addJs('public/assets/pages/scripts/dashboard.min.js')
		->addJs('public/assets/pages/scripts/components-typeahead.js')
		//<!-- END PAGE LEVEL SCRIPTS -->
		//<!-- BEGIN THEME LAYOUT SCRIPTS -->
		->addJs('public/assets/layouts/layout3/scripts/layout.min.js')
		->addJs('public/assets/layouts/layout3/scripts/demo.min.js')
		->addJs('public/assets/layouts/global/scripts/quick-sidebar.min.js');
		//<!-- END THEME LAYOUT SCRIPTS -->

		$userData = $this->session->get("userData");
		if(count($userData) == 0)
		{
			$this->response->redirect('');
		}else
		{
			if(!$userData->disable)
			{
				$this->session->set("LoginTextError","บัญชีถูกระงับ");
				$this->session->remove("userData");
			   	return $this->response->redirect('');
			}

		}
		//$this->view->studentId = $userData->studentId;
		$this->view->name = $userData->fname.' '.$userData->lname;
		$this->view->baseUrl = $this->baseUrl();

		$init = 2535;
		$selectyear = [];
		$current = date("Y") + 543;
		for($count = $init; $count < $current; $count++) {
			array_push($selectyear,$count);
		}
		$this->view->yearlist = $selectyear;

	}

	function indexAction()
	{
		$this->tag->setTitle("Profile");
		$this->view->disable();
		$this->response->redirect('main/home');
	}

	
	function anotherAction()
	{
		var_dump('testfunction');
		exit();
	}


	function homeAction()
	{
		$this->session->remove("LoginTextError");
		$firebase = $this->connect_firebase();
		$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
		$uid = hash('sha256', $userData->studentId);
		$userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid)); //change
		$this->session->set("userData",$userData);

		$this->assets
		->addCss('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')
		->addCss('public/assets/pages/css/profile-2.min.css')
		->addCss('public/assets/global/plugins/datatables/datatables.min.css')
		->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
		->addCss('public/css/tableconfig.css');

		$this->assets
		->addJs('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')
		->addJs('public/assets/global/scripts/datatable.js')
		->addJs('public/assets/global/plugins/datatables/datatables.min.js')
		->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
		->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');
    	// ->addJs('http://maps.google.com/maps/api/js?sensor=false')
    	// ->addJs('public/assets/global/plugins/gmaps/gmaps.min.js');

  	$engGear = (int)substr($userData->studentId,0,2)-12;
  	$this->view->webtitle = "Profile";
		$this->view->topic = "Profile";
		$this->view->fname = $userData->fname;
		$this->view->lname = $userData->lname;
		$this->view->efname = $userData->efname;
		$this->view->elname = $userData->elname;
		$this->view->nickname = $userData->nickname;
		$this->view->email = $userData->email;
		$this->view->gender = $userData->gender;
		$this->view->address = $userData->address;
		$this->view->city = $userData->city;
		$this->view->phone = $userData->phone;
		$this->view->facebookUid = $userData->facebookUid;
		$this->view->studentId = $userData->studentId;
		$this->view->entryYear = $userData->entryYear;
		$this->view->graduateYear = $userData->graduateYear;
		$this->view->advisor = $userData->advisor;
		$this->view->advisorName = json_decode($firebase->get("advisor/".$userData->advisor."/name"));
		$this->view->imgProfile = $userData->imgProfile;
		$this->view->alumniType = $userData->alumniType;
		$this->view->engGear = $engGear;
		$this->view->gen = $userData->gen;
		if($userData->education == null)
			$this->view->education = array();
		else
			$this->view->education = $userData->education;
		if($userData->certificate == null)
			$this->view->certificate = array();
		else
			$this->view->certificate = $userData->certificate;
		$advisorList = $this->getAdvisorList();
    	$this->view->advisorList = $advisorList;

		$userJob = array();
		foreach ($userData->companyWork as $key => $value) {

			$companyData = json_decode($firebase->get("companies/".$value->companyId));

			$userWork = array();
			foreach ($value->userWork as $subKey => $subValue) {
				$jobData = json_decode($firebase->get("jobs/".$subValue));
				array_push($userWork, $jobData->name);
			}
			$userJob[$value->endWork] = array("companyName" => $companyData->name,"companyId" => $value->companyId,"startWork" => $value->startWork,"endWork" => $value->endWork,"jobs" => $userWork,"companyProfileKey" => $key);
			// array_push($userJob, array("companyName" => $companyData->name,"companyId" => $value->companyId,"startWork" => $value->startWork,"endWork" => $value->endWork,"jobs" => $userWork,"companyProfileKey" => $key));

		}
		krsort($userJob);
		$this->view->userJob = $userJob;

	}

	function userAction()
	{
		$userData = $this->session->get("userData");

		$this->assets
		->addCss('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')
		->addCss('public/assets/pages/css/profile-2.min.css');

		$this->assets
		->addJs('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
  	// ->addJs('http://maps.google.com/maps/api/js?sensor=false')
  	// ->addJs('public/assets/global/plugins/gmaps/gmaps.min.js');

    $this->view->webtitle = $userData->webtitle;
		$this->view->topic = $userData->topic;
		$this->view->fname = $userData->fname;
		$this->view->lname = $userData->lname;
		$this->view->email = $userData->email;
		$this->view->gender = $userData->gender;
		$this->view->address = $userData->address;
		$this->view->city = $userData->city;
		$this->view->phone = $userData->phone;
		$this->view->facebook = $userData->facebook;
		$this->view->studentId = $userData->studentId;
		$this->view->entryYear = $userData->entryYear;
		$this->view->graduateYear = $userData->graduateYear;
		$this->view->advisor = $userData->advisor;
	}
	public function userProfileAction()
  {
	  	$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new

  		$get = $this->request->get();
        $this->view->webtitle = $get['studentId'];
        $firebase = $this->connect_firebase();
        $uid = hash('sha256', $get['studentId']);
		$userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid)); //change
		if($userData == Null)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบุคคลที่ท่านค้นหา</h2>";
		}else
		{
		$this->assets
		->addCss('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')
		->addCss('public/assets/pages/css/profile-2.min.css');

		$this->assets
		->addJs('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
    	// ->addJs('http://maps.google.com/maps/api/js?sensor=false')
    	// ->addJs('public/assets/global/plugins/gmaps/gmaps.min.js');

    	$engGear = (int)substr($userData->studentId,0,2)-12;
    	$this->view->webtitle = $userData->studentId;
		$this->view->topic = $userData->studentId;
		$this->view->fname = $userData->fname;
		$this->view->lname = $userData->lname;
		$this->view->efname = $userData->efname;
		$this->view->elname = $userData->elname;
		$this->view->nickname = $userData->nickname;
		$this->view->gender = $userData->gender;
		$this->view->address = $userData->address;
		$this->view->city = $userData->city;
		$this->view->phone = $userData->phone;
		$this->view->facebookUid = $userData->facebookUid;
		$this->view->studentId = $userData->studentId;
		$this->view->entryYear = $userData->entryYear;
		$this->view->graduateYear = $userData->graduateYear;
		$this->view->advisor = $userData->advisor;
		$this->view->advisorName = json_decode($firebase->get("advisor/".$userData->advisor."/name"));
		$this->view->imgProfile = $userData->imgProfile;
		$this->view->alumniType = $userData->alumniType;
		$this->view->engGear = $engGear;
    $this->view->gen = $userData->gen;
		if($userData->education == null)
			$this->view->education = array();
		else
			$this->view->education = $userData->education;
		if($userData->certificate == null)
			$this->view->certificate = array();
		else
			$this->view->certificate = $userData->certificate;

		$userJob = array();
		foreach ($userData->companyWork as $key => $value) {

			$companyData = json_decode($firebase->get("companies/".$value->companyId));

			$userWork = array();
			foreach ($value->userWork as $subKey => $subValue) {
				$jobData = json_decode($firebase->get("jobs/".$subValue));
				array_push($userWork, $jobData->name);
			}
			$userJob[$value->endWork] = array("companyName" => $companyData->name,"companyId" => $value->companyId,"startWork" => $value->startWork,"endWork" => $value->endWork,"jobs" => $userWork,"companyProfileKey" => $key);
			// array_push($userJob, array("companyName" => $companyData->name,"companyId" => $value->companyId,"startWork" => $value->startWork,"endWork" => $value->endWork,"jobs" => $userWork,"companyProfileKey" => $key));

		}
		krsort($userJob);
		$this->view->userJob = $userJob;
		}
  }

	function getidAction() {
		$stdcode = $this->session->get("userData");
		$stdyear =  substr($stdcode->studentId,0,2);
		var_dump($stdyear);

		$firebase = $this->connect_firebase();
		$list = json_decode($firebase->get("alumnus/".$stdyear,array('print' => 'pretty','orderBy' => '"fname"')));
		var_dump($list);
		exit();
	}

	function alumniAction()
	{
		//$this->view->webtitle = "alumnus";
		$this->view->webtitle = "รายชื่อนักศึกษาเก่าในรุ่น";
		$this->view->topic_header = "รายชื่อนักศึกษาเก่าในรุ่น";
		$this->view->tableattr = ["","รหัสนักศึกษา","ชื่อ-นามสกุล","ชื่อเล่น","อาจารย์ที่ปรึกษา","สถานที่ทำงาน","ประเภทของงาน","Facebook"];
		$this->view->tableattrall = ["รหัสนักศึกษา","ชื่อ-นามสกุล","ชื่อเล่น"];

		$stdcode = $this->session->get("userData");
		$stdyear =  substr($stdcode->studentId,0,2);
		$year = $stdyear + 2500;

		$this->assets
		->addCss('public/assets/global/plugins/datatables/datatables.min.css')
		->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
		->addCss('public/css/tableconfig.css');

		$this->assets
		->addJs('public/assets/global/scripts/datatable.js')
		->addJs('public/assets/global/plugins/datatables/datatables.min.js')
		->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
		->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');

		$userData = $this->session->get("userData");
		$yearStart = $userData->entryYear;
		$firebase = $this->connect_firebase();
		//$usersDataAll = json_decode($firebase->get("alumnus/".$stdyear,array('print' => 'pretty','orderBy' => '"fname"')));
		$usersData = json_decode($firebase->get('alumnus/'.$stdyear)); //change
		$advisorAll = (array)json_decode($firebase->get("advisor/"));
		$companyWorkAll = (array)json_decode($firebase->get("companies/"));
		$jobAll = (array)json_decode($firebase->get("jobs/"));
		$userDatasView = $this->processToUserDatasView($allUid,$usersData,$advisorAll,$companyWorkAll,$jobAll);
		//$userDatasViewAll = $this->processToUserDatasView($usersDataAll);
    	$this->view->userDatasView = $userDatasView;
		//$this->view->userDatasViewAll = $userDatasViewAll;

	}

	function statAction()
	{
		$this->view->webtitle = "สถิติ";
		$t = time();
		$rand =  rand(0,$t);

		$this->assets
		->addJs('public/js/highchart/code/highcharts.src.js')
		->addJs('public/js/chartfile-h.js')
		->addJs('public/js/alumnusstat.js');

		$firebase = $this->connect_firebase();

		/*get amount of alumni*/
    	$bd_cpe = 0; $bd_cpes = 0;$md_cpe = 0; $phd_cpe = 0;$bd_isne = 0;
    	$i = 0;
		$alumnuslist = array();

		$jobsfield = array();
		$regionData = array();
   	 	$usersData = json_decode($firebase->get('alumnus/'));
   	 	foreach ($usersData as $key => $value) {
   	 	  $gradstd[$i] = new StdClass();
   	 	  array_push($alumnuslist, $value);
   	 	  //echo "CPE : ".($i+1)." / Student : ".count((array)$alumnuslist[$i]);
   	 	  $gradstd[$i]->year = ($i+2535);
   	 	  $gradstd[$i]->cpe = count((array)$alumnuslist[$i]);
   	 	  $gradstd[$i]->isne = 0;
	
	   	 	  // $allstd += count((array)$alumnuslist[$i]);
   	 	  $i++;
   	 	  //var_dump($value);
   	 	  foreach ($value as $key => $stdinfo) {
   	 	    //var_dump($stdinfo->studentId);
   	 	    $year = substr($stdinfo->studentId,0,2);
   	 	    if($year > 48)
   	 	    {
   	 	      $degree  = substr($stdinfo->studentId,3,2);
   	 	      if($degree == 61) {
   	 	        $id = substr($stdinfo->studentId,5,1);
   	 	        if($id == 0)        $bd_cpe++;
   	 	        else if($id == 1)   $bd_isne++;
   	 	        else                $bd_cpes++;
   	 	      }
   	 	      else if($degree == 63)  $md_cpe++;
   	 	      else                    $phd_cpe++;
   	 	    }else $bd_cpe++;

   	 	    if(count((array)$stdinfo) > 5){
				array_push($regionData, $stdinfo->city);
				$data = $stdinfo->companyWork;
				if(count((array)$data) != 0)
				{
					foreach ($data as $companykey => $value) {
						foreach ($value->userWork as $jobskey => $jobsname) {
							$jobs = json_decode($firebase->get("jobs/".$jobsname, array('print' => 'pretty')));
							array_push($jobsfield, $jobs->name);
						}
					}
				}
			}
   	 	  }
   	 	}
	
    	$jobsfield = array_count_values($jobsfield);
    	$regionData = array_count_values($regionData);
    	//get company category
    	//get companyId
   	 	$company = array();
   	 	$companyWorksname = array();
		$companiesData = json_decode($firebase->get("companies/"));
	   	// $std = json_decode($firebase->get("companies/",array('print' => 'pretty')));
   	 	foreach ($companiesData as $key => $value) {
   	 	  array_push($company, $value);
   	 	  foreach ($value->work as $key => $workkey) {
    	    $worksname = json_decode($firebase->get("companyWorks/".$workkey, array('print' => 'pretty')));
    	    array_push($companyWorksname, $worksname->name);
    	  }
   	 	}
	
    	$companyWorksname = array_count_values($companyWorksname);
		$this->view->stdamount = json_encode($gradstd);
		$this->view->workfield = json_encode($jobsfield);
		$this->view->region = json_encode($regionData);
		$this->view->companywork = json_encode($companyWorksname);

	}

	// function testworkAction()
	// {
	// 	$firebase = $this->connect_firebase();

	// 	$jobsfield = array();
	// 	$usres = json_decode($firebase->get('users/',array("shallow" => "true")));
	// 	foreach ($usres as $key => $value) {
	// 		$data = json_decode($firebase->get('users/'.$key."/companyWork/",array('print' => 'pretty','orderBy' => '"startWork"','limitToFirst' => 1)));
	// 		if(count((array)$data) != 0)
	// 		{
	// 			foreach ($data as $companykey => $value) {
	// 				foreach ($value->userWork as $jobskey => $jobsname) {
	// 					$jobs = json_decode($firebase->get("jobs/".$jobsname, array('print' => 'pretty')));
	// 					array_push($jobsfield, $jobs);
	// 				}
	// 			}
	// 		}
	// 	}
	// 	var_dump($jobsfield);
	// 	exit();
	// }

	function regionAction()
	{
		$firebase = $this->connect_firebase();
		/*get amount of alumni*/
		$regionData = array();

		$usersData = json_decode($firebase->get("alumnus/",array("shallow" => true))); //change
		foreach ($usersData as $key => $value) {
				array_push($regionData, $value->city);
		}
		print_r($regionData);
		$regionData = array_count_values($regionData);
		print_r(json_encode($regionData));

		exit();
		//$this->view->region = json_encode($regionData);
	}

	function logoutAction()
	{
		$this->session->remove("studentId");
		$this->session->remove("facebookUid");
		$this->session->remove("userData");
		$this->response->redirect('');
	}

	function companyAction()
	{
		$this->view->webtitle = "รายชื่อบริษัท";
		$this->view->topic_header = "รายชื่อบริษัท";

		$this->assets
		->addCss('public/assets/global/plugins/datatables/datatables.min.css')
		->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
		->addCss('public/css/tableconfig.css');

		$this->assets
		->addJs('public/assets/global/scripts/datatable.js')
  		->addJs('public/assets/global/plugins/datatables/datatables.min.js')
  		->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
		->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');

		$this->view->tableattr = ["","ชื่อบริษัท","จังหวัด","ประเภทงาน","เว็บไซต์","เบอร์โทรศัพท์"];

		$firebase = $this->connect_firebase();
		$companies = json_decode($firebase->get("companies"));
		$data = array();
		foreach ($companies as $key => $value) {
			$companyWorks = array();
			foreach ($value->work as $subKey => $subValue) {
				$companyWork = json_decode($firebase->get("companyWorks/".$subValue));
				array_push($companyWorks, array("name" => $companyWork->name,"key" => $subValue));
			}
		array_push($data, array("name" => $value->name,"phone" => $value->phone,"website" => $value->website,"city" => $value->city,"id"=>$key,"work"=>$companyWorks));

		}
		$this->view->data = $data;
	}

	function editProfileAction()
	{
		// $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
		$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
		$post = $this->request->getPost();
		
		//Profile
        $firebase = $this->connect_firebase();
        $userData = $this->session->get("userData");
        $uid = hash('sha256', $userData->studentId);
        $lastUpdate = time();
        foreach ($post as $key => $value) {
        	if($key=="fbook")
        		$key="facebook";
        	$firebase->set('alumnus/'.$stdyear.'/'.$uid."/".$key,$value);		//change
        }
        $firebase->set('alumnus/'.$stdyear.'/'.$uid."/lastUpdate",$lastUpdate);	//change
        $this->response->redirect('main/home#tab_1_3');


	}

	public function editImgProfileAction()
    {
        // $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
		$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new

        $post = $this->request->getPost();
        $imgUrl = $post['url1']."%".$post['url2']."&token=".$post['token'];
        $userData = $this->session->get("userData");
        $uid = hash('sha256', $userData->studentId);
        $firebase = $this->connect_firebase();
        $firebase->set('alumnus/'.$stdyear."/".$uid."/imgProfile",$imgUrl);	//change
    }

    public function resetPasswoedAction()
    {
        // $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);
        $password = $post['password'];
        $salt = $this->generateRandomString(40);
        $newPassword = $this->hash_salt_generate($password,$salt);
        $firebase = $this->connect_firebase();
        $firebase->set('alumnus/'.$stdyear.'/'.$uid."/password",$newPassword);	//change
        $firebase->set('alumnus/'.$stdyear.'/'.$uid."/salt",$salt);				//change
        $this->response->redirect('main/home');
    }
    public function addCompanyAction()
    {
        // $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $this->view->webtitle = "เพิ่มบริษัทที่เคยทำงาน";
        $this->view->topic_header = "เพิ่มบริษัทที่เคยทำงาน";
        $firebase = $this->connect_firebase();

        $this->view->listJobs = $this->getJobList();
        $this->view->listCompanyWorks = $this->getCompanyWorkList();

    }
    public function addedCompanyAction()
    {
        // $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $firebase = $this->connect_firebase();
        $post = $this->request->getPost();
        $userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);
        $lastUpdate = time();
        //Companu Profile
        $companyName = $post['companyName'];
        $companyAddress = $post['companyAddress'];
        $companyCity = $post['companyCity'];
        $companyPhone = $post['companyPhone'];
        $companyWebsite = $post['companyWebsite'];
        $companyWork = $post['companyWork'];
        $companyFacebook = $post['companyFacebook'];
        $userWork = $post['userWork'];
        $startWork = $post['startWork'];
        $endWork = $post['endWork'];

        //Other
        if($post['OtherUserWork'] != ""){
        	$OtherUserWork = $post['OtherUserWork'];
        	 $firebase->push("other/jobs",array("name" => $OtherUserWork));
        }
        if($post['OtherCompanyWork'] != ""){
        	$OtherCompanyWork = $post['OtherCompanyWork'];
        	$firebase->push("other/companyWorks",array("name" => $OtherCompanyWork));
        }



        //Check Company
        $companyToCheck = (array)json_decode($firebase->get("companies/", array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$companyName.'"')));
        if(count($companyToCheck)==0)
        {
            //Company Data
            $companyData = array(
                "address" => $companyAddress,
                "city" => $companyCity,
                "name" => $companyName,
                "phone" => $companyPhone,
                "website" => $companyWebsite,
                "work" => $companyWork,
                "facebook" => $companyFacebook,
                "lastUpdate" => $lastUpdate
            );
            //Add Company in Firebase
            $companyId = json_decode($firebase->push('companies/', $companyData))->name;
            $firebase->push('alumnus/'.$stdyear.'/'.$uid."/companyWork",array("companyId" => $companyId,"companyName" => $companyName,"userWork" => $userWork,"startWork" => $startWork,"endWork" => $endWork));
        }else	//change---------^
        {
            foreach ($companyToCheck as $key => $value) {
                $firebase->push('alumnus/'.$stdyear.'/'.$uid."/companyWork",array("companyId" => $key,"companyName" => $companyName,"userWork" => $userWork,"startWork" => $startWork,"endWork" => $endWork));
            }	//change-----------^
        }

        $this->response->redirect('main/home#tab_1_3');

    }

    public function editCompanyAction()
    {
    	// $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
    	$firebase = $this->connect_firebase();
    	$get = $this->request->get();
    	$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);
        $userCompanyWork = json_decode($firebase->get("alumnus/".$stdyear.'/'.$uid.'/companyWork/'.$get['key']));
        if($userCompanyWork == Null)
		{
			$this->view->disableLevel(
			    View::LEVEL_ACTION_VIEW
			);
			echo "<h2>ไม่พบข้อมูลบริษัท</h2>";
		}else
		{
			$this->session->set("companyEditId",$userCompanyWork->companyId);
			$this->session->set("usersCompanyWork",$get['key']);
			$companyData = json_decode($firebase->get("companies/".$userCompanyWork->companyId));
			$companyWorks = array();
			foreach ($companyData->work as $key => $value) {
				$work = json_decode($firebase->get("companyWorks/".$value));
				array_push($companyWorks, $work->name);
			}
			$jobWork = array();
			foreach ($userCompanyWork->userWork as $key => $value) {
				$job = json_decode($firebase->get("jobs/".$value));
				array_push($jobWork, $job->name);
			}
			$this->view->companyName = $companyData->name;
			$this->view->companyAddress = $companyData->address;
			$this->view->companyCity = $companyData->city;
			$this->view->userWork = $jobWork;
			// $this->view->companyWork = $companyWorks;
			$this->view->startWork = $userCompanyWork->startWork;
			$this->view->endWork = $userCompanyWork->endWork;
			$this->view->companyPhone = $companyData->phone;
			$this->view->companyWebsite = $companyData->website;
			$this->view->companyFacebook = $companyData->facebook;
			$this->view->companyWork = $companyData->work;

        	$this->view->webtitle = "แก้ไขบริษัทที่เคยทำงาน";
        	$this->view->topic_header = "เแก้ไขบริษัทที่เคยทำงาน";
       	
        	$this->view->listJobs = $this->getJobList();
        	$this->view->listCompanyWorks = $this->getCompanyWorkList();
        	$init = 2535;
			$selectyear = [];
			$current = date("Y") + 543;
			for($count = $init; $count < $current; $count++) {
				array_push($selectyear,$count);
			}
			$this->view->yearlist = $selectyear;

		}
    	
    }

    public function editedCompanyAction()
    {
    	$companyEditId = $this->session->get("companyEditId");
		$usersCompanyWork = $this->session->get("usersCompanyWork");
		$post = $this->request->getPost();
		$firebase = $this->connect_firebase();
		$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);

        $firebase->set("alumnus/".$stdyear.'/'.$uid.'/companyWork/'.$usersCompanyWork.'/endWork',$post['endWork']);
        $firebase->set("alumnus/".$stdyear.'/'.$uid.'/companyWork/'.$usersCompanyWork.'/startWork',$post['startWork']);
        $firebase->set("alumnus/".$stdyear.'/'.$uid.'/companyWork/'.$usersCompanyWork.'/userWork',$post['userWork']);

        $firebase->set("companies/".$companyEditId."/name",$post['companyName']);
        $firebase->set("companies/".$companyEditId."/address",$post['companyAddress']);
        $firebase->set("companies/".$companyEditId."/city",$post['companyCity']);
        $firebase->set("companies/".$companyEditId."/work",$post['companyWork']);
        $firebase->set("companies/".$companyEditId."/website",$post['companyWebsite']);
        $firebase->set("companies/".$companyEditId."/facebook",$post['companyFacebook']);
        $firebase->set("companies/".$companyEditId."/phone",$post['companyPhone']);

        $this->session->remove("companyEditId");
        $this->session->remove("usersCompanyWork");
        $this->response->redirect('main/home#tab_1_3');

    }

    public function deleteCompanyAction()
    {
    	$firebase = $this->connect_firebase();
        $get = $this->request->get();
        $userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);
        $firebase->delete('alumnus/'.$stdyear.'/'.$uid."/companyWork/".$get['key']); //change
        $this->response->redirect('main/home#tab_1_3');

    }

    public function addEducationAction()
    {
    	$this->view->webtitle = "เพิ่มประวัติการศึกษา";
		$this->view->topic_header = "เพิ่มประวัติการศึกษา";


    }

    public function addedEducationAction()
    {
    	$firebase = $this->connect_firebase();
    	$post = $this->request->getPost();
    	if($post['course'] == "" || $post['major'] == "" || $post['university'] == "" || $post['startStudy'] == "" || $post['endStudy'] == "" )
    	{
    		$this->view->webtitle = "Error";
    		$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>กรอกข้อมูลไม่ครบ กรุณาตรวจสอบข้อมูล</h2> <a href='addEducation'>กลับ</a>";
    	}else
    	{
    		$majorData = json_decode($firebase->get("major/",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$post['major'].'"')));
    		if(count((array)$majorData) == 0)
    		{
    			$firebase->push("major",array("name" => $post['major'],"status" => true));
    		}
    		$universityData = json_decode($firebase->get("university/",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$post['university'].'"')));
    		if(count((array)$universityData) == 0)
    		{
    			$firebase->push("university",array("name" => $post['university'],"status" => true));
    		}
    		$userData = $this->session->get("userData");
			$stdyear = substr($userData->studentId,0,2); //new
    		$uid = hash('sha256', $userData->studentId);
    		$firebase->push('alumnus/'.$stdyear.'/'.$uid."/education",array(	//change
    				"course" => $post['course'],
    				"major" => $post['major'],
    				"university" => $post['university'],
    				"startStudy" => $post['startStudy'],
    				"endStudy" => $post['endStudy']
    			));
    		$this->response->redirect('main/home#tab_1_3');
    	}

    }

    public function editEducationAction()
    {
    	$firebase = $this->connect_firebase();
    	$get = $this->request->get();
		$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);
        $userEducation= json_decode($firebase->get("alumnus/".$stdyear.'/'.$uid.'/education/'.$get['key']));
        if($userEducation == Null)
		{
			$this->view->disableLevel(
			    View::LEVEL_ACTION_VIEW
			);
			echo "<h2>ไม่พบข้อมูล</h2>";
		}else
		{
			$this->view->webtitle = "แก้ไขข้อมูลการศึกษา";
        	$this->view->topic_header = "แก้ไขข้อมูลการศึกษา";
        	$this->session->set("educationEditId",$get['key']);
        	$course = ['ตรี','โท','เอก'];
        	$courseOption = [];
        	foreach ($course as $key => $value) {
        		if($userEducation->course == $value)
        		{
        			array_push($courseOption, "<option value=".$value." selected>".$value."</option>");
        		}else
        		{
        			array_push($courseOption, "<option value=".$value.">".$value."</option>");
        		}
        	}
        	$init = 2535;
			$selectyear = [];
			$current = date("Y") + 543;
			for($count = $init; $count < $current; $count++) {
				array_push($selectyear,$count);
			}
			$this->view->yearlist = $selectyear;
        	$this->view->courseOption = $courseOption;
        	$this->view->major = $userEducation->major;
        	$this->view->startStudy = $userEducation->startStudy;
        	$this->view->endStudy = $userEducation->endStudy;
        	$this->view->university = $userEducation->university;
		}
    }

    public function editedEducationAction()
    {
    	$post = $this->request->getPost();
    	$educationEditId = $this->session->get("educationEditId");
    	$firebase = $this->connect_firebase();
    	$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/education/'.$educationEditId.'/course',$post['course']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/education/'.$educationEditId.'/endStudy',$post['endStudy']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/education/'.$educationEditId.'/major',$post['major']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/education/'.$educationEditId.'/startStudy',$post['startStudy']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/education/'.$educationEditId.'/university',$post['university']);
    	$this->session->remove("educationEditId");
    	$this->response->redirect('main/home#tab_1_3');

    }

    public function deleteEducationAction()
    {
    	$firebase = $this->connect_firebase();
        $get = $this->request->get();
        $userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);
        $firebase->delete('alumnus/'.$stdyear.'/'.$uid."/education/".$get['key']);	//change
        $this->response->redirect('main/home#tab_1_3');

    }

    public function addCerAction()
    {
    	$this->view->webtitle = "เพิ่มประกาศนียบัตร";
		$this->view->topic_header = "เพิ่มประกาศนียบัตร";
    }

    public function addedCerAction()
    {
    	$firebase = $this->connect_firebase();
    	$post = $this->request->getPost();
    	if($post['certifier'] == "" || $post['certificate'] == "" || $post['receipt'] == "" )
    	{
    		$this->view->webtitle = "Error";
    		$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>กรอกข้อมูลไม่ครบ กรุณาตรวจสอบข้อมูล</h2> <a href='addCer'>กลับ</a>";
    	}else
    	{
    		$certificateData = json_decode($firebase->get("certificate/",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$post['certificate'].'"')));
    		if(count((array)$majorData) == 0)
    		{
    			$firebase->push("certificate",array("name" => $post['certificate'],"status" => true));
    		}
    		$certifierData = json_decode($firebase->get("certifier/",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$post['certifier'].'"')));
    		if(count((array)$certifierData) == 0)
    		{
    			$firebase->push("certifier",array("name" => $post['certifier'],"status" => true));
    		}
    		$userData = $this->session->get("userData");
			$stdyear = substr($userData->studentId,0,2); //new
    		$uid = hash('sha256', $userData->studentId);
    		$firebase->push('alumnus/'.$stdyear.'/'.$uid."/certificate",array(	//change
    				"certifier" => $post['certifier'],
    				"certificate" => $post['certificate'],
    				"receipt" => $post['receipt']
    			));
    		$this->response->redirect('main/home#tab_1_3');
    	}

    }

    public function editCerAction()
    {
    	$firebase = $this->connect_firebase();
    	$get = $this->request->get();
		$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);
        $userCer= json_decode($firebase->get("alumnus/".$stdyear.'/'.$uid.'/certificate/'.$get['key']));
        if($userCer == Null)
		{
			$this->view->disableLevel(
			    View::LEVEL_ACTION_VIEW
			);
			echo "<h2>ไม่พบข้อมูล</h2>";
		}else
		{
			$this->view->webtitle = "แก้ไขข้อมูลประกาศนียบัตร";
        	$this->view->topic_header = "แก้ไขข้อมูลประกาศนียบัตร";
        	$this->session->set("cerEditId",$get['key']);
			$init = 2535;
			$selectyear = [];
			$current = date("Y") + 543;
			for($count = $init; $count < $current; $count++) {
				array_push($selectyear,$count);
			}
			$this->view->yearlist = $selectyear;
        	$this->view->certificate = $userCer->certificate;
        	$this->view->certifier = $userCer->certifier;
        	$this->view->receipt = $userCer->receipt;
		}
    }

    public function editedCerAction()
    {
    	$post = $this->request->getPost();
    	$cerEditId = $this->session->get("cerEditId");
    	$firebase = $this->connect_firebase();
    	$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/certificate/'.$cerEditId.'/certificate',$post['certificate']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/certificate/'.$cerEditId.'/certifier',$post['certifier']);
    	$firebase->set("alumnus/".$stdyear.'/'.$uid.'/certificate/'.$cerEditId.'/receipt',$post['receipt']);
    	$this->session->remove("cerEditId");
    	$this->response->redirect('main/home#tab_1_3');

    }

    public function deleteCerAction()
    {
    	$firebase = $this->connect_firebase();
        $get = $this->request->get();
        $userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
        $uid = hash('sha256', $userData->studentId);
        $firebase->delete('alumnus/'.$stdyear.'/'.$uid."/certificate/".$get['key']); // change
        $this->response->redirect('main/home#tab_1_3');

    }

    public function listCompanyAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $firebase = $this->connect_firebase();
        $listCompany = array();
        $companData = json_decode($firebase->get("companies/", array('print' => 'pretty')));
        foreach ($companData as $key => $value) {
            array_push($listCompany, $value->name);
        }
        echo json_encode($listCompany);
        exit();
    }

    public function checkCompanyAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $post = $this->request->getPost();
        $firebase = $this->connect_firebase();

        $companyName = $post['name'];
        $companData = json_decode($firebase->get("companies/", array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$companyName.'"')));
        foreach ($companData as $key => $value) {
            echo json_encode($value);
        }
        exit();
        // echo $companyName;

    }

    function companyprofileAction()
    {
    	$get = $this->request->get();
    	$id = $get['id'];
    	$firebase = $this->connect_firebase();
    	$companyData = json_decode($firebase->get("companies/".$id));
    	if($companyData == Null)
    	{
    		$this->view->webtitle = "ไม่พบบริษัทที่ที่ท่านค้นหา";
    		$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบริษัทที่ที่ท่านค้นหา</h2>";
    	}else
    	{
    		$this->view->webtitle = $companyData->name;
        	// $this->view->topic_header = "รายชื่อบริษัท";
        	$this->assets
            ->addCss('public/css/tagsconfig.css');


            $this->view->name = $companyData->name;
            $this->view->address = $companyData->address;
						$this->view->city = $companyData->city;
            $this->view->phone = $companyData->phone;
            $this->view->website = $companyData->website;
						$this->view->facebook = $companyData->facebook;
            $companyWorks = array();
            foreach ($companyData->work as $key => $value) {
            	$data = json_decode($firebase->get("companyWorks/".$value));
            	array_push($companyWorks, array("name" => $data->name,"id" => $value));
            }
            $this->view->work = $companyWorks;
    	}


    }

    function profileAction()
    {
    	$get = $this->request->get();
        $this->view->webtitle = $get['studentId'];
		$stdid = $get['studentId'];
		$stdyear = substr($stdid,0,2); //new

        $firebase = $this->connect_firebase();
        $uid = hash('sha256', $get['studentId']);
		$userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid));
		if($userData == Null)
		{
			$this->view->disableLevel(
        	    View::LEVEL_ACTION_VIEW
        	);
        	echo "<h2>ไม่พบบุคคลที่ท่านค้นหา</h2>";
		}else
		{
		$this->assets
		->addCss('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')
		->addCss('public/assets/pages/css/profile-2.min.css')
		->addCss('public/assets/global/plugins/datatables/datatables.min.css')
		->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
		->addCss('public/css/tableconfig.css');

		$this->assets
		->addJs('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')
		->addJs('public/assets/global/scripts/datatable.js')
		->addJs('public/assets/global/plugins/datatables/datatables.min.js')
		->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
		->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');

  	$engGear = (int)substr($userData->studentId,0,2)-12;
  	$this->view->webtitle = $userData->studentId.' '.$userData->fname.' '.$userData->lname;
		$this->view->topic = $userData->studentId;
		$this->view->fname = $userData->fname;
		$this->view->lname = $userData->lname;
		$this->view->efname = $userData->efname;
		$this->view->elname = $userData->elname;
		$this->view->nickname = $userData->nickname;
		$this->view->gender = $userData->gender;
		$this->view->address = $userData->address;
		$this->view->city = $userData->city;
		$this->view->phone = $userData->phone;
		$this->view->facebookUid = $userData->facebookUid;
		$this->view->studentId = $userData->studentId;
		$this->view->entryYear = $userData->entryYear;
		$this->view->graduateYear = $userData->graduateYear;
		$this->view->advisor = $userData->advisor;
		$this->view->advisorName = json_decode($firebase->get("advisor/".$userData->advisor."/name"));
		$this->view->imgProfile = $userData->imgProfile;
		$this->view->alumniType = $userData->alumniType;
		$this->view->gen = $userData->gen;
		$this->view->engGear = $engGear;

		$userJob = array();
		foreach ($userData->companyWork as $key => $value) {

			$companyData = json_decode($firebase->get("companies/".$value->companyId));

			$userWork = array();
			foreach ($value->userWork as $subKey => $subValue) {
				$jobData = json_decode($firebase->get("jobs/".$subValue));
				array_push($userWork, $jobData->name);
			}
			$userJob[$value->endWork] = array("companyName" => $companyData->name,"companyId" => $value->companyId,"startWork" => $value->startWork,"endWork" => $value->endWork,"jobs" => $userWork,"companyProfileKey" => $key);
			// array_push($userJob, array("companyName" => $companyData->name,"companyId" => $value->companyId,"startWork" => $value->startWork,"endWork" => $value->endWork,"jobs" => $userWork,"companyProfileKey" => $key));

		}
		krsort($userJob);
		$this->view->userJob = $userJob;
		}

  }


	function searchAction()
	{
		$this->view->webtitle = "ค้นหา";
		$this->view->topic_header = "ค้นหา";
		$this->view->tableattr = ["","รหัสนักศึกษา","ชื่อ-นามสกุล","ชื่อเล่น","เกียร์","บริษัทที่ทำงาน","ประเภทของงาน","Facebook"];
		$this->assets
		->addCss('public/assets/global/plugins/datatables/datatables.min.css')
		->addCss('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')
		->addCss('public/css/tableconfig.css');

		$this->assets
		->addJs('public/assets/global/scripts/datatable.js')
		->addJs('public/assets/global/plugins/datatables/datatables.min.js')
		->addJs('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
		->addJs('public/assets/pages/scripts/table-datatables-responsive.min.js');
    	$this->view->job = "";
		$this->view->stdname = "";
		$this->view->stdcode = "";
		$this->view->certificate = "";
		$this->view->course = "";
		$this->view->gear = "";
		$this->view->city = "";
		$this->view->advisor = "";
		$this->view->advisorName = " ทุกคน ";

		$firebase = $this->connect_firebase();
		$year = json_decode($firebase->get("alumnus/",array("shallow" => true)));
		$year = count((array)$year) + 35;

		$yearNow = (int)date("Y",time());
		$yearNow = (string)$yearNow+543;
		$yearNow = (string)$yearNow;
		$yearStudent = substr($yearNow,2);
		$yearForSearch = array();
		for ($i=35; $i < $year; $i++) {
		array_push($yearForSearch, $i);
		}
		$this->view->yearStudent = $yearStudent;
		$this->view->yearForSearch = $yearForSearch;
		$firebase = $this->connect_firebase();
		$get = $this->request->get();
		$get['yearEnd'] = $get['yearStart'];
		$allUid = array();
		$usersAllInSearch = array();
		$check = false;
		foreach ($get as $key => $value) {
		if($key == "yearStart")
		{
			if($get['yearStart'] != null && $get['yearStart'] != "")
			{

			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInYear = $this->searchByYear($allUid,$get['yearStart'],$get['yearEnd']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInYear;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInYear);
			// $usersAllInSearch += $userInJob;
			$check = true;
			}
		}
		else if($key == "gear")
		{
			if($get['gear'] != null && $get['gear'] != "")
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInGen = $this->searchGen($get['gear']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInGen;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInGen);
			// $usersAllInSearch += $userInGen;
			$this->view->gear = $get['gear'];
			$check = true;
			}
		}
		else if($key == "type")
		{
			if($get['type'] != null && $get['type'] != "" )
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInType = $this->searchType($allUid,$get['type']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInType;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInType);
			$check = true;
			}

		}else if($key == "advisor")
		{
			if($get['advisor'] != null && $get['advisor'] != "" )
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInAdvisor = $this->searchAdvisor($allUid,$get['advisor']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInAdvisor;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInAdvisor);
			// $usersAllInSearch += $userInAdvisor;
			$this->view->advisor = "";
			// $this->view->advisorName = json_decode($firebase->get("advisor/".$get['advisor']."/"))->name;
			$this->view->advisorName = " ทุกคน ";
			$check = true;
			}
		}else if ($key == "certificate")
		{
			if($get['certificate'] != null && $get['certificate'] != "")
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInCertificate = $this->searchCertificate($allUid,$get['certificate']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInCertificate;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInCertificate);
			$check = true;
			}
		}else if ($key == "stdname")
		{
			if($get['stdname'] != null && $get['stdname'] != "")
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInStdname = $this->searchStdname($allUid,$get['stdname']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInStdname;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInStdname);
			$this->view->stdname = $get['stdname'];
			$check = true;
			}
		}
		else if ($key == "stdcode")
		{
			if($get['stdcode'] != null && $get['stdcode'] != "")
			{
			if($allUid == null)
				$allUid = $this->allUid($get['yearStart'],$get['yearEnd']);

			$userInStdcode = $this->searchStdcode($get['stdcode'],$allUid,$get['yearStart'],$get['yearEnd']);
			if(count($usersAllInSearch)==0 && !$check)
				$usersAllInSearch += $userInStdcode;
			else
				$usersAllInSearch=array_intersect_key($usersAllInSearch,$userInStdcode);
			$this->view->stdcode = $get['stdcode'];
			$check = true;
			}
		}
		}
		$usersData = array();
		$userData = $this->session->get("userData");
		$stdyear = substr($userData->studentId,0,2); //new
		foreach ($usersAllInSearch as $uid => $value) {
			for ($i=$get['yearStart']; $i <= $get['yearEnd']; $i++) { 
      		 	$userData = json_decode($firebase->get('alumnus/'.$i.'/'.$uid));
      		 	if(count((array)$userData) != 0)
      		 	{
      		 		$usersData[$uid] = $userData;
      		 		break;
      		 	}
      		}
		}
		$advisorAll = (array)json_decode($firebase->get("advisor/"));
		$companyWorkAll = (array)json_decode($firebase->get("companies/"));
		$jobAll = (array)json_decode($firebase->get("jobs/"));
		$userDatasView = $this->processToUserDatasView($allUid,$usersData,$advisorAll,$companyWorkAll,$jobAll);
			$this->view->userDatasView = $userDatasView;
			$advisorList = json_decode($firebase->get("advisor",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
			$this->view->advisorList = $advisorList;

	}

}
