<?php
use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;

class FacebookController extends ControllerBase
{
	public function initialize()
    {

    }

    public function indexAction()
    {

    	$this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $this->view->getTitle = "Welcome";
        $facebookUid = $this->request->get('uid');
        $firebase = $this->connect_firebase();
        $facebookData = (array)json_decode($firebase->get("facebookAccount/", array('print' => 'pretty','orderBy' => '"facebookUid"','equalTo' => '"'.$facebookUid.'"')));
        foreach ($facebookData as $key => $value) {
            $facebookData = $value;
        }
        if(count($facebookData) == 0)
        {
            $this->session->set("facebookUid", $facebookUid);
            $this->response->redirect('authentication/facebookAuth');
        }else
        {
            $stdyear = substr($facebookData->studentId,0,2); //new
            $uid = hash('sha256', $facebookData->studentId);
            $userData = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid)); //change
            $this->session->set("userData", $userData);
            $this->response->redirect('main/home'); 
        }

    }

}
