<?php
require_once ('firebase/firebaseLib.php');
require_once ('Facebook/Facebook.php');
use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Url;

class ControllerBase extends Controller
{
	   public function initialize()
    {
    }

    public function authen($username,$password)
    {

    }
    public function connect_firebase()
    {
        $firebase = new \firebase\FirebaseLib('https://cpe-alumni.firebaseio.com/','JkzyvOuQ3wVnueqsprYiCR2Xk2wXJtQg9Ltv5rLS');
        return $firebase;
    }
    public function facebook()
    {
        $fb = new \Facebook\Facebook([
          'app_id' => '1902059640064554',
          'app_secret' => '5694086e90e718ffbc025ca44e14e956',
          'default_graph_version' => 'v2.9',
          //'default_access_token' => '{access-token}', // optional
        ]);
        return $fb;

    }
    public function generateRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function hash_salt_generate($password,$salt)
    {
      $password = hash('sha256', $password);
      $salt = hash('sha256', $salt);
      $hash_salt = hash('sha256', $password.$salt);
      return $hash_salt;
    }
    public function searchInFirebase($url,$key,$word)
    {
      $firebase = $this->connect_firebase();
      $data = json_decode($firebase->get($url, array('print' => 'pretty','orderBy' => '"'.$key.'"','equalTo' => '"'.$word.'"')));
      return $data;
    }


    public function addAdminlog($message)
    {
      $adminData = $this->session->get("adminData");
      $timestamp = time();
      $adminUid = hash('sha256', $adminData->username);
      $firebase = $this->connect_firebase();
      $firebase->push("adminLog/".$adminUid,array("time" => $timestamp*-1,"message" => $message));
    }
    public function baseUrl()
    {
        return "/";
    }

    public function allUid($yearStart,$yearEnd)
    {
      $firebase = $this->connect_firebase();
      $users = array();
      for ($i=$yearStart; $i <= $yearEnd; $i++) { 
        $data = json_decode($firebase->get('alumnus/'.$i));
        if($data != null)
        {
          foreach ($data as $uid => $value) {
            $users[$uid] = $value;
          }
        }
      }
      return $users;
    }

    public function searchByYear($allUid,$yearStart,$yearEnd)
    {
      $usersUid = array();
      foreach ($allUid as $uid => $value) {
        $usersUid[$uid] = $uid;
      }
      return $usersUid;
    }

    public function searchGen($keyword)
    { 
      $userData = $this->session->get("userData");  //new
      $stdyear = substr($userData->studentId,0,2); //new
      $year = (string)$keyword+12;
      $firebase = $this->connect_firebase();
      $usersUid = array();
      // $data = json_decode($firebase->get('alumnus/',array('print' => 'pretty','orderBy' => '"entryYear"','equalTo' => '"'.$year.'"')));  //change
      $data = json_decode($firebase->get('alumnus/'.$year,array('shallow' => 'true')));
        if($data != null)
        {
          foreach ($data as $uid => $value) {
            $usersUid[$uid] = $uid;
          }
        }
        return $usersUid;
    }

    public function searchType($allUid,$keyword)
    { 
      $usersUid = array();
      foreach ($allUid as $uid => $value) {
        if(count((array)$value) > 5){
          if($value->alumniType == $keyword){
            $usersUid[$uid] = $uid;
          }
        }
      }
      return $usersUid;
    }

    public function searchAdvisor($allUid,$keyword)
    { 
      $usersUid = array();
      foreach ($allUid as $uid => $value) {
        if(count((array)$value) > 5){
          if($value->advisor == $keyword){
            $usersUid[$uid] = $uid;
          }
        }
      }
      return $usersUid;
    }

    public function searchCertificate($allUid,$keyword)
    {
      $userData = $this->session->get("userData");  //new
      $stdyear = substr($userData->studentId,0,2); //new
      $firebase = $this->connect_firebase();
      $usersUid = array();

      foreach ($allUid as $uid => $value) {
        if(count((array)$value) > 5){
          foreach ($value->certificate as $key => $certificateValue) {
            if($certificateValue->certificate == $keyword){
              $usersUid[$uid] = $uid;
            }
          }
        } 
      }
      return $usersUid;
    }

    public function searchStdname($allUid,$name)
    {
      $nameSplit = explode(" ", $name);
      $fname = $nameSplit[0];
      $lname = $nameSplit[1];
      $usersUid = array();
      foreach ($allUid as $uid => $value) {
        if(count((array)$value) > 5){
          if($value->fname == $fname && $value->lname == $lname){
            $usersUid[$uid] = $uid;
          }
        }
      }
      return $usersUid;
    }

    public function searchStdcode($stdCode)
    {
      $firebase = $this->connect_firebase();
      $stdyear = substr($stdCode,0,2);
      $data = json_decode($firebase->get('alumnus/'.$stdyear,array('print' => 'pretty','orderBy' => '"studentId"','equalTo' => '"'.$stdCode.'"')));
      foreach ($data as $uid => $value) {
        $usersUid[$uid] = $uid;
      }
      return $usersUid;
    }

    public function searchJobKey($keyword)
    {
      $firebase = $this->connect_firebase();
      $data = json_decode($firebase->get("jobs",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$keyword.'"')));
      foreach ($data as $key => $value) {
        return $key;
      }
    }

    public function searchJobs($keyword,$allUid)
    {   
        $userData = $this->session->get("userData");  //new
		    $stdyear = substr($userData->studentId,0,2); //new

        $firebase = $this->connect_firebase();
        $jobKey = $this->searchJobKey($keyword);
        $usersUid = array();
        foreach ($allUid as $uid => $value) {
          $data = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid."/companyWork"));
          foreach ($data as $key => $subValue) {
            if(in_array($jobKey, $subValue->userWork))
            {
              $usersUid[$uid] = $uid;
            }
          }

        }
        return $usersUid;
    }

    public function searchCourse($keyword,$allUid)
    {
      $userData = $this->session->get("userData");  //new
		  $stdyear = substr($userData->studentId,0,2); //new
      $firebase = $this->connect_firebase();
      $usersUid = array();
        foreach ($allUid as $uid => $value) {
          $data = json_decode($firebase->get('alumnus/'.$stdyear.'/'.$uid."/education",array('print' => 'pretty','orderBy' => '"course"','equalTo' => '"'.$keyword.'"')));
          if(count((array)$data) != 0)
          {
            $usersUid[$uid] = $uid;
          }

        }
        return $usersUid;
    }



    public function searchCity($keyword)
    { 
      $userData = $this->session->get("userData");  //new
		  $stdyear = substr($userData->studentId,0,2); //new
      $firebase = $this->connect_firebase();
      $usersUid = array();
      $data = json_decode($firebase->get('alumnus/',array('print' => 'pretty','orderBy' => '"city"','equalTo' => '"'.$keyword.'"')));  //change
      foreach ($data as $uid => $value) {
        $usersUid[$uid] = $uid;
      }
        return $usersUid;
    }

    public function processToUserDatasView($allUid,$usersData,$advisorAll,$companyWorkAll,$jobAll)
    {
      
      $firebase = $this->connect_firebase();
      $userDatasView = array();
      foreach ($usersData as $uid => $userData)
      {
          $fullname = $userData->fname." ".$userData->lname;
          $advisor = $userData->advisor;
          $advisorName = $advisorAll[$userData->advisor]->name;
          $engGear = (int)substr($userData->studentId,0,2)-12;
          $userJob = array();
          foreach ($userData->companyWork as $key => $value) {
              $userJob[$value->endWork] = array("companyId" => $value->companyId,"userWork" => $value->userWork);
          }
          $companyWorkName = "";
          $companyWorkId = "";
          $listJob = "";
          krsort($userJob);
          foreach ($userJob as $key => $value)
          {
              $companyWorkName = $companyWorkAll[$value['companyId']]->name;
              $companyWorkId = $value['companyId'];  
              foreach ($value['userWork'] as $key => $value) {
                $jobName = $jobAll[$value]->name;
                if($listJob == "")
                  $listJob = $jobName;
                else
                  $listJob = $listJob.", ".$jobName;
              }
              break;
          }

          array_push($userDatasView, array("fullname" => $fullname,"nickname" => $userData->nickname,"studentId" => $userData->studentId,"engGear"=>$engGear,"companyWorkName" => $companyWorkName,"companyWorkId" => $companyWorkId,"advisor" => $advisor,"advisorName" => $advisorName,"disable" => $userData->disable,"facebookUid" => $userData->facebookUid,"jobs" => $listJob));

      }
      return $userDatasView;

    }


    public function getAdvisorList()
    {
      $firebase = $this->connect_firebase();
      $advisors = (array)json_decode($firebase->get("advisor",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
      usort($advisors,function ($a, $b)
      {
        return strcmp($a->name, $b->name);
      });
      $advisorsDisplay = array();
      foreach ($advisors as $key => $value) {
        $advisor = json_decode($firebase->get("advisor",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$value->name.'"')));
        foreach ($advisor as $advisorKey => $advisorValue) {
          $advisorsDisplay[$advisorKey] = $advisorValue;
        }
      }
      return $advisorsDisplay;
    }

    public function getCompanyWorkList()
    {
      $firebase = $this->connect_firebase();
      $listCompanyWork = (array)json_decode($firebase->get("companyWorks",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
        usort($listCompanyWork,function ($a, $b)
        {
          return strcmp($a->name, $b->name);
        });
      $CompanyWorkDisplay = array();
      foreach ($listCompanyWork as $key => $value) {
        $CompanyWork = json_decode($firebase->get("companyWorks",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$value->name.'"')));
        foreach ($CompanyWork as $CompanyWorkKey => $CompanyWorkrValue) {
          $value = (array)$CompanyWorkrValue;
          $value['key'] = $CompanyWorkKey;
          $CompanyWorkDisplay[$CompanyWorkKey] = $value;
        }
      }
      return $CompanyWorkDisplay;
    }

    public function getJobList()
    {
      $firebase = $this->connect_firebase();
      $listJob = (array)json_decode($firebase->get("jobs",array('print' => 'pretty','orderBy' => '"status"','equalTo' => 'true')));
        usort($listJob,function ($a, $b)
        {
          return strcmp($a->name, $b->name);
        });
        $listJobs = array();
        foreach ($listJob as $key => $value) {
            array_push($listJobs, array("key" => $key, "name" => $value->name));
        }
      $listJobkDisplay = array();
      foreach ($listJob as $key => $value) {
        $job = json_decode($firebase->get("jobs",array('print' => 'pretty','orderBy' => '"name"','equalTo' => '"'.$value->name.'"')));
        foreach ($job as $jobKey => $jobValue) {
          $value = (array)$jobValue;
          $value['key'] = $jobKey;
          $listJobkDisplay[$jobKey] = $value;
        }
      }
      return $listJobkDisplay;
    }

    
}
