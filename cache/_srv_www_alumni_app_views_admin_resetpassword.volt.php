<div class="portlet light">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase"><?= $topic_header ?></span>
    </div>
</div>

<div class="portlet-body">
    <!-- BEGIN FORM-->
    <form action="resetPass" class="form-horizontal" id="form_sample_1" method="post">
        <div class="form-body">
            <div class="form-group">
                <label class="control-label col-md-3">Password</label>
                <div class="col-md-4">
                    <input type="password" class="form-control" rows="3" name="password" value="">
                </div>               
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Confirm Password</label>
                <div class="col-md-4">
                    <input type="password" class="form-control" rows="3" name="cpassword" value="">
                </div>               
            </div>

            <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button id="addCompanyButton" type="submit" class="btn green">รีเซ็ต</button>
                </div>
            </div>
            </div>
        </div>
    </form>
    <!-- END FORM-->
</div>                                                
</div>
