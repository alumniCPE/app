<div class="col-md-12">
  <div class="portlet light portlet-fit " style="height:95%">
      <div class="portlet-title">
          <div class="caption">
              <i class="icon-graduation font-green"></i>
              <span class="caption-subject font-green bold uppercase">จำนวนนักศึกษาที่จบการศึกษา</span>
          </div>
      </div>
      <div class="portlet-body">
          <div id="amount" style="height:392px;"></div>
      </div>
  </div>
</div>

<div class="col-md-4">
  <div class="portlet light portlet-fit ">
      <div class="portlet-title">
          <div class="caption">
              <i class=" icon-briefcase font-green"></i>
              <span class="caption-subject font-green bold uppercase">จำนวนนักศึกษาเก่า แบ่งตามสายงาน</span>
          </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-xs-12" id="workfield" style="height:300px;"></div>
        </div>
      </div>
  </div>
</div>

<div class="col-md-4">
  <div class="portlet light portlet-fit ">
      <div class="portlet-title">
          <div class="caption">
              <i class="icon-pointer font-green"></i>
              <span class="caption-subject font-green bold uppercase">จำนวนนักศึกษาเก่า แบ่งตามจังหวัดที่อยู่</span>
          </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-xs-12" id="region" style="height:300px;"></div>
        </div>
      </div>
  </div>
</div>

<div class="col-md-4">
  <div class="portlet light portlet-fit ">
      <div class="portlet-title">
          <div class="caption">
              <i class=" icon-pie-chart font-green"></i>
              <span class="caption-subject font-green bold uppercase"> ประเภทของธุรกิจ </span>
          </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-xs-12" id="company" style="height:300px;"></div>
        </div>
      </div>
  </div>
</div>

<script type="text/javascript">
  var stdamount = <?= $stdamount ?>;
  var workfield = <?= $workfield ?>;
  var region = <?= $region ?>;
  var companywork = <?= $companywork ?>;
</script>
