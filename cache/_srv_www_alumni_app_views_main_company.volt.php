<div class="portlet light ">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-briefcase font-green"></i>
        <span class="caption-subject bold uppercase"><?= $topic_header ?></span>
    </div>
</div>

<div class="portlet-body">
    <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="sample_2">
        <thead>
            <tr>
                
                <?php foreach ($tableattr as $attr) { ?>
                  <th class="col-md-2"> <?= $attr ?> </th>
                <?php } ?>

            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($data as $key => $value) {
                echo "<tr>";
                echo "<th></th>";
                echo "<td><a href='companyprofile?id=".$value['id']."'>".$value['name']."</a></td>";
                echo "<td>".$value['city']."</td>";
                echo "<td>";
                foreach ($value['work'] as $subKey => $subValue) {
                    // echo count($value['companyWorkJob']);
                    if($subKey+1 == count($value['work']))
                    {
                        echo $subValue['name'];
                    }else
                    {
                        echo $subValue['name']." ,";
                    }
                }
                echo "</td>";
                echo "<td><a>".$value['website']."</a></td>";
                echo "<td>".$value['phone']."</td>";
                echo "</tr>";
                # code...
            }

            ?>
        </tbody>
    </table>
</div>
</div>
