<!DOCTYPE html>
<html ng-app="registerApp">
    <head>
    	<meta charset="utf-8" />
        <title><?= $getTitle ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?= $this->assets->outputCss() ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
    </head>

    <body class=" login" ng-controller="registerController">
    <br><br>
        	<div class="col-md-8 col-md-offset-2">
            	<div class="portlet light bordered" id="form_wizard_1">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class=" icon-layers font-red"></i>
                          <span class="caption-subject font-red bold uppercase"> สมัครบัญชีผู้ใช้งาน -
                              <span class="step-title"> Step 1 of 4 </span>
                          </span>
                      </div>

                  </div>
                  <div class="portlet-body form">
  										<form class="form-horizontal" action="register/addUser" id="submit_form" method="POST">
  											<div class="form-wizard">
  												<div class="form-body">
  													<ul class="nav nav-pills nav-justified steps">
  														<li>
  															<a href="#tab1" data-toggle="tab" class="step">
  																<span class="number"> 1 </span>
  																<span class="desc">
  																	<i class="fa fa-check"></i> ยินดีต้อนรับ </span>
  															</a>
  														</li>
  														<li>
  															<a href="#tab2" data-toggle="tab" class="step">
  																<span class="number"> 2 </span>
  																<span class="desc">
  																	<i class="fa fa-check"></i> ข้อมูลส่วนตัว </span>
  															</a>
  														</li>
  														<li>
  															<a href="#tab3" data-toggle="tab" class="step">
  																<span class="number"> 3 </span>
  																<span class="desc">
  																	<i class="fa fa-check"></i> การทำงาน </span>
  															</a>
  														</li>
  														<li>
  															<a href="#tab4" data-toggle="tab" class="step">
  																<span class="number"> 4 </span>
  																<span class="desc">
  																	<i class="fa fa-check"></i> ยืนยันข้อมูล </span>
  															</a>
  														</li>
  													</ul>
  													<div id="bar" class="progress progress-striped" role="progressbar">
  														<div class="progress-bar progress-bar-success"> </div>
  													</div>
  													<div class="tab-content">
  														<div class="alert alert-danger display-none">
  															<button class="close" data-dismiss="alert"></button> กรุณากรอกข้อมูลให้ครบถ้วน </div>
  														<div class="alert alert-success display-none">
  															<button class="close" data-dismiss="alert"></button> กรอกข้อมูลสำเร็จ </div>
  														<div class="tab-pane active" id="tab1">
  															<h4 class="text-center">เว็บไชต์บันทึกข้อมูลศิษย์เก่าของนักศึกษาภาควิชาคอมพิวเตอร์ มหาวิทยาลัยเชียงใหม่</h4>
                              </div>

  														<div class="tab-pane" id="tab2">
  															<h3 class="block col-md-12">กรุณากรอกข้อมูลส่วนตัว</h3>

                                <div class="form-group col-md-6">
                                  <label class="control-label">ชื่อที่ใช้ระหว่างศึกษา (ไม่ต้องมีคำนำหน้า)
  																	<span class="required"> * </span>
  																</label>
  																	<input type="text" class="form-control" name="fname" value="<?= $fname ?>" />
                                    <span class="help-block"> </span>
  															</div>

                                <div class="form-group col-md-6">
                                  <label class="control-label">นามสกุลที่ใช้ระหว่างศึกษา
                                    <span class="required"> * </span>
                                  </label>
                                  <input type="text" class="form-control" name="lname" value="<?= $lname ?>" />
                                  <span class="help-block"> </span>
  															</div>

                                <div class="col-xs-12" style="padding:0">
    															<div class="form-group col-md-6">
    																<label class="control-label">ชื่อที่ใช้ระหว่างศึกษา(ภาษาอังกฤษ)
    																	<span class="required"> * </span>
    																</label>
    																<input type="text" class="form-control" name="efname" ng-model="efname"/>
                                    <span class="help-block"> </span>
                                  </div>

                                  <div class="form-group col-md-6">
                                    <label class="control-label">นามสกุลที่ใช้ระหว่างศึกษา(ภาษาอังกฤษ)
                                      <span class="required"> * </span>
                                    </label>
                                    <input type="text" class="form-control" name="elname" ng-model="elname"/>
                                    <span class="help-block"> </span>
    															</div>
                                </div>

                                <div class="form-group col-md-6">
                                  <label class="control-label">ชื่อเล่น
                                    <span class="required"> * </span>
                                  </label>
                                  <input type="text" class="form-control" name="nickname" />
                                  <span class="help-block"> </span>
                                </div>

                                <div class="form-group col-md-6">
                                  <label class="control-label">อีเมล
                                    <span class="required"> * </span>
                                  </label>
                                    <input type="text" class="form-control" name="email" />
                                    <span class="help-block"> </span>
                                </div>


  															<div class="form-group col-md-12">
  																<label class="control-label">เพศ
  																	<span class="required"> * </span>
  																</label>
  																	<div class="radio-list">
  																		<?php
                                          if($gender == "ชาย")
                                            echo "<label><input type='radio' name='gender' value='ชาย'' data-title='ชาย' checked/> ชาย </label>";
                                          else
                                            echo "<label><input type='radio' name='gender' value='ชาย'' data-title='ชาย' /> ชาย </label>";

                                          if($gender == "หญิง")
                                            echo "<label><input type='radio' name='gender' value='หญิง'' data-title='หญิง' checked/> หญิง </label>";
                                          else
                                            echo "<label><input type='radio' name='gender' value='หญิง'' data-title='หญิง' /> หญิง </label>";
                                      ?>
  																	</div>
  																	<div id="form_gender_error"> </div>
  															</div>

                                <div class="form-group col-md-12">
  																<label class="control-label">ที่อยู่ปัจจุบัน
  																	<span class="required"> * </span>
  																</label>
                                    <textarea class="form-control" rows="3" name="address"></textarea>
  																	<!--<span class="help-block"> กรอกที่อยู่ปัจจุบัน </span>-->
  															</div>

                                <div class="form-group col-md-6">
  																<label class="control-label">จังหวัด</label>
  																	<select name="city" id="city_list" class="form-control">
  																		<option selected> เลือกจังหวัด </option>
                                      <option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
                                      <option value="กระบี่">กระบี่ </option>
                                      <option value="กาญจนบุรี">กาญจนบุรี </option>
                                      <option value="กาฬสินธุ์">กาฬสินธุ์ </option>
                                      <option value="กำแพงเพชร">กำแพงเพชร </option>
                                      <option value="ขอนแก่น">ขอนแก่น</option>
                                      <option value="จันทบุรี">จันทบุรี</option>
                                      <option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
                                      <option value="ชัยนาท">ชัยนาท </option>
                                      <option value="ชัยภูมิ">ชัยภูมิ </option>
                                      <option value="ชุมพร">ชุมพร </option>
                                      <option value="ชลบุรี">ชลบุรี </option>
                                      <option value="เชียงใหม่">เชียงใหม่ </option>
                                      <option value="เชียงราย">เชียงราย </option>
                                      <option value="ตรัง">ตรัง </option>
                                      <option value="ตราด">ตราด </option>
                                      <option value="ตาก">ตาก </option>
                                      <option value="นครนายก">นครนายก </option>
                                      <option value="นครปฐม">นครปฐม </option>
                                      <option value="นครพนม">นครพนม </option>
                                      <option value="นครราชสีมา">นครราชสีมา </option>
                                      <option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
                                      <option value="นครสวรรค์">นครสวรรค์ </option>
                                      <option value="นราธิวาส">นราธิวาส </option>
                                      <option value="น่าน">น่าน </option>
                                      <option value="นนทบุรี">นนทบุรี </option>
                                      <option value="บึงกาฬ">บึงกาฬ</option>
                                      <option value="บุรีรัมย์">บุรีรัมย์</option>
                                      <option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
                                      <option value="ปทุมธานี">ปทุมธานี </option>
                                      <option value="ปราจีนบุรี">ปราจีนบุรี </option>
                                      <option value="ปัตตานี">ปัตตานี </option>
                                      <option value="พะเยา">พะเยา </option>
                                      <option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
                                      <option value="พังงา">พังงา </option>
                                      <option value="พิจิตร">พิจิตร </option>
                                      <option value="พิษณุโลก">พิษณุโลก </option>
                                      <option value="เพชรบุรี">เพชรบุรี </option>
                                      <option value="เพชรบูรณ์">เพชรบูรณ์ </option>
                                      <option value="แพร่">แพร่ </option>
                                      <option value="พัทลุง">พัทลุง </option>
                                      <option value="ภูเก็ต">ภูเก็ต </option>
                                      <option value="มหาสารคาม">มหาสารคาม </option>
                                      <option value="มุกดาหาร">มุกดาหาร </option>
                                      <option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
                                      <option value="ยโสธร">ยโสธร </option>
                                      <option value="ยะลา">ยะลา </option>
                                      <option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
                                      <option value="ระนอง">ระนอง </option>
                                      <option value="ระยอง">ระยอง </option>
                                      <option value="ราชบุรี">ราชบุรี</option>
                                      <option value="ลพบุรี">ลพบุรี </option>
                                      <option value="ลำปาง">ลำปาง </option>
                                      <option value="ลำพูน">ลำพูน </option>
                                      <option value="เลย">เลย </option>
                                      <option value="ศรีสะเกษ">ศรีสะเกษ</option>
                                      <option value="สกลนคร">สกลนคร</option>
                                      <option value="สงขลา">สงขลา </option>
                                      <option value="สมุทรสาคร">สมุทรสาคร </option>
                                      <option value="สมุทรปราการ">สมุทรปราการ </option>
                                      <option value="สมุทรสงคราม">สมุทรสงคราม </option>
                                      <option value="สระแก้ว">สระแก้ว </option>
                                      <option value="สระบุรี">สระบุรี </option>
                                      <option value="สิงห์บุรี">สิงห์บุรี </option>
                                      <option value="สุโขทัย">สุโขทัย </option>
                                      <option value="สุพรรณบุรี">สุพรรณบุรี </option>
                                      <option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
                                      <option value="สุรินทร์">สุรินทร์ </option>
                                      <option value="สตูล">สตูล </option>
                                      <option value="หนองคาย">หนองคาย </option>
                                      <option value="หนองบัวลำภู">หนองบัวลำภู </option>
                                      <option value="อำนาจเจริญ">อำนาจเจริญ </option>
                                      <option value="อุดรธานี">อุดรธานี </option>
                                      <option value="อุตรดิตถ์">อุตรดิตถ์ </option>
                                      <option value="อุทัยธานี">อุทัยธานี </option>
                                      <option value="อุบลราชธานี">อุบลราชธานี</option>
                                      <option value="อ่างทอง">อ่างทอง </option>
  																	</select>
  															</div>

                                <div class="form-group col-md-6">
  																<label class="control-label">เบอร์โทรศัพท์ติดต่อ
  																	<span class="required"> * </span>
  																</label>
  																<input type="text" class="form-control" name="phone" maxlength="10" />
                                  <small>ใส่เฉพาะตัวเลข <br/> (เช่น 053942023)</small>
  															</div>

                                <h3 class="block col-md-12">หลักสูตรที่จบการศึกษา</h3>

                                <div class="form-group col-md-12">
                                  <label class="control-label">
                                    หลักสูตรที่จบการศึกษา (หากจบการศึกษามากกว่า 1 หลักสูตร สามารถเพิ่มเติมภายหลังได้)
                                    <span class="required"> * </span>
                                  </label>
                                    <div class="radio-list">
                                      <label>
                                        <input id="alumniType" type="radio" name="alumniType" value="CPE" data-title="CPE" checked /> CPE (หลักสูตรปริญญาตรี สาขาวิศวกรรมคอมพิวเตอร์) </label>
                                      <label>
                                        <input id="alumniType" type="radio" name="alumniType" value="ISNE" data-title="ISNE" /> ISNE (หลักสูตรปริญญาตรี สาขาวิศวกรรมระบบสารสนเทศและเครือข่าย) </label>
                                        <label>
                                        <input id="alumniType" type="radio" name="alumniType" value="MCPE" data-title="MCPE" /> MCPE (หลักสูตรปริญญาโท สาขาวิศวกรรมคอมพิวเตอร์) </label>
                                        <label>
                                        <input id="alumniType" type="radio" name="alumniType" value="PHDCPE" data-title="PHDCPE" /> PHDCPE (หลักสูตรปริญญาเอก สาขาวิศวกรรมคอมพิวเตอร์) </label>
                                    </div>
                                    <div id="form_gender_error"> </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-md-3">ปีที่เข้าศึกษา(พ.ศ.)
                                    <span class="required"> * </span>
                                  </label>
                                  <div class="col-md-4">
                                    <select name="entryYear" id="work_list" class="form-control"  value="<?= $entryYear ?>">
                                      <?php foreach ($selyear as $year) { ?>
                                        <option value="<?= $year ?>"> <?= $year ?> </option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-md-3">ปีที่จบการศึกษา(พ.ศ.)
                                    <span class="required"> * </span>
                                  </label>
                                  <div class="col-md-4">
                                    <select name="graduateYear" id="work_list" class="form-control" >
                                      <?php foreach ($selyear as $year) { ?>
                                        <option value="<?= $year + 4 ?>"> <?= $year + 4 ?> </option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-md-3">อาจารย์ที่ปรึกษาโครงงาน
                                    <span class="required"> * </span>
                                  </label>
                                  <div class="col-md-4">
                                    <select name="advisor" id="city_list" class="form-control" >
                                      <option value="" selected> เลือกอาจารย์ที่ปรึกษาโครงงาน </option>
                                      <?php
                                      foreach ($advisor as $key => $value) {
                                        echo "<option value='$key'>".$value->name."</option>";
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>

  														</div>

  														<div class="tab-pane" id="tab3">
                                <div class="form-group">
  																<label class="control-label col-md-3">ชื่อองค์กรที่ทำงานในปัจจุบัน</label>
  																<div class="col-md-4">
  																	<input type="text" class="listCompany form-control" rows="3" name="companyName" ng-model="companyName">
  																</div>
                                  <div class="col-md-2">
                                    <input class="btn yellow" type="button" name="checkCompany" value="ตรวจสอบบริษัท" ng-click="checkCompany()">
                                  </div>
  															</div>
                                <div class="form-group">
  																<label class="control-label col-md-3">ที่อยู่บริษัท</label>
                                  <span class="required"> * </span>
  																<div class="col-md-4">
  																	<input type="text" class="form-control" name="companyAddress" ng-model="companyAddress">
  																</div>
  															</div>
                                <div class="form-group">
                                  <label class="control-label col-md-3">จังหวัด</label>
                                  <div class="col-md-4">
                                    <select name="companyCity" id="city_list" class="form-control" ng-model="companyCity">
                                      <option selected> เลือกจังหวัด </option>
                                      <option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
                                      <option value="กระบี่">กระบี่ </option>
                                      <option value="กาญจนบุรี">กาญจนบุรี </option>
                                      <option value="กาฬสินธุ์">กาฬสินธุ์ </option>
                                      <option value="กำแพงเพชร">กำแพงเพชร </option>
                                      <option value="ขอนแก่น">ขอนแก่น</option>
                                      <option value="จันทบุรี">จันทบุรี</option>
                                      <option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
                                      <option value="ชัยนาท">ชัยนาท </option>
                                      <option value="ชัยภูมิ">ชัยภูมิ </option>
                                      <option value="ชุมพร">ชุมพร </option>
                                      <option value="ชลบุรี">ชลบุรี </option>
                                      <option value="เชียงใหม่">เชียงใหม่ </option>
                                      <option value="เชียงราย">เชียงราย </option>
                                      <option value="ตรัง">ตรัง </option>
                                      <option value="ตราด">ตราด </option>
                                      <option value="ตาก">ตาก </option>
                                      <option value="นครนายก">นครนายก </option>
                                      <option value="นครปฐม">นครปฐม </option>
                                      <option value="นครพนม">นครพนม </option>
                                      <option value="นครราชสีมา">นครราชสีมา </option>
                                      <option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
                                      <option value="นครสวรรค์">นครสวรรค์ </option>
                                      <option value="นราธิวาส">นราธิวาส </option>
                                      <option value="น่าน">น่าน </option>
                                      <option value="นนทบุรี">นนทบุรี </option>
                                      <option value="บึงกาฬ">บึงกาฬ</option>
                                      <option value="บุรีรัมย์">บุรีรัมย์</option>
                                      <option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
                                      <option value="ปทุมธานี">ปทุมธานี </option>
                                      <option value="ปราจีนบุรี">ปราจีนบุรี </option>
                                      <option value="ปัตตานี">ปัตตานี </option>
                                      <option value="พะเยา">พะเยา </option>
                                      <option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
                                      <option value="พังงา">พังงา </option>
                                      <option value="พิจิตร">พิจิตร </option>
                                      <option value="พิษณุโลก">พิษณุโลก </option>
                                      <option value="เพชรบุรี">เพชรบุรี </option>
                                      <option value="เพชรบูรณ์">เพชรบูรณ์ </option>
                                      <option value="แพร่">แพร่ </option>
                                      <option value="พัทลุง">พัทลุง </option>
                                      <option value="ภูเก็ต">ภูเก็ต </option>
                                      <option value="มหาสารคาม">มหาสารคาม </option>
                                      <option value="มุกดาหาร">มุกดาหาร </option>
                                      <option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
                                      <option value="ยโสธร">ยโสธร </option>
                                      <option value="ยะลา">ยะลา </option>
                                      <option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
                                      <option value="ระนอง">ระนอง </option>
                                      <option value="ระยอง">ระยอง </option>
                                      <option value="ราชบุรี">ราชบุรี</option>
                                      <option value="ลพบุรี">ลพบุรี </option>
                                      <option value="ลำปาง">ลำปาง </option>
                                      <option value="ลำพูน">ลำพูน </option>
                                      <option value="เลย">เลย </option>
                                      <option value="ศรีสะเกษ">ศรีสะเกษ</option>
                                      <option value="สกลนคร">สกลนคร</option>
                                      <option value="สงขลา">สงขลา </option>
                                      <option value="สมุทรสาคร">สมุทรสาคร </option>
                                      <option value="สมุทรปราการ">สมุทรปราการ </option>
                                      <option value="สมุทรสงคราม">สมุทรสงคราม </option>
                                      <option value="สระแก้ว">สระแก้ว </option>
                                      <option value="สระบุรี">สระบุรี </option>
                                      <option value="สิงห์บุรี">สิงห์บุรี </option>
                                      <option value="สุโขทัย">สุโขทัย </option>
                                      <option value="สุพรรณบุรี">สุพรรณบุรี </option>
                                      <option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
                                      <option value="สุรินทร์">สุรินทร์ </option>
                                      <option value="สตูล">สตูล </option>
                                      <option value="หนองคาย">หนองคาย </option>
                                      <option value="หนองบัวลำภู">หนองบัวลำภู </option>
                                      <option value="อำนาจเจริญ">อำนาจเจริญ </option>
                                      <option value="อุดรธานี">อุดรธานี </option>
                                      <option value="อุตรดิตถ์">อุตรดิตถ์ </option>
                                      <option value="อุทัยธานี">อุทัยธานี </option>
                                      <option value="อุบลราชธานี">อุบลราชธานี</option>
                                      <option value="อ่างทอง">อ่างทอง </option>
                                    </select>
                                  </div>
                                </div>
  								<div class="form-group">
  								  <label class="control-label col-md-3">ลักษณะงาน</label>
                      <div class="col-md-4">
                        <select name="userWork[]" class="form-control selectpicker" ng-model="userWork" multiple>
                        <?php
                          foreach ($listJobs as $key => $value) {
                              echo "<option value=".$value['key'].">".$value['name']."</option>";
                          }
                        ?>
                      </select>
                      </div>
                      <div class="col-md-5"><small>(เลือกได้มากกว่า 1 ข้อ)</small></div>
  								</div>
                  <div class="form-group">
                    <label class="control-label col-md-3">อื่นๆ </label>
                    <div class="col-md-4">
                       <input type="text" class="form-control" name="OtherUserWork" value="">

                    </div>
                  </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3">ลักษณะธุรกิจ</label>
                                  <div class="col-md-4">
                                    <select id="companyWork" name="companyWork[]" class="form-control selectpicker" ng-model="companyWork" multiple>
                                    <?php
                                      foreach ($listCompanyWorks as $key => $value) {
                                          echo "<option value=".$value['key'].">".$value['name']."</option>";
                                      }
                                    ?>

                                  </select>
			                           </div>
                                 <div class="col-md-5"><small>(เลือกได้มากกว่า 1 ข้อ)</small></div>
                                </div>
                              <div class="form-group">
                                <label class="control-label col-md-3">อื่นๆ </label>
                                <div class="col-md-4">
                                   <input type="text" class="form-control" name="OtherCompanyWork" value="">
                                </div>
                                <div class="col-md-5">
                                  <small>หากกรอกในช่อง "อื่นๆ" ระบบจะทำการตรวจสอบแล้ว เพิ่มเข้าไปในประเภทของธุรกิจภายหลัง</small>
                                </div>
                              </div>

                                <div class="form-group">
                                  <label class="control-label col-md-3">ปีที่เริ่มเข้าทำงาน (พ.ศ.)</label>
                                  <div class="col-md-4">
                                  <select name="startWork" id="work_list" class="form-control" ng-model="startWork">
                                      <?php foreach ($selyear as $year) { ?>
                                        <<option value="<?= $year ?>"> <?= $year ?> </option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3">ทำงานจนถึงเมื่อ (พ.ศ).</label>
                                  <div class="col-md-4">
                                  <select name="endWork" id="work_list" class="form-control" ng-model="endWork">
                                    <?php foreach ($selyear as $year) { ?>
                                      <<option value="<?= $year ?>"> <?= $year ?> </option>
                                    <?php } ?>
                                    <option value="ปัจจุบัน"> ปัจจุบัน </option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3">เบอร์โทรศัพท์ติดต่อที่ทำงาน</label>
                                  <div class="col-md-4">
                                    <input type="text" class="form-control" name="companyPhone" ng-model="companyPhone">
                                  </div>
                                  <div class="col-md-5">
                                    <small>ใส่เฉพาะตัวเลข <br/> (เช่น 053942023)</small>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3">เว็บไซต์ของหน่วยงาน</label>
                                  <div class="col-md-4">
                                    <input type="text" class="form-control" name="companyWebsite" ng-model="companyWebsite">
                                  </div>
                                  <div class="col-md-5">
                                    <small>ใส่ url ของเว็บไซต์ <br/> (เช่น http://cpe.eng.cmu.ac.th/ )</small>
                                  </div>
                                </div>
                                <div class="form-group">
                                 <label class="control-label col-md-3">Facebook ของหน่วยงาน</label>
                                 <div class="col-md-4">
                                   <input type="text" class="form-control" name="companyfbook" />
                                   <span class="help-block"></span>
                                 </div>
                                </div>
  														</div>

  														<div class="tab-pane" id="tab4">
  															<h3 class="block">ตรวจสอบข้อมูล</h3>

                                <!-- Display Account -->
                                    <!-- <h4 class="form-section">ข้อมูลบัญชี</h4>
  															   <div class="form-group">
  															   	<label class="control-label col-md-3">รหัสนึกศึกษา : </label>
  															   	<div class="col-md-4">
  															   		<p class="form-control-static" data-display="studentId"> </p>
  															   	</div>
  															   </div>
  															   <div class="form-group">
  															   	<label class="control-label col-md-3">อีเมล : </label>
  															   	<div class="col-md-4">
  															   		<p class="form-control-static" data-display="email"> </p>
  															   	</div>
  															   </div>
                                    <div class="form-group">
                                      <label class="control-label col-md-3">Facebook : </label>
  															   	<div class="col-md-4">
  															   		<p class="form-control-static" data-display="fbook"> </p>
  															   	</div>
                                    </div> -->
                                <!--END Display Account -->
                              <div class="col-md-12">
                                <h4 class="form-section">ข้อมูลส่วนตัว</h4>

                                <div class="form-group">
  																<label class="control-label col-md-4"><b>ชื่อ-นามสกุล </b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="fname"> </p>
                                    								<p class="form-control-static" data-display="lname"> </p>
  																</div>
  															</div>
  															<div class="form-group">
  																<label class="control-label col-md-4"><b>ชื่อ-นามสกุล(ภาษาอังกฤษ) </b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="efname"> </p>
                                    								<p class="form-control-static" data-display="elname"> </p>
  																</div>
  															</div>
                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>ชื่อเล่น </b></label>
                                  <div class="col-md-8">
                                    <p class="form-control-static" data-display="nickname"> </p>
                                  </div>
                                </div>
  															<div class="form-group">
  																<label class="control-label col-md-4"><b>เพศ </b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="gender"> </p>
  																</div>
  															</div>
  															<div class="form-group">
  																<label class="control-label col-md-4"><b>เบอร์โทรศัพท์ติดต่อ </b></label>
  																<div class="col-md-4">
  																	<p class="form-control-static" data-display="phone"> </p>
  																</div>
                                  <div class="col-md-4">
                                    <!-- <small>ใส่เฉพาะตัวเลข <br/> (เช่น 053942023)</small> -->
                                  </div>
  															</div>
  															<div class="form-group">
  																<label class="control-label col-md-4"><b>ที่อยู่ปัจจุบัน </b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="address"> </p>
  																</div>
  															</div>
  															<div class="form-group">
  																<label class="control-label col-md-4"><b>จังหวัด </b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="city"> </p>
  																</div>
  															</div>
                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>Email </b></label>
                                  <div class="col-md-8">
                                    <p class="form-control-static" data-display="email"> </p>
                                  </div>
                                </div>

                                  <h4 class="form-section">ข้อมูลการศึกษา</h4>
                                  <div class="form-group">
                                    <label class="control-label col-md-4"><b>หลักสูตรที่จบการศึกษา</b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="alumniType"> </p>
  																</div>
                                  </div>

                                  <div class="form-group">
                                    <label class="control-label col-md-4"><b>ปีที่เริ่มการศึกษา</b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="entryYear"> </p>
  																</div>
                                	</div>

                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>ปีที่จบการศึกษา</b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="graduateYear"> </p>
  																</div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>อาจารย์ที่ปรึกษาโครงงาน</b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="advisor"> </p>
  																</div>
                                </div>

                                <h4 class="form-section">การทำงาน</h4>
  															<div class="form-group">
  																<label class="control-label col-md-4"><b>ชื่อองค์กรที่ทำงานในปัจจุบัน</b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="companyName"> </p>
  																</div>
  															</div>
                                <div class="form-group">
  																<label class="control-label col-md-4"><b>ที่อยู่ขององค์กร</b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="companyAddress"> </p>
  																</div>
  															</div>
                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>จังหวัด</b></label>
                                  <div class="col-md-8">
                                    <p class="form-control-static" data-display="companyCity"> </p>
                                  </div>
                                </div>
  															<div class="form-group">
  																<label class="control-label col-md-4"><b>ลักษณะงาน</b></label>
  																<div class="col-md-8">
  																	<p class="form-control-static" data-display="userWork[]"> </p>
  																</div>
  															</div>
                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>ลักษณะธุรกิจ</b></label>
                                  <div class="col-md-8">
                                    <p class="form-control-static" data-display="companyWork[]"> </p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>ปีที่เริ่มเข้าทำงาน (พ.ศ.)</b></label>
                                  <div class="col-md-8">
                                    <p class="form-control-static" data-display="startWork"> </p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>ทำงานจนถึงเมื่อ (พ.ศ.)</b></label>
                                  <div class="col-md-8">
                                    <p class="form-control-static" data-display="endWork"> </p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>เบอร์โทรศัพท์ติดต่อ</b></label>
                                  <div class="col-md-4">
                                    <p class="form-control-static" data-display="companyPhone"> </p>
                                  </div>
                                  <div class="col-md-4">
                                    <!-- <small>ใส่เฉพาะตัวเลข <br/> (เช่น 053942023)</small> -->
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>เว็บไชต์ของหน่วยงาน</b></label>
                                  <div class="col-md-8">
                                    <p class="form-control-static" data-display="companyWebsite"> </p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4"><b>Facebook ของหน่วยงาน</b></label>
                                  <div class="col-md-8">
                                    <p class="form-control-static" data-display="companyfbook"> </p>
                                  </div>
                                </div>
                              </div>

  														</div>
  													</div>
  												</div>
  												<div class="form-actions">
  													<div class="row">
  														<div class="text-center">
  															<a href="javascript:;" class="btn default button-previous">
  																<i class="fa fa-angle-left"></i> Back </a>
  															<a href="javascript:;" class="btn btn-outline green button-next" > Continue
  																<i class="fa fa-angle-right"></i>
  															</a>

                                <button class="btn green button-submit"> Submit <i class="fa fa-check"></i></button>


  														</div>
  													</div>
  												</div>
  											</div>
  										</form>

                  </div>
              </div>



        <div class="copyright" style="margin-bottom:0;">
          <h4> ภาควิชาวิศวกรรมคอมพิวเตอร์่ </h4>
          <p>
            คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่ <br>
            239 ถนนห้วยแก้ว ตำบลสุเทพ อำเภอเมือง จังหวัดเชียงใหม่ 50200
          </p>
        </div>

        <?= $this->assets->outputJs() ?>
        <script src="<?= $baseUrl ?>public/js/angular/angular.min.js"></script>
        <script src="<?= $baseUrl ?>public/js/angular/angular-route.min.js"></script>
        <script src="<?= $baseUrl ?>public/js/angular/angular-sanitize.min.js"></script>
        <?php $random = rand(0,99999);
          echo "<script src='".$baseUrl."public/js/register/script.js?".$random."'></script>";
        ?>
      </div>

  </body>
  </html>
