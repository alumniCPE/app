<div class="portlet light ">
  <div class="portlet-title">
      <div class="caption font-green">
          <i class="icon-graduation font-green"></i>
          <span class="caption-subject bold uppercase"><?= $topic_header ?></span>
      </div>
  </div>

<!--   <ul class="nav nav-tabs">
      <li class="active">
          <a href="#alumnireg" data-toggle="tab"> รายชื่อนักศึกษาเก่าที่ลงทะเบียนแล้ว </a>
      </li>
  </ul>
 -->
  <div class="tab-content">
      <div class="tab-pane active" id="alumnireg">
          <div class="portlet-body">
              <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="sample_2">
                  <thead>
                      <tr>
                          
                          <?php foreach ($tableattr as $attr) { ?>
                              <?php if ($attr !== 'ประเภทของงาน') { ?>
                                <th class=" " style='text-align: center;'> <?= $attr ?> </th>
                              <?php } ?>
                              <?php if ($attr === 'ประเภทของงาน') { ?>
                                <th class="sorting_asc" style='text-align: center;'> <?= $attr ?> </th>
                              <?php } ?>
                          <?php } ?>
                      </tr>
                  </thead>
                  <tbody>

                  <?php
                  foreach ($userDatasView as $key => $value) {
                      if($value['advisor'] == null)
                      {
                        echo "<tr>";
                          echo "<td></td>";
                          echo "<td style='text-align: center;'>".$value['studentId']."</td>";
                          echo "<td class='col-md-2'>".$value['fullname']."</td>";
                          echo "<td style='text-align: center;'>".$value['nickname']."</td>";
                          echo "<td style='text-align: center;'> - </td>";
                          echo "<td style='text-align: center;'> - </td>";
                          echo "<td style='text-align: center;'> - </td>";
                          echo "<td style='text-align: center;'> - </td>";
                      echo "</tr>";
                      }else{
                        echo "<tr>";
                          echo "<td></td>";
                          echo "<td style='text-align: center;'>".$value['studentId']."</td>";
                          echo "<td  class='col-md-2'><a href='profile?studentId=".$value['studentId']."'>".$value['fullname']."</a></td>";
                          //echo "<td style='text-align: center;'><a href='search?gear=".$value['engGear']."'>".$value['engGear']."</a></td>";
                          echo "<td style='text-align: center;'>".$value['nickname']."</td>";
                          //echo "<td><a href='search?advisor=".$value['advisor']."'>".$value['advisorName']."</a></td>";
                          echo "<td class='col-md-2'>".$value['advisorName']."</td>";
                          echo "<td class='col-md-3'><a href='companyprofile?id=".$value['companyWorkId']."'>".$value['companyWorkName']."</a></td>";
                          echo "<td>".$value['jobs']."</td>";
                          echo "<td class='col-md-1' style='text-align: center;'>
                           <a href='http://www.facebook.com/".$value['facebookUid']."' target='_blank'><button type='button' class='btn green btn-outline' title='Facebook'><i class='fa fa-facebook'></button></a></td>";
                        echo "</tr>";
                      }

                  }
                  ?>
                  </tbody>
              </table>
          </div>
        </div>
      </div>
</div>
