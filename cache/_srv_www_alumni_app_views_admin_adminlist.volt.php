

<div class="portlet light ">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase"><?= $topic_header ?></span>
    </div>
</div>

<div class="portlet-body">
    <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="sample_2">
        <thead>
            <tr>
                
                <?php foreach ($tableattr as $attr) { ?>
                  <td> <?= $attr ?> </td>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
        <?php

            foreach ($adminData as $key => $value) {
                echo "<tr>";
                    echo "<td></td>";
                    echo "<td>".$value->fname." ".$value->lname."</td>";
                    echo "<td>".$value->username."</td>";
                    echo "<td>";
                        echo "<a href='adminEdit?id=".$key."'><button type='button' class='btn yellow btn-outline' title='แก้ไข'>
                  <i class='fa fa-wrench'></i></button>";
                        echo "<a href='adminDelte?id=".$key."'><button type='button' class='btn red btn-outline' title='ลบ'>
                  <i class='fa fa-trash'></i></button>";
                    echo "</td>";
                echo "</tr>";
            }

        ?>
        </tbody>
    </table>
</div>
</div>

<div class="portlet light ">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase">ประวัติการเข้าใช้งานของผู้ดูแลระบบ</span>
    </div>
</div>

<div class="portlet-body">
    <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="sample_1">
        <thead>
            <tr>
                  <td></td>
                  <th> ชื่อ-นามสกุล </th>
                  <th> กิจกรรม </th>
                  <th> เวลา </th>
            </tr>
        </thead>
        <tbody>
        <?php

            foreach ($adminLogData as $uid => $acticities) {
                foreach ($acticities as $key => $value) {
                    if($key != "fname" && $key != "lname" )
                    {
                        echo "<tr>";
                            echo "<td></td>";
                            echo "<td>".$acticities->fname." ".$acticities->lname."</td>";
                            echo "<td>".$value->message."</td>";
                            echo "<td>".date('d/m/Y H:i', $value->time*-1)." น.</td>";
                        echo "</tr>";
                    }

                }
            }

        ?>
        </tbody>
    </table>
</div>
</div>