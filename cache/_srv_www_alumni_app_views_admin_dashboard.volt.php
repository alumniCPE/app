<div class="col-md-10">
  <div class="portlet light portlet-fit" style="height:95%">
      <div class="portlet-title">
          <div class="caption">
              <i class="icon-graduation font-green"></i>
              <span class="caption-subject font-green bold uppercase">จำนวนนักศึกษาที่จบการศึกษา</span>
          </div>
      </div>
      <div class="portlet-body">
          <div id="amount" style="height:350px;"></div>
      </div>
  </div>
</div>
<div class="col-md-2"  style="text-align:center;">
    <div class="portlet light portlet-fit">
        <div class="portlet-title">

          <div class="row">
              <span class="font-grey-mint font-md bold"> จำนวนศิษย์เก่าทั้งหมด </span>
              <span class="uppercase font-hg font-red-flamingo bold"> <?= $bd_cpe + $bd_cpes + $md_cpe + $phd_cpe + $bd_isne ?>
                  <span class="font-lg font-grey-mint">คน</span>
              </span>
          </div>
          <hr>
          <div class="row">
              <table class="col-xs-12">
                <thead>
                  <tr>
                    <td><span class="font-grey-mint font-lg bold"> CPE </span></td>
                    <td><span class="uppercase font-hg font-red-flamingo"> <?= $bd_cpe + $bd_cpes + $md_cpe + $phd_cpe ?></span></td>
                    <td><span class="font-lg font-grey-mint">คน</span></td>
                  </tr>
                  <tr>
                    <td><span class="font-grey-mint font-lg bold"> ISNE </span></td>
                    <td><span class="uppercase font-hg font-red-flamingo"> <?= $bd_isne ?></span></td>
                    <td><span class="font-lg font-grey-mint">คน</span></td>
                  </tr>

                  <tr>
                    <td><span class="font-grey-mint font-sm"> ปริญญาเอก </span></td>
                    <td><span class="font-red-flamingo"> <?= $phd_cpe ?> </span></td>
                    <td><span class="font-sm font-grey-mint"> คน </span></td>
                  </tr>
                  <tr>
                    <td><span class="font-grey-mint font-sm"> ปริญญาโท </span></td>
                    <td><span class="font-red-flamingo"> <?= $md_cpe ?> </span></td>
                    <td><span class="font-sm font-grey-mint"> คน </span></td>
                  </tr>
                  <tr>
                    <td><span class="font-grey-mint font-sm"> ปริญญาตรี </span></td>
                    <td><span class="font-red-flamingo"> <?= $bd_cpe ?> </span></td>
                    <td><span class="font-sm font-grey-mint"> คน </span></td>
                  </tr>
                  <tr>
                    <td><span class="font-grey-mint font-sm"> ภาคพิเศษ </span></td>
                    <td><span class="font-red-flamingo"> <?= $bd_cpes ?> </span></td>
                    <td><span class="font-sm font-grey-mint"> คน </span></td>
                  </tr>
                </thead>
              </table>
          </div>
        </div>
    </div>

    <div class="portlet light portlet-fit col-sm-6 col-md-12">
        <div class="portlet-title">

          <div class="row">
              <div class="font-grey-mint font-md bold"> ผู้ลงทะเบียนใช้งาน </div>
              <span class="uppercase font-hg font-red-flamingo bold"> <?= $user ?> </span>
              <span class="font-md font-grey-mint"> / <?= $bd_cpe + $bd_cpes + $md_cpe + $phd_cpe + $bd_isne ?> </span>
              <span class="font-lg font-grey-mint"> คน</span>

              <br><span class="font-grey-mint font-sm"> ถูกปิดการใช้งาน </span>
                  <span class="font-red-flamingo"> <?= $disable ?> </span>
                  <span class="font-sm font-grey-mint"> คน </span>

              <br><div class="font-grey-mint font-md"> ผู้ดูแลระบบ
                  <span class="uppercase font-hg font-red-flamingo"> <?= $admin ?> </span>
                  <span class="font-md font-grey-mint"> คน</span>
                  </div>
          </div>

        </div>
    </div>
  </div>

<div class="col-md-4">
  <div class="portlet light portlet-fit ">
      <div class="portlet-title">
          <div class="caption">
              <i class=" icon-briefcase font-green"></i>
              <span class="caption-subject font-green bold uppercase">จำนวนนักศึกษาเก่า แบ่งตามสายงาน</span>
          </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-xs-12" id="workfield" style="height:300px;"></div>
        </div>
      </div>
  </div>
</div>

<div class="col-md-4">
  <div class="portlet light portlet-fit ">
      <div class="portlet-title">
          <div class="caption">
              <i class="icon-pointer font-green"></i>
              <span class="caption-subject font-green bold uppercase">จำนวนนักศึกษาเก่า แบ่งตามจังหวัดที่อยู่</span>
          </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-xs-12" id="region" style="height:300px;"></div>
        </div>
      </div>
  </div>
</div>

<div class="col-md-4">
  <div class="portlet light portlet-fit ">
      <div class="portlet-title">
          <div class="caption">
              <i class=" icon-pie-chart font-green"></i>
              <span class="caption-subject font-green bold uppercase"> ประเภทของธุรกิจ </span>
          </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-xs-12" id="company" style="height:300px;"></div>
        </div>
      </div>
  </div>
</div>


<script type="text/javascript">
  var stdamount = <?= $stdamount ?>;
  var workfield = <?= $workfield ?>;
  var region = <?= $region ?>;
  var companywork = <?= $companywork ?>;
</script>
