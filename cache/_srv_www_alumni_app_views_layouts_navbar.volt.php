  <div class="page-container-bg-solid">
        <!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="home">
                          <div style="margin:0.5em;">
                            <img alt="" class="img" src="../public/img/cpelogo.png" height="50px" width="210px">
                            <!-- <svg width="100%"> -->
                            	<!-- <image xlink:href="../public/img/cpelogo.svg" x="0" y="0" height="50px" width="210px"/> -->
                            <!-- </svg> -->
                          </div>
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="../public/assets/layouts/layout3/img/avatar.png">
                                    <span class="username"><?= $name ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="home">
                                            <i class="icon-user"></i> ข้อมูลส่วนตัว </a>
                                    </li>
                                    <li>
                                        <a href="logout">
                                            <i class="icon-key"></i> ออกจากระบบ </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->

                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
                <div class="container">

                    <div class="hor-menu ">
                      <ul class="nav navbar-nav">

                        

                        <li class="menu-dropdown classic-menu-dropdown ">
                          <a href="home"> ข้อมูลส่วนตัว
                          </a>
                        </li>

                        <li class="menu-dropdown classic-menu-dropdown ">
                          <a href="stat"> สถิตินักศึกษาเก่า
                          </a>
                        </li>

                        <li class="menu-dropdown classic-menu-dropdown ">
                          <a href="alumni"> รายชื่อนักศึกษาเก่าในรุ่น
                          </a>
                        </li>

                        <li class="menu-dropdown classic-menu-dropdown ">
                          <a href="company"> รายชื่อหน่วยงาน
                          </a>
                        </li>
                        <li class="menu-dropdown classic-menu-dropdown ">
                          <a href="search"> ค้นหา
                          </a>
                        </li>


                      </ul>
                    </div>

                </div>
            </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS --
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Dashboard</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">

                          <?= $this->getContent() ?>

                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->

        <!-- BEGIN INNER FOOTER -->
        <div class="page-footer col-xs-12" style="background-color:#141414;">
            
                <div class="col-md-7"></div>

                <div class="col-md-5">

                    <div class="copyright" style="margin-bottom:0;">
                      <h4> ภาควิชาวิศวกรรมคอมพิวเตอร์่ </h4>
                      <p>
                        คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่ <br>
                        239 ถนนห้วยแก้ว ตำบลสุเทพ อำเภอเมือง จังหวัดเชียงใหม่ 50200
                      </p>
                    </div>

                </div>
        </div>

        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
