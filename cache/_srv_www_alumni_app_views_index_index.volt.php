<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8" />
        <title><?= $getTitle ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta content="Alumni,ศิษย์,ศิษย์เก่า,มช,วิดวะคอมมช,วิดวะมช,วิศวะคอมมช,วิศวะคอม,วิศวะ,คอม,วิดวะคอม,CPE,CMU,CPECMU,Computer,ComputerEngineering" name="keywords"/>
        <?= $this->assets->outputCss() ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link rel="icon" type="image/png" sizes="16x16" href="../public/img/favico/favicon.ico">
    </head>
    <body class=" login">
      <div class="logo"></div>
        <!-- BEGIN LOGIN -->
        <div class="content" style="margin-top:20px;text-align: center;">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="authentication" method="post">
              <h3 class="form-title font-green bold" style="font-size: 2.5em;">CPE ALUMNI</h3>
              <h4 class="text-muted">ระบบฐานข้อมูลนักศึกษาเก่า<br>ภาควิชาวิศวกรรมคอมพิวเตอร์</h4>
              <hr>

                <div class="login-options">

                <a href="javascript:;" class="btn blue" onclick="FacebookLogin()"> เข้าสู่ระบบด้วย Facebook &nbsp;<i class="fa fa-facebook-official"></i></a><br>

                <p id="checkedParameterText" style="color:red;"><?= $LoginTextError ?></p>
                </div>
                <div class="form-group">
                <label class="control-label col-md-12"></label>
                  <div class="col-md-12">

                  </div>
                </div>
                <div class="create-account">
                    <!-- <p><a href="authentication/admin" id="register-btn" class="uppercase">สำหรับผู้ดูแลระบบ</a></p> -->
                </div>
            </form>
            <!-- END LOGIN FORM -->


        </div>
        <div class="copyright" style="margin-bottom:0;">
          <h4> ภาควิชาวิศวกรรมคอมพิวเตอร์ </h4>
          <p>
            คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่ <br>
            239 ถนนห้วยแก้ว ตำบลสุเทพ อำเภอเมือง จังหวัดเชียงใหม่ 50200
          </p>
        </div>

        <?= $this->assets->outputJs() ?>
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '1902059640064554',
              xfbml      : true,
              version    : 'v2.9'
            });
            FB.AppEvents.logPageView();
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));

          function FacebookLogin()
          {
            FB.getLoginStatus(function(response) {
              if (response.status === 'connected') {
                // console.log(response);
                window.location = "facebook?uid="+response.authResponse.userID;
              }
              else {
                FB.login(function(response){
                  console.log(response);
                  if (response.status === 'connected') {
                    window.location = "main";
                  }else{
                    window.location = "";
                  }
                });
              }
            });
          }
        </script>
	</body>
</html>
