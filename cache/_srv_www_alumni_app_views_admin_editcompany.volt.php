<div class="portlet light" ng-app="addCompany" ng-controller="addcompanyController">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase"><?= $topic_header ?></span>
    </div>
</div>

<div class="portlet-body">
    <!-- BEGIN FORM-->
    <form action="editedUserCompany" class="form-horizontal" id="form_sample_1" method="post">
        <div class="form-body">
            <div class="form-group">
                <label class="control-label col-md-3">ชื่อบริษัทที่ทำงาน</label>
                <div class="col-md-4">
                    <input type="text" class="listCompany form-control" rows="3" value="<?= $companyName ?>" name="companyName" onchange="checkedParameter()">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">ที่อยู่บริษัท</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" value="<?= $companyAddress ?>" name="companyAddress" onchange="checkedParameter()">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">จังหวัด</label>
              <div class="col-md-4">
                <select name="companyCity" id="city_list" class="form-control" onchange="checkedParameter()">
                  <option value="<?= $companyCity ?>" selected><?= $companyCity ?></option>
                  <option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
                  <option value="กระบี่">กระบี่ </option>
                  <option value="กาญจนบุรี">กาญจนบุรี </option>
                  <option value="กาฬสินธุ์">กาฬสินธุ์ </option>
                  <option value="กำแพงเพชร">กำแพงเพชร </option>
                  <option value="ขอนแก่น">ขอนแก่น</option>
                  <option value="จันทบุรี">จันทบุรี</option>
                  <option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
                  <option value="ชัยนาท">ชัยนาท </option>
                  <option value="ชัยภูมิ">ชัยภูมิ </option>
                  <option value="ชุมพร">ชุมพร </option>
                  <option value="ชลบุรี">ชลบุรี </option>
                  <option value="เชียงใหม่">เชียงใหม่ </option>
                  <option value="เชียงราย">เชียงราย </option>
                  <option value="ตรัง">ตรัง </option>
                  <option value="ตราด">ตราด </option>
                  <option value="ตาก">ตาก </option>
                  <option value="นครนายก">นครนายก </option>
                  <option value="นครปฐม">นครปฐม </option>
                  <option value="นครพนม">นครพนม </option>
                  <option value="นครราชสีมา">นครราชสีมา </option>
                  <option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
                  <option value="นครสวรรค์">นครสวรรค์ </option>
                  <option value="นราธิวาส">นราธิวาส </option>
                  <option value="น่าน">น่าน </option>
                  <option value="นนทบุรี">นนทบุรี </option>
                  <option value="บึงกาฬ">บึงกาฬ</option>
                  <option value="บุรีรัมย์">บุรีรัมย์</option>
                  <option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
                  <option value="ปทุมธานี">ปทุมธานี </option>
                  <option value="ปราจีนบุรี">ปราจีนบุรี </option>
                  <option value="ปัตตานี">ปัตตานี </option>
                  <option value="พะเยา">พะเยา </option>
                  <option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
                  <option value="พังงา">พังงา </option>
                  <option value="พิจิตร">พิจิตร </option>
                  <option value="พิษณุโลก">พิษณุโลก </option>
                  <option value="เพชรบุรี">เพชรบุรี </option>
                  <option value="เพชรบูรณ์">เพชรบูรณ์ </option>
                  <option value="แพร่">แพร่ </option>
                  <option value="พัทลุง">พัทลุง </option>
                  <option value="ภูเก็ต">ภูเก็ต </option>
                  <option value="มหาสารคาม">มหาสารคาม </option>
                  <option value="มุกดาหาร">มุกดาหาร </option>
                  <option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
                  <option value="ยโสธร">ยโสธร </option>
                  <option value="ยะลา">ยะลา </option>
                  <option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
                  <option value="ระนอง">ระนอง </option>
                  <option value="ระยอง">ระยอง </option>
                  <option value="ราชบุรี">ราชบุรี</option>
                  <option value="ลพบุรี">ลพบุรี </option>
                  <option value="ลำปาง">ลำปาง </option>
                  <option value="ลำพูน">ลำพูน </option>
                  <option value="เลย">เลย </option>
                  <option value="ศรีสะเกษ">ศรีสะเกษ</option>
                  <option value="สกลนคร">สกลนคร</option>
                  <option value="สงขลา">สงขลา </option>
                  <option value="สมุทรสาคร">สมุทรสาคร </option>
                  <option value="สมุทรปราการ">สมุทรปราการ </option>
                  <option value="สมุทรสงคราม">สมุทรสงคราม </option>
                  <option value="สระแก้ว">สระแก้ว </option>
                  <option value="สระบุรี">สระบุรี </option>
                  <option value="สิงห์บุรี">สิงห์บุรี </option>
                  <option value="สุโขทัย">สุโขทัย </option>
                  <option value="สุพรรณบุรี">สุพรรณบุรี </option>
                  <option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
                  <option value="สุรินทร์">สุรินทร์ </option>
                  <option value="สตูล">สตูล </option>
                  <option value="หนองคาย">หนองคาย </option>
                  <option value="หนองบัวลำภู">หนองบัวลำภู </option>
                  <option value="อำนาจเจริญ">อำนาจเจริญ </option>
                  <option value="อุดรธานี">อุดรธานี </option>
                  <option value="อุตรดิตถ์">อุตรดิตถ์ </option>
                  <option value="อุทัยธานี">อุทัยธานี </option>
                  <option value="อุบลราชธานี">อุบลราชธานี</option>
                  <option value="อ่างทอง">อ่างทอง </option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">ประเภทของงานที่ทำ</label>
              <div class="col-md-4">
                <select id="userWork" name="userWork[]" class="form-control selectpicker" onchange="checkedParameter()" multiple>
                <?php
                  foreach ($listJobs as $key => $value) {
                      echo "<option value=".$value['key'].">".$value['name']."</option>";
                  }
                ?>
              </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">ประเภทของธุรกิจ</label>
              <div class="col-md-4">
                <select id="companyWork" name="companyWork[]" class="form-control selectpicker" onchange="checkedParameter()" multiple>
                <?php
                  foreach ($listCompanyWorks as $key => $value) {
                    
                    if(in_array($value['key'], $companyWork))
                    {
                      echo "<option value=".$value['key']." selected>".$value['name']."</option>";
                    }else
                    {
                      echo "<option value=".$value['key'].">".$value['name']."</option>";
                    }
                  }
                ?>

              </select>
             </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">เริ่มทำงานปี (พ.ศ.)</label>
                <div class="col-md-4">
                <select name="startWork" id="work_list" class="form-control" onchange="checkedParameter()">
                  <option value="<?= $startWork ?>"><?= $startWork ?></option>
                  <?php foreach ($yearlist as $year) { ?>
                    <option value="<?= $year ?>"><?= $year ?></option>
                  <?php } ?>
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">สิ้นสุดการทำงานปี (พ.ศ.)</label>
              <div class="col-md-4">
              <select name="endWork" id="work_list" class="form-control">
                <option value="<?= $endWork ?>"><?= $endWork ?></option>
                <option value="ปัจจุบัน"> ปัจจุบัน </option>
                <?php foreach ($yearlist as $year) { ?>
                  <option value="<?= $year ?>"><?= $year ?></option>
                <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">เบอร์โทรศัพท์ติดต่อ</label>
              <div class="col-md-4">
                <input type="text" class="form-control" value="<?= $companyPhone ?>" maxlength="15" name="companyPhone" onchange="checkedParameter()">
              </div>
              <div class="col-md-5">
                <small>ใส่เฉพาะตัวเลข <br/> (เช่น 053942023)</small>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">เว็บไชตของหน่วยงาน</label>
              <div class="col-md-4">
                <input type="text" class="form-control" value="<?= $companyWebsite ?>" name="companyWebsite" onchange="checkedParameter()">
              </div>
              <div class="col-md-5">
                <small>ใส่ url ของเว็บไซต์ <br/> (เช่น http://cpe.eng.cmu.ac.th/ )</small>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Facebook ของหน่วยงาน</label>
              <div class="col-md-4">
                <input type="text" class="form-control" value="<?= $companyFacebook ?>" name="companyFacebook" onchange="checkedParameter()">
              </div>
              <div class="col-md-5">
                <small>ใส่ url ของ Facebook <br/> (เช่น https://www.facebook.com/ComputerEngineeringCMU )</small>
              </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3"></label>
              <div class="col-md-4">
                 <p id="checkedParameterText" style="color:red;"></p>
              </div>
            </div>
            <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button id="addCompanyButton" type="submit" class="btn green">แก้ไข</button>
                    <button type="reset" class="btn default">รีเซ็ต</button>
                </div>
            </div>
            </div>
        </div>
    </form>
    <!-- END FORM-->
</div>
</div>
<script src="<?= $baseUrl ?>public/js/angular/angular.min.js"></script>
<script src="<?= $baseUrl ?>public/js/angular/angular-route.min.js"></script>
<script src="<?= $baseUrl ?>public/js/angular/angular-sanitize.min.js"></script>>
<?php $random = rand(0,99999);
  echo "<script src='".$baseUrl."public/js/main/addCompany.js?".$random."'></script>";
?>
