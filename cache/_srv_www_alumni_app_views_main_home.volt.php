<div class="profile" style="background: #fff;padding: 20px;" ng-app="mainApp" ng-controller="mainController">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> <?= $fname ?> <?= $lname ?> </a>
            </li>
            <li>
                <a href="#tab_1_3" data-toggle="tab"> แก้ไขข้อมูลส่วนตัว </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-sm-4 col-md-3">
                        <img src="<?= $imgProfile ?>" class="img-responsive pic-bordered" alt="" />
                        <table style="background: aliceblue;" class="col-xs-12">
                          <thead>
                              <tr>
                                <th class="col-md-2" style="text-align:center;"></th>
                                <th class="col-md-10"></th>
                              </tr>
                          </thead>
                          <tbody>
                            <tr class="profile-list">
                              <td class="text-center"><i class="fa fa-user"></i></td>
                              <td> <?= $gender ?> </td>
                            </tr>
                            <tr class="profile-list">
                              <td class="text-center"><i class="fa fa-map-marker"></i></td>
                              <td> <?= $address ?> <br> จ.<?= $city ?> </td>
                            </tr>
                            <tr class="profile-list">
                              <td class="text-center"><i class="fa fa-phone"></i></td>
                              <td> <?= $phone ?> </td>
                            </tr>
                            <tr class="profile-list">
                              <td class="text-center"><i class="fa fa-facebook"></i></td>
                              <td> <a href="http://www.facebook.com/<?= $facebookUid ?>"><?= $fname ?> <?= $lname ?></a> </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                    <div class="col-sm-8 col-md-9">
                        <div class="row">
                            <div class="col-md-12 profile-info">
                                <h1 class="font-green sbold uppercase"><?= $fname ?> <?= $lname ?> (<?= $efname ?> <?= $elname ?>)</h1>
                                <hr>
                                <div class="col-xs-4 col-md-3">
                                  <span class="font-green sbold uppercase">ชื่อเล่น </span><br/>
                                  <span class="font-green sbold uppercase">เกียร์ </span><br/>
                                  <span class="font-green sbold uppercase">รุ่น </span><br/>
                                  <span class="font-green sbold uppercase">รหัสนักศึกษา </span><br/>
                                  <span class="font-green sbold uppercase">ระยะเวลาที่ศึกษา </span><br/>
                                  <span class="font-green sbold uppercase">อาจารย์ที่ปรึกษา </span><br/>
                                </div>

                                <div class="col-xs-8 col-md-9">
                                  <span> <?= $nickname ?> </span><br/>
                                  <span> <?= $engGear ?> </span><br/>
                                  <span> <?= $alumniType ?>#<?= $gen ?> </span><br/>
                                  <span> <?= $studentId ?> </span><br/>
                                  <span> <?= $entryYear ?> - <?= $graduateYear ?> </span><br/>
                                  <span> <?= $advisorName ?> </span><br/>
                                </div>
                            </div>

                        </div>
                        <!--end row-->
                        <hr/>
                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_11" data-toggle="tab"> ประวัติการทำงาน  </a>
                                </li>
                                <li>
                                    <a href="#tab_1_12" data-toggle="tab"> ประวัติการศึกษา  </a>
                                </li>
                                <li>
                                    <a href="#tab_1_13" data-toggle="tab"> ประกาศนียบัตร  </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_11">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-advance table-hover table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-6">
                                                        <i class="fa fa-briefcase"></i> ชื่อหน่วยงาน </th>
                                                    <th class="col-md-3">
                                                        <i class="fa fa-user"></i> ประเภทของงาน </th>
                                                    <th class="col-md-3">
                                                        <i class="fa fa-calendar"></i> ช่วงเวลา </th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                              <?php
                                              foreach ($userJob as $key => $value) {
                                                echo "<tr>";

                                                  echo "<td><a href='javascript:;'>".$value['companyName']."</a></td>";
                                                  echo "<td>";
                                                  foreach ($value['jobs'] as $subKey => $subValue) {
                                                      if($subKey+1 == count($value['jobs']))
                                                        echo $subValue;
                                                      else
                                                        echo $subValue." ,";
                                                  }
                                                  echo "</td>";
                                                  echo "<td>".$value['startWork']." - ".$value['endWork']."</td>";

                                                echo "</tr>";
                                              }
                                              ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_1_12">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-advance table-hover table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-2">
                                                        <i class="fa fa-briefcase"></i> ระดับการศึกษา </th>
                                                    <th class="col-md-2">
                                                        <i class="fa fa-book"></i> หลักสูตร/สาขา </th>
                                                    <th class="col-md-2">
                                                        <i class="fa fa-calendar-o"></i> ปีที่เริ่มการศึกษา </th>
                                                    <th class="col-md-2">
                                                        <i class="fa fa-calendar-o"></i> ปีที่จบการศึกษา </th>
                                                    <th class="col-md-4">
                                                        <i class="fa fa-university"></i> สถานศึกษา </th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                              <?php
                                              foreach ($education as $key => $value) {
                                                echo "<tr>";

                                                  echo "<td> ป.".$value->course."</td>";
                                                  echo "<td>".$value->major."</td>";
                                                  echo "<td>".$value->startStudy."</td>";
                                                  echo "<td>".$value->endStudy."</td>";
                                                  echo "<td>".$value->university."</td>";

                                                echo "</tr>";
                                              }
                                              ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_1_13">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-advance table-hover table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <i class="fa fa-certificate"></i> ประกาศนียบัตรวิชาชีพ </th>
                                                    <th>
                                                        <i class="fa fa-bookmark"></i> ผู้ออกใบประกาศ </th>

                                                    <th>
                                                        <i class="fa fa-calendar-plus-o"></i> ปีที่ได้รับ </th>

                                                </tr>
                                            </thead>

                                            <tbody>

                                              <?php
                                              foreach ($certificate as $key => $value) {
                                                echo "<tr>";

                                                  echo "<td>".$value->certificate."</td>";
                                                  echo "<td>".$value->certifier."</td>";
                                                  echo "<td>".$value->receipt."</td>";


                                                echo "</tr>";
                                              }
                                              ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--tab_1_2-->
            <div class="tab-pane" id="tab_1_3">
                <div class="row profile-account">
                    <div class="col-md-3">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">
                            <li class="active">
                                <a data-toggle="tab" href="#tab_1-1">
                                    <i class="fa fa-user"></i> ข้อมูลส่วนตัว </a>
                                <span class="after"> </span>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_2-2">
                                    <i class="fa fa-picture-o"></i> รูปประจำตัว </a>
                            </li>
                            <!-- <li>
                                <a data-toggle="tab" href="#tab_3-3">
                                    <i class="fa fa-file-text-o"></i> บัญชีผู้ใช้ </a>
                            </li> -->
                            <li>
                                <a data-toggle="tab" href="#tab_4-4" >
                                    <i class="fa fa-briefcase"></i> การศึกษาและอาชีพ </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                <form role="form" action="editProfile" method="post">
                                <h3 class="block">ข้อมูลส่วนตัว</h3>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">ชื่อ (ภาษาไทย)</label>
                                        <input type="text" class="form-control" name="fname" value="<?= $fname ?>" /> </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">นามสกุล (ภาษาไทย)</label>
                                        <input type="text" class="form-control" name="lname" value="<?= $lname ?>" /> </div>

                                    <div class="form-group col-md-6">
                                        <label class="control-label">ชื่อ (ภาษาอังกฤษ)</label>
                                        <input type="text" class="form-control" name="efname" value="<?= $efname ?>" /> </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">นามสกุล (ภาษาอังกฤษ)</label>
                                        <input type="text" class="form-control" name="elname" value="<?= $elname ?>" /> </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">ชื่อเล่น</label>
                                        <input type="text" class="form-control" name="nickname" value="<?= $nickname ?>" /> </div>

                                    <div class="form-group col-md-6">
                                      <label class="control-label">อีเมล</label>
                                      <input type="text" class="form-control" name="email" value="<?= $email ?>" /> </div>


                                    <div class="form-group col-md-12">
                                    <label class="control-label">เพศ</label>
                                      <div class="radio-list">
                                        <?php
                                            if($gender == "ชาย")
                                              echo "<label><input type='radio' name='gender' value='ชาย' data-title='ชาย' checked/> ชาย </label>";
                                            else
                                              echo "<label><input type='radio' name='gender' value='ชาย' data-title='ชาย' /> ชาย </label>";

                                            if($gender == "หญิง")
                                              echo "<label><input type='radio' name='gender' value='หญิง' data-title='หญิง' checked/> หญิง </label>";
                                            else
                                              echo "<label><input type='radio' name='gender' value='หญิง' data-title='หญิง' /> หญิง </label>";
                                        ?>
                                      </div>
                                      <div id="form_gender_error"> </div>
                                      </div>

                                    <div class="form-group col-xs-12">
                                        <label class="control-label">ที่อยู่ปัจจุบัน</label>
                                        <textarea class="form-control" rows="3" name="address" value="<?= $address ?>"><?= $address ?></textarea>
                                    </div>

                                    <div class="form-group col-xs-12">
      																<label class="control-label">จังหวัด</label>
      																	<select name="city" class="form-control" >
      																		<option value="<?= $city ?>" selected><?= $city ?></option>
                                          <option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
                                          <option value="กระบี่">กระบี่ </option>
                                          <option value="กาญจนบุรี">กาญจนบุรี </option>
                                          <option value="กาฬสินธุ์">กาฬสินธุ์ </option>
                                          <option value="กำแพงเพชร">กำแพงเพชร </option>
                                          <option value="ขอนแก่น">ขอนแก่น</option>
                                          <option value="จันทบุรี">จันทบุรี</option>
                                          <option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
                                          <option value="ชัยนาท">ชัยนาท </option>
                                          <option value="ชัยภูมิ">ชัยภูมิ </option>
                                          <option value="ชุมพร">ชุมพร </option>
                                          <option value="ชลบุรี">ชลบุรี </option>
                                          <option value="เชียงใหม่">เชียงใหม่ </option>
                                          <option value="เชียงราย">เชียงราย </option>
                                          <option value="ตรัง">ตรัง </option>
                                          <option value="ตราด">ตราด </option>
                                          <option value="ตาก">ตาก </option>
                                          <option value="นครนายก">นครนายก </option>
                                          <option value="นครปฐม">นครปฐม </option>
                                          <option value="นครพนม">นครพนม </option>
                                          <option value="นครราชสีมา">นครราชสีมา </option>
                                          <option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
                                          <option value="นครสวรรค์">นครสวรรค์ </option>
                                          <option value="นราธิวาส">นราธิวาส </option>
                                          <option value="น่าน">น่าน </option>
                                          <option value="นนทบุรี">นนทบุรี </option>
                                          <option value="บึงกาฬ">บึงกาฬ</option>
                                          <option value="บุรีรัมย์">บุรีรัมย์</option>
                                          <option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
                                          <option value="ปทุมธานี">ปทุมธานี </option>
                                          <option value="ปราจีนบุรี">ปราจีนบุรี </option>
                                          <option value="ปัตตานี">ปัตตานี </option>
                                          <option value="พะเยา">พะเยา </option>
                                          <option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
                                          <option value="พังงา">พังงา </option>
                                          <option value="พิจิตร">พิจิตร </option>
                                          <option value="พิษณุโลก">พิษณุโลก </option>
                                          <option value="เพชรบุรี">เพชรบุรี </option>
                                          <option value="เพชรบูรณ์">เพชรบูรณ์ </option>
                                          <option value="แพร่">แพร่ </option>
                                          <option value="พัทลุง">พัทลุง </option>
                                          <option value="ภูเก็ต">ภูเก็ต </option>
                                          <option value="มหาสารคาม">มหาสารคาม </option>
                                          <option value="มุกดาหาร">มุกดาหาร </option>
                                          <option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
                                          <option value="ยโสธร">ยโสธร </option>
                                          <option value="ยะลา">ยะลา </option>
                                          <option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
                                          <option value="ระนอง">ระนอง </option>
                                          <option value="ระยอง">ระยอง </option>
                                          <option value="ราชบุรี">ราชบุรี</option>
                                          <option value="ลพบุรี">ลพบุรี </option>
                                          <option value="ลำปาง">ลำปาง </option>
                                          <option value="ลำพูน">ลำพูน </option>
                                          <option value="เลย">เลย </option>
                                          <option value="ศรีสะเกษ">ศรีสะเกษ</option>
                                          <option value="สกลนคร">สกลนคร</option>
                                          <option value="สงขลา">สงขลา </option>
                                          <option value="สมุทรสาคร">สมุทรสาคร </option>
                                          <option value="สมุทรปราการ">สมุทรปราการ </option>
                                          <option value="สมุทรสงคราม">สมุทรสงคราม </option>
                                          <option value="สระแก้ว">สระแก้ว </option>
                                          <option value="สระบุรี">สระบุรี </option>
                                          <option value="สิงห์บุรี">สิงห์บุรี </option>
                                          <option value="สุโขทัย">สุโขทัย </option>
                                          <option value="สุพรรณบุรี">สุพรรณบุรี </option>
                                          <option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
                                          <option value="สุรินทร์">สุรินทร์ </option>
                                          <option value="สตูล">สตูล </option>
                                          <option value="หนองคาย">หนองคาย </option>
                                          <option value="หนองบัวลำภู">หนองบัวลำภู </option>
                                          <option value="อำนาจเจริญ">อำนาจเจริญ </option>
                                          <option value="อุดรธานี">อุดรธานี </option>
                                          <option value="อุตรดิตถ์">อุตรดิตถ์ </option>
                                          <option value="อุทัยธานี">อุทัยธานี </option>
                                          <option value="อุบลราชธานี">อุบลราชธานี</option>
                                          <option value="อ่างทอง">อ่างทอง </option>
      																	</select>
      															</div>

                                    <div class="form-group col-xs-12">
                                        <label class="control-label">เบอร์โทรติดต่อ</label>
                                        <input type="text" class="form-control" name="phone" value="<?= $phone ?>"/>
                                    </div>
                                    <h3 class="block">ข้อมูลการศึกษา</h3>

                                    <div class="form-group col-md-6">
                                      <label class="control-label">หลักสูตรที่จบการศึกษา</label>
                                      <select name="alumniType" class="form-control" >
                                        <option value="<?= $alumniType ?>" selected hidden><?= $alumniType ?></option>
                                        <option value="CPE"> CPE </option>
                                        <option value="ISNE"> ISNE </option>
                                        <option value="MCPE"> MCPE </option>
                                        <option value="PHDCPE"> PHDCPE </option>
                                      </select>
                                      
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">รุ่น</label>
                                        <input type="text" class="form-control" name="gen" value="<?= $gen ?>" /> </div>
                                    <div id="form_gender_error"> </div>


                                    <div class="form-group col-md-6">
                                      <label class="control-label">ปีที่เริ่มศึกษา</label>
                                        <select name="entryYear" class="form-control" >
                                          <option hidden value="<?= $entryYear ?>" selected><?= $entryYear ?></option>
                                          <option hidden> เลือกปี (พ.ศ.) </option>
                                          <option value="current"> ปัจจุบัน </option>
                                          <?php foreach ($yearlist as $year) { ?>
                                            <option value="<?= $year ?>"><?= $year ?></option>
                                          <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                      <label class="control-label">ปีที่จบการศึกษา</label>
                                        <select name="graduateYear" class="form-control" >
                                          <option hidden value="<?= $graduateYear ?>" selected><?= $graduateYear ?></option>
                                          <option value="current"> ปัจจุบัน </option>
                                          <?php foreach ($yearlist as $year) { ?>
                                            <option value="<?= $year ?>"><?= $year ?></option>
                                          <?php } ?>
                                        </select>
                                    </div>


                                    <div class="form-group col-md-6">
                                      <label class="control-label">อาจารย์ที่ปรึกษา</label>
                                        <select name="advisor" class="form-control" >
                                          <option value="<?= $advisor ?>" selected><?= $advisorName ?></option>
                                          <?php
                                          foreach ($advisorList as $key => $value) {
                                            echo "<option value='$key'>".$value->name."</option>";
                                          }
                                          ?>
                                        </select>
                                    </div>


                                    <div class="col-xs-12 margiv-top-10">
                                        <button type="submit" class="btn green uppercase">บันทึก</button>
                                        <a href="javascript:;" class="btn default"> ยกเลิก </a>
                                    </div>
                                </form>
                            </div>
                            <div id="tab_2-2" class="tab-pane">
                                <form action="editImgProfile" role="form" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> เลือกรูปภาพ </span>
                                                    <span class="fileinput-exists"> เปลี่ยนรูปภาพ </span>
                                                    <input type="file" name="imgProfile[]" ng-file-select="onFileSelect($files)"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> นำรูปภาพออก </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="margin-top-10">
                                        <a href="javascript:;" class="btn green" ng-click="uploadFile()"> ตกลง </a>
                                        <a href="javascript:;" class="btn default"> ยกเลิก </a>
                                    </div>
                                </form>
                            </div>
                            <div id="tab_3-3" class="tab-pane">
                                <form action="resetPasswoed" method="post">
                                    <div class="form-group">
                                      <label class="control-label">รหัสนักศึกษา</label>
                                      <input type="number" class="form-control" value="<?= $studentId ?>" readonly/> </div>
                                    <div class="form-group">
                                        <label class="control-label">แก้ไขรหัสผ่าน</label>
                                        <input id="password" type="password" class="form-control" name="password" mg-model="password" onkeyup="checkedPass()"/> </div>
                                    <div class="form-group">
                                        <label class="control-label">ยืนยันรหัสผ่าน</label>
                                        <input id="repassword" type="password" class="form-control" name="repassword" mg-model="repassword" onkeyup="checkedPass()"/> </div>
                                    <div class="form-group">
                                        <p id="checkedPassText" style="color:red;"></p>
                                    </div>

                                    <div class="margin-top-10">
                                        <button id="submitReset" type="submit" class="btn green uppercase" >บันทึก</button>
                                        <a href="javascript:;" class="btn default"> ยกเลิก </a>
                                    </div>
                                </form>
                            </div>
                            <div id="tab_4-4" class="tab-pane">
                              <h3>ประวัติการทำงาน</h3>
                                <form action="#">
                                  <table class="table table-striped table-bordered table-advance table-hover table-responsive">
                                      <thead>
                                          <tr>
                                              <th>
                                                  <i class="fa fa-briefcase"></i> บริษัท </th>
                                              <th>
                                                  <i class="fa fa-user"></i> ประเภทของงาน </th>
                                              <th>
                                                  <i class="fa fa-calendar-plus-o"></i> ปีที่เข้าทำงาน </th>
                                              <th></th>
                                          </tr>
                                      </thead>

                                      <tbody>
                                          <?php
                                            foreach ($userJob as $key => $value) {
                                              echo "<tr>";

                                                echo "<td><a href='javascript:;'>".$value['companyName']."</a></td>";
                                                echo "<td>";
                                                foreach ($value['jobs'] as $subKey => $subValue) {
                                                    if($subKey+1 == count($value['jobs']))
                                                      echo $subValue;
                                                    else
                                                      echo $subValue." ,";
                                                }
                                                echo "</td>";
                                                echo "<td>".$value['startWork']." - ".$value['endWork']."</td>";
                                                echo "<td>";
                                                echo "<a href='editCompany?key=".$value['companyProfileKey']."' class='btn yellow'>"."<span class='glyphicon glyphicon-wrench'></span>"."</a>";
                                                echo "<a href='deleteCompany?key=".$value['companyProfileKey']."' class='btn red'>"."<span class='glyphicon glyphicon-trash'></span>"."</a>";
                                                echo "</td>";

                                              echo "</tr>";
                                            }
                                          ?>
                                      </tbody>
                                  </table>
                                    <!--end profile-settings-->
                                    <div class="margin-top-10">
                                        <a href="addCompany" class="btn green"> เพิ่ม </a>
                                    </div>
                                </form>

                              <h3>ประวัติการศึกษา</h3>
                                <form action="#">
                                  <table class="table table-striped table-bordered table-advance table-hover table-responsive">
                                      <thead>
                                          <tr>
                                              <th>
                                                  <i class="fa fa-graduation-cap"></i> ระดับการศึกษา </th>
                                              <th>
                                                  <i class="fa fa-book"></i> หลักสูตร/สาขา </th>
                                              <th>
                                                  <i class="fa fa-calendar"></i> ปีที่ศึกษา </th>
                                              <th>
                                                  <i class="fa fa-university"></i> สถานศึกษา </th>
                                              <th></th>
                                          </tr>
                                      </thead>

                                      <tbody>
                                        <?php
                                        foreach ($education as $key => $value) {
                                          echo "<tr>";
                                            echo "<td> ป.".$value->course."</td>";
                                            echo "<td>".$value->major."</td>";
                                            echo "<td>".$value->startStudy." - ".$value->endStudy."</td>";
                                            echo "<td>".$value->university."</td>";
                                            echo "<td>";
                                            echo "<a href='editEducation?key=".$key."' class='btn yellow'>"."<span class='glyphicon glyphicon-wrench'></span>"."</a>";
                                            echo "<a href='deleteEducation?key=".$key."' class='btn red'>"."<span class='glyphicon glyphicon-trash'></span>"."</a>";
                                            echo "</td>";
                                          echo "</tr>";
                                        }
                                        ?>
                                      </tbody>
                                  </table>
                                    <!--end profile-settings-->
                                    <div class="margin-top-10">
                                        <a href="addEducation" class="btn green"> เพิ่ม </a>
                                    </div>
                                </form>

                              <h3>ประกาศนียบัตร</h3>
                                <form action="#">
                                  <table class="table table-striped table-bordered table-advance table-hover table-responsive" style='text-align: center;'>
                                      <thead>
                                          <tr>
                                              <th>
                                                  <i class="fa fa-certificate"></i> ประกาศนียบัตรวิชาชีพ </th>
                                                <th>
                                                  <i class="fa fa-university"></i> หน่วยงานที่ออกใบประกาศ </th>
                                              <th >
                                                  <i class="fa fa-calendar"></i> ปีที่ได้รับ </th>

                                              <th> </th>
                                          </tr>
                                      </thead>

                                      <tbody>
                                        <?php
                                        foreach ($certificate as $key => $value) {
                                          echo "<tr>";
                                            echo "<td>".$value->certificate."</td>";
                                            echo "<td>".$value->certifier."</td>";
                                            echo "<td>".$value->receipt."</td>";
                                            echo "<td>";
                                            echo "<a href='editCer?key=".$key."' class='btn yellow'>"."<span class='glyphicon glyphicon-wrench'></span>"."</a>";
                                            echo "<a href='deleteCer?key=".$key."' class='btn red'>"."<span class='glyphicon glyphicon-trash'></span>"."</a>";
                                            echo "</td>";
                                          echo "</tr>";
                                        }
                                        ?>
                                      </tbody>
                                  </table>
                                    <!--end profile-settings-->
                                    <div class="margin-top-10">
                                        <a href="addCer" class="btn green"> เพิ่ม </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= $baseUrl ?>public/js/angular/angular.min.js"></script>
<script src="<?= $baseUrl ?>public/js/angular/angular-route.min.js"></script>
<script src="<?= $baseUrl ?>public/js/angular/angular-sanitize.min.js"></script>
<?php $random = rand(0,99999);
  echo "<script src='".$baseUrl."public/js/main/script.js?".$random."'></script>";
?>
<script src="https://www.gstatic.com/firebasejs/4.0.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAVeY2EVXwt8TnklcacE7ORKnJBqwOtsgU",
    authDomain: "cpe-alumni.firebaseapp.com",
    databaseURL: "https://cpe-alumni.firebaseio.com",
    projectId: "cpe-alumni",
    storageBucket: "cpe-alumni.appspot.com",
    messagingSenderId: "227546499185"
    // apiKey: "AIzaSyDMvZE9u40TwZlQ9AGwYe0H6rlHBeI6HeQ",
    // authDomain: "re-alumni.firebaseapp.com",
    // databaseURL: "https://re-alumni.firebaseio.com",
    // projectId: "re-alumni",
    // storageBucket: "re-alumni.appspot.com",
    // messagingSenderId: "793025382179"
  };
  firebase.initializeApp(config);
</script>
<style>
  .form-group {
    margin-left: 0px;
    margin-right: 0px;
  }
  .profile-list {
    height: 30px;
    border-bottom: 1px solid white;
  }
</style>
