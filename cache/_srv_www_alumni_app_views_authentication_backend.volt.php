<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8" />
        <title><?= $getTitle ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta content="Alumni,ศิษย์,ศิษย์เก่า,มช,วิดวะคอมมช,วิดวะมช,วิศวะคอมมช,วิศวะคอม,วิศวะ,คอม,วิดวะคอม,CPE,CMU,CPECMU,Computer,ComputerEngineering" name="keywords"/>
        <?= $this->assets->outputCss() ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <body class=" login">
      <div class="logo"></div>
        <!-- BEGIN LOGIN -->
        <div class="content" style="margin-top:20px;">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="adminAuth" method="post">
              <span class="form-title font-green" style="font-size: 2.5em;">CPE ALUMNI</span>
              <p>ระบบฐานข้อมูลนักศึกษาเก่า ภาควิชาวิศวกรรมคอมพิวเตอร์่</p>

              <!-- Log in by Username And Password -->
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง </span>
                </div>

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" />
                </div>

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn green uppercase">Login</button>
                    <label class="rememberme check">
                    <input type="checkbox" name="remember" value="1" />Remember </label>
                </div>
              <!-- End Log in by Username And Password -->

            </form>
            <!-- END LOGIN FORM -->


        </div>
        <div class="copyright" style="margin-bottom:0;">
          <h4> ภาควิชาวิศวกรรมคอมพิวเตอร์่ </h4>
          <p>
            คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่ <br>
            239 ถนนห้วยแก้ว ตำบลสุเทพ อำเภอเมือง จังหวัดเชียงใหม่ 50200
          </p>
        </div>

        <?= $this->assets->outputJs() ?>

	</body>
</html>
