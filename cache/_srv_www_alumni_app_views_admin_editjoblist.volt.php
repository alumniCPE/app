<div class="profile" style="background: #fff;padding: 20px;">
  <div class="tabbable-line tabbable-full-width">
      <ul class="nav nav-tabs">
          <li class="active">
              <a href="#tab_1_1" data-toggle="tab"> แก้ไขลักษณะสายงาน </a>
          </li>
      </ul>

      <div class="tab-content row">
          <!--tab_1_2-->
          <div class="tab-pane active" id="tab_1_1">
            <div class="col-xs-12">
      <form action="editedJobList" class="form-horizontal" id="form_sample_1" method="post">
          <div class="form-body">
              <div class="form-group">
                  <label class="control-label col-md-3">ชื่อ</label>
                  <div class="col-md-4">
                      <input type="text" class="form-control" rows="3" value="<?= $name ?>" name="name">
                  </div>               
              </div>
              <div class="form-actions">
              <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                      <button type="submit" class="btn green">แก้ไข</button>
                  </div>
              </div>
              </div>
          </div>
      </form>
            </div>
        </div>
      </div>
    </div>
</div>
