<div class="portlet light ">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase"><?= $topic_header ?></span>
    </div>
</div>

<div class="portlet light portlet-fit ">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-puzzle font-green"></i>
            <span class="caption-subject font-green bold uppercase">รายชื่ออาจารย์ที่ปรึกษา</span>
        </div>
    </div>
    <div class="portlet-body row">
      <!-- <form action="" method="post"> -->
        <div class="form-group">
            <a href="addAdvisor"><button type="button" class="btn green"><i class="fa fa-plus"></i> เพิ่มอาจารย์ที่ปรึกษา </button></a>
            <br><br><br>
            <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="sample_2">
            <thead>
                <tr>
                      <td>  </td>
                      <td> ชื่อและนามสกุล </td>
                      <td>  </td>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach ($advisor as $key => $value) {
                    echo "<tr>";
                    echo "<th></th>";
                    echo "<td>".$value->name."</a></td>";
                    echo "<td>";
                      echo "<a href='editAdvisor?id=".$key."'><button type='button' class='btn yellow btn-outline' alt='แก้ไข'>
                      <i class='fa fa-wrench'></i></button></a>
                      <a href='deleteAdvisor?id=".$key."'><button type='button' class='btn red btn-outline' alt='ลบ'>
                      <i class='fa fa-trash'></i></button></a>";
                    echo "</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
           </table>

        </div>
    </div>
</div>
</div>
