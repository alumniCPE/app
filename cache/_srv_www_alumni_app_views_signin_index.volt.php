<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>CPE ALUMNI</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <!-- <meta content="" name="description"/> -->
        <meta content="วิศวะคอม,วิศวะ,คอม,วิดวะคอม,CPE,CMU,CPECMU,Computer,ComputerEngineering" name="keywords"/>
        <?= $this->assets->outputCss() ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
    </head>

    <body class=" login">
      <header class="header navbar-fixed-top">
          <!-- Navbar -->
          <nav class="navbar" role="navigation">
              <div class="container">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="menu-container">

                      <!-- Logo -->
                      <div class="logo">
                          <a class="logo-wrap" href="index.html">
                              CPECMU-LOGO
                              <!-- <img class="logo-img logo-img-main" src="img/logo.png" alt="Asentus Logo"> -->
                              <!-- <img class="logo-img logo-img-active" src="img/logo-dark.png" alt="Asentus Logo"> -->
                          </a>
                      </div>
                      <!-- End Logo -->
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->

                  <!-- End Navbar Collapse -->
              </div>
          </nav>
          <!-- Navbar -->
      </header>
      <!--========== END HEADER ==========-->

      <!--========== SLIDER ==========-->
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

          <div class="carousel-inner" role="listbox">
              <div class="item active">
                  <img class="img-responsive" src="img/1920x1080/01.jpg" alt="Slider Image">
                  <div class="container">
                      <div class="carousel-centered">
                          <div class="margin-b-40" style="margin-top:40px;">
                              <br>
                              <h2 class="carousel-title">CPE Alumni</h2>
                              <p>ระบบฐานข้อมูลนักศึกษาเก่า ภาควิชาวิศวกรรมคอมพิวเตอร์ <br>
                              คณะวิศวกรรมสาสตร์มหาวิทยาลัยเชียงใหม่</p>
                          </div>
                          <br>
                          <a href="#" class="btn-theme btn-theme-sm btn-white-brd text-uppercase">Login with Facebook</a>
                      </div>
                  </div>
              </div>
          </div>

      </div>

      <!--========== FOOTER ==========-->
      <footer class="footer">
          <div class="content container">
              <div class="row">
                  <div class="col-md-6">
                    CPECMU LOGO
                      <!-- <img class="footer-logo" src="img/logo.png" alt="Asentus Logo"> -->
                  </div>
                  <div class="col-md-6">
                      <p class="color-base fweight-700">
                        ภาควิชาวิศวกรรมคอมพิวเตอร์
                      </p>
                      <p>
                        คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่ <br>
                        239 ถนนห้วยแก้ว ตำบลสุเทพ อำเภอเมือง จังหวัดเชียงใหม่ 50200
                      </p>
                  </div>
              </div>
          </div>
      </footer>
      <!--========== END FOOTER ==========-->

      <!-- Back To Top -->
      <a href="javascript:void(0);" class="js-back-to-top back-to-top">Top</a>

        <?= $this->assets->outputJs() ?>
	</body>
</html>
