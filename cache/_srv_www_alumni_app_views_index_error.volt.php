<div class="error_404">
  <div class="col-md-6 col-md-offset-3 vcenter text-center">
    <h1 class="font-blue-sharp bold uppercase">404</h1>
    <h3 class="text-muted">ไม่พบหน้าที่ท่านต้องการ</h3>
    <p class="text-muted"> หน้าที่ท่านค้นหาอาจไม่มีอยู่ หรือมีการเปลี่ยนแปลงแล้ว</p>

    <a href="javascript:history.back()" class="btn green"> กลับไปยังหน้าที่แล้ว </a>
  </div>
</div>
