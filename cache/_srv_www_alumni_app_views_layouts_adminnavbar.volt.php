<div class="page-container-bg-solid">

      <div class="page-header">

          <div class="page-header-top">
              <div class="container">

                  <div class="page-logo">
                      <a href="dashboard">
                        <div style="margin:0.5em;">
                        <img alt="" class="img" src="../public/img/cpelogo.png" height="50px" width="210px">
                          <!-- <svg width="100%">
                            <image xlink:href="../public/img/cpelogo.svg" x="0" y="0" height="50px" width="210px"/>
                          </svg> -->
                        </div>
                      </a>
                  </div>


                  <a href="javascript:;" class="menu-toggler"></a>


                  <div class="top-menu">
                      <ul class="nav navbar-nav pull-right">

                          <li class="dropdown dropdown-user dropdown-dark">
                              <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                  <img alt="" class="img-circle" src="../public/assets/layouts/layout3/img/avatar.png">
                                  <span class="username"><?= $username ?></span>
                              </a>
                              <ul class="dropdown-menu dropdown-menu-default">
                                  <li>
                                      <a href="dashboard">
                                          <i class="icon-user"></i> หน้าหลัก </a>
                                  </li>
                                  <li>
                                      <a href="logout">
                                          <i class="icon-key"></i> ออกจากระบบ </a>
                                  </li>
                              </ul>
                          </li>

                      </ul>
                  </div>

              </div>
          </div>

          <div class="page-header-menu">
              <div class="container">

                  <div class="hor-menu ">
                    <ul class="nav navbar-nav">

                      

                      <li class="menu-dropdown classic-menu-dropdown ">
                        <a href="dashboard"> หน้าหลัก
                        </a>
                      </li>

                      <li class="menu-dropdown classic-menu-dropdown ">
                        <a> ข้อมูลศิษย์เก่า
                        </a>
                          <ul class="dropdown-menu pull-left">
                              <li>
                                  <a href="user" class="nav-link  "> ข้อมูลศิษย์เก่าที่ลงทะเบียน </a>
                              </li>
                              <li>
                                  <a href="useradd" class="nav-link  "> เพิ่มข้อมูลศิษย์เก่า </a>
                              </li>
                              <li>
                                  <a href="addAlumnus" class="nav-link  "> นำเข้าข้อมูลศิษย์เก่า </a>
                              </li>
                          </ul>
                      </li>

                      <li class="menu-dropdown classic-menu-dropdown ">
                        <a> ข้อมูลบริษัท
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li>
                                <a href="company" class="nav-link  "> รายชื่อบริษัท </a>
                            </li>
                            <li>
                                <a href="companyadd" class="nav-link  "> เพิ่มข้อมูลบริษัท </a>
                            </li>
                        </ul>
                      </li>

                      <li class="menu-dropdown classic-menu-dropdown ">
                        <a> ข้อมูลผู้ดูแลระบบ
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li>
                                <a href="adminList" class="nav-link  "> รายชื่อผู้ดูแลระบบ </a>
                            </li>
                            <li>
                                <a href="addAdmin" class="nav-link  "> เพิ่มผู้ดูแลระบบ </a>
                            </li>
                        </ul>
                      </li>
                      <li class="menu-dropdown classic-menu-dropdown ">
                        <a href="advisor"> ข้อมูลอาจารย์
                        </a>
                      </li>
                      <li class="menu-dropdown classic-menu-dropdown ">
                        	<a href="tags" > ข้อมูลคำสำคัญ </a>
                      </li>
                      <li class="menu-dropdown classic-menu-dropdown ">
                        <a href="search"> ค้นหา
                        </a>
                      </li>

                    </ul>
                  </div>

              </div>
          </div>
      </div>

      <div class="page-container">

          <div class="page-content-wrapper">

              <div class="page-content">
                  <div class="container">

                      <div class="page-content-inner">

                        <?= $this->getContent() ?>

                      </div>
                  </div>
              </div>
          </div>
      </div>

      <div class="page-footer col-xs-12" style="background-color:#141414;">

        <div class="col-md-7"></div>

        <div class="col-md-5">

            <div class="copyright" style="margin-bottom:0;">
              <h4> ภาควิชาวิศวกรรมคอมพิวเตอร์่ </h4>
              <p>
                คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่ <br>
                239 ถนนห้วยแก้ว ตำบลสุเทพ อำเภอเมือง จังหวัดเชียงใหม่ 50200
              </p>
            </div>

        </div>
      </div>

      <div class="scroll-to-top">
          <i class="icon-arrow-up"></i>
      </div>
</div>
