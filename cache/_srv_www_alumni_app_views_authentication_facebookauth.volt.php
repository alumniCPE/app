<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8" />
        <title><?= $getTitle ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta content="Alumni,ศิษย์,ศิษย์เก่า,มช,วิดวะคอมมช,วิดวะมช,วิศวะคอมมช,วิศวะคอม,วิศวะ,คอม,วิดวะคอม,CPE,CMU,CPECMU,Computer,ComputerEngineering" name="keywords"/>
        <?= $this->assets->outputCss() ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <body class=" login">
      <div class="logo"></div>
        <!-- BEGIN LOGIN -->
        <div class="content" style="margin-top:20px;">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="facebookPush" method="post">
              <span class="form-title font-green" style="font-size: 2.5em;">CPE ALUMNI</span>
              <p>ตรวจสอบรหัสนักศึกษาและบัญชีเฟสบุ๊คของคุณ</p>

                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Incorrect username or password. Please try again </span>
                </div>

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">รหัสนักศึกษา</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="รหัสนักศึกษา" name="studentId" /><br>
                    <label class="control-label visible-ie8 visible-ie9">ชื่อ-นามสกุล ภาษาไทย</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="ชื่อ-นามสกุล ภาษาไทยที่ใช้ในการศึกษา" name="fullname" />
                    <small class="help-block">ไม่ต้องมีคำนำหน้า (เช่น ยุทธพงษ์ สมจิต)</small>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn green uppercase">ตรวจสอบ</button>
                </div>
            </form>
            <!-- END LOGIN FORM -->


        </div>
        <div class="copyright" style="margin-bottom:0;">
          <h4> ภาควิชาวิศวกรรมคอมพิวเตอร์ </h4>
          <p>
            คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่ <br>
            239 ถนนห้วยแก้ว ตำบลสุเทพ อำเภอเมือง จังหวัดเชียงใหม่ 50200
          </p>
        </div>

        <?= $this->assets->outputJs() ?>

	</body>
</html>
