<div class="portlet light ">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase"><?= $topic_header ?></span>
    </div>
</div>

<div class="col-md-6 col-sm-12">
  <!-- BEGIN EXAMPLE TABLE PORTLET-->
  <div class="portlet light portlet-fit portlet-datatable ">
      <div class="portlet-title">
          <div class="caption">
              <i class=" icon-layers font-green"></i>
              <span class="caption-subject font-green sbold uppercase">สายงานใหม่</span>
          </div>
      </div>
      <div class="portlet-body">
        <table class="table" id="sample_1">
            <thead>
                <tr>
                    <th> ชื่อ </th>
                    <th>  </th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($newTagJob as $key => $value) {
                    echo "<tr class='odd gradeX'>";
                    echo "<td>".$value->name."</a></td>";
                    echo "<td>";
                      echo "
                      <a href='deleteNewTagJob?id=".$key."'><button type='button' class='btn red btn-outline' alt='ลบ'>
                      <i class='fa fa-trash'></i></button></a>";
                    echo "</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- END EXAMPLE TABLE PORTLET-->
</div>
<div class="col-md-6 col-sm-12">
  <!-- BEGIN EXAMPLE TABLE PORTLET-->
  <div class="portlet light portlet-fit portlet-datatable ">
      <div class="portlet-title">
          <div class="caption">
              <i class=" icon-layers font-green"></i>
              <span class="caption-subject font-green sbold uppercase">ธุรกิจใหม่</span>
          </div>
      </div>
      <div class="portlet-body">
        <table class="table" id="sample_3">
            <thead>
                <tr>
                    <th> ชื่อ </th>
                    <th> </th>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach ($newTagCompanyWork as $key => $value) {
                    echo "<tr>";
                    echo "<td>".$value->name."</a></td>";
                    echo "<td>";
                      echo "
                      <a href='deleteNewTagCompanyWork?id=".$key."'><button type='button' class='btn red btn-outline' alt='ลบ'>
                      <i class='fa fa-trash'></i></button></a>";
                    echo "</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- END EXAMPLE TABLE PORTLET-->
</div>

<div class="col-md-12 col-sm-12">
<div class="portlet light portlet-fit ">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-puzzle font-green"></i>
            <span class="caption-subject font-green bold uppercase">ประเภทของงาน</span>
        </div>
    </div>
    <div class="portlet-body row">
      <!-- <form action="" method="post"> -->
        <div class="form-group">
            <label class="control-label col-md-2">ประเภทของงาน</label>
            <div class="col-md-10">
                <!-- <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput"> -->

                <?php
                foreach ($jobs as $key => $value) {
                  echo "<button style='margin:2px' class='btn btn-info btn-ls tag'>".$value->name." <a href='editJobList?id=$key' <i class='fa fa-wrench'></i></a>  <a href='deleteJobList?id=$key' <i class='fa fa-times'></i></a></button>";
                  echo " ";
                }
                ?>
              <!-- <button class='btn btn-info btn-ls tag'>test <a href='' <i class='fa fa-wrench'></i></a>  <a href='' <i class='fa fa-times'></i></a></button> -->

                <div class="margin-top-10">
                  <a href="addJobList"><button type="button" class="btn green"><i class="fa fa-plus"></i> เพิ่มประเภทงาน </button></a>
                </div>
            </div>

        </div>
      <!-- </form> -->
    </div>
</div>
</div>
<div class="portlet light portlet-fit ">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-puzzle font-green"></i>
            <span class="caption-subject font-green bold uppercase">ประเภทของธุรกิจ</span>
        </div>
    </div>
    <div class="portlet-body row">
      <!-- <form action="" method="post"> -->
        <div class="form-group">
            <label class="control-label col-md-2">ประเภทของธุรกิจ</label>
            <div class="col-md-10">
                <!-- <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput"> -->
                <?php
                foreach ($companyWork as $key => $value) {
                  echo "<button style='margin:2px' class='btn btn-info btn-ls tag'>".$value->name." <a href='editCompanyCategory?id=$key' <i class='fa fa-wrench'></i></a>  <a href='deleteCompanyCategory?id=$key' <i class='fa fa-times'></i></a></button>";
                  echo " ";
                }
                ?>

                <div class="margin-top-10">
                  <a href="addCompanyCategory"><button type="button" class="btn green"><i class="fa fa-plus"></i> เพิ่มประเภทธุรกิจ </button></a>
                </div>
            </div>

        </div>
      <!-- </form> -->
    </div>
</div>

</div>
<!-- <div class="portlet light portlet-fit ">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-magnifier font-green"></i>
            <span class="caption-subject font-green bold uppercase">Tags เทคโนโลยี / ความสนใจ</span>
        </div>
    </div>
    <div class="portlet-body row">
      <form action="tagsedit" method="post">
        <div class="form-group">
            <label class="control-label col-md-2">เทคโนโลยี / ความสนใจ</label>
            <div class="col-md-10">
                <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput">
                <div class="margin-top-10">
                  <button type="submit" class="btn yellow"><i class="fa fa-wrench"></i> แก้ไข Tags </button>
                </div>
            </div>

        </div>
      </form>

      
    </div>
</div> -->
