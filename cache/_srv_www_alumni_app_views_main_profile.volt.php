<div class="profile" style="background: #fff;padding: 20px;" ng-app="mainApp" ng-controller="mainController">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> <?= $fname ?> <?= $lname ?> </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-3">
                      <img src="<?= $imgProfile ?>" class="img-responsive pic-bordered" alt="" />
                      <table style="background: aliceblue;" class="col-xs-12">
                        <thead>
                            <tr>
                              <th class="col-md-2" style="text-align:center;"></th>
                              <th class="col-md-10"></th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr class="profile-list">
                            <td class="text-center"><i class="fa fa-user"></i></td>
                            <td> <?= $gender ?> </td>
                          </tr>
                          <tr class="profile-list">
                            <td class="text-center"><i class="fa fa-map-marker"></i></td>
                            <td> <?= $address ?> <br> จ.<?= $city ?> </td>
                          </tr>
                          <tr class="profile-list">
                            <td class="text-center"><i class="fa fa-phone"></i></td>
                            <td> <?= $phone ?> </td>
                          </tr>
                          <tr class="profile-list">
                            <td class="text-center"><i class="fa fa-facebook"></i></td>
                            <td> <a href="http://www.facebook.com/<?= $facebookUid ?>"><?= $fname ?> <?= $lname ?></a> </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                          <div class="col-md-12 profile-info">
                              <h1 class="font-green sbold uppercase"><?= $fname ?> <?= $lname ?> (<?= $efname ?> <?= $elname ?>)</h1>
                              <hr>
                              <div class="col-xs-4 col-md-3">
                                <span class="font-green sbold uppercase">ชื่อเล่น </span><br/>
                                <span class="font-green sbold uppercase">เกียร์ </span><br/>
                                <span class="font-green sbold uppercase">รุ่น </span><br/>
                                <span class="font-green sbold uppercase">รหัสนักศึกษา </span><br/>
                                <span class="font-green sbold uppercase">ระยะเวลาที่ศึกษา </span><br/>
                                <span class="font-green sbold uppercase">อาจารย์ที่ปรึกษา </span><br/>
                              </div>

                              <div class="col-xs-8 col-md-9">
                                <span> <?= $nickname ?> </span><br/>
                                <span> <?= $engGear ?> </span><br/>
                                <span> <?= $alumniType ?>#<?= $gen ?> </span><br/>
                                <span> <?= $studentId ?> </span><br/>
                                <span> <?= $entryYear ?> - <?= $graduateYear ?> </span><br/>
                                <span> <?= $advisorName ?> </span><br/>
                              </div>
                          </div>

                        </div>
                        <!--end row-->
                        <hr/>
                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_11" data-toggle="tab"> ประวัติการทำงาน  </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_11">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                          <thead>
                                              <tr>
                                                  <th class="col-md-6">
                                                      <i class="fa fa-briefcase"></i> ชื่อหน่วยงาน </th>
                                                  <th class="col-md-3">
                                                      <i class="fa fa-user"></i> ลักษณะงาน </th>
                                                  <th class="col-md-3">
                                                      <i class="fa fa-calendar"></i> ช่วงเวลา </th>
                                              </tr>
                                          </thead>

                                            <tbody>

                                              <?php
                                              foreach ($userJob as $key => $value) {
                                                echo "<tr>";

                                                  echo "<td><a href='javascript:;'>".$value['companyName']."</a></td>";
                                                  echo "<td>";
                                                  foreach ($value['jobs'] as $subKey => $subValue) {
                                                      if($subKey+1 == count($value['jobs']))
                                                        echo $subValue;
                                                      else
                                                        echo $subValue." ,";
                                                  }
                                                  echo "</td>";
                                                  echo "<td>".$value['startWork']." - ".$value['endWork']."</td>";

                                                echo "</tr>";
                                              }
                                              ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
  .form-group {
    margin-left: 0px;
    margin-right: 0px;
  }
  .profile-list {
    height: 30px;
    border-bottom: 1px solid white;
  }
</style>
