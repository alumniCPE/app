# CPE-ALUMNI (source file) README #

This repository contains source file from Project **ALUMNI CPE**

**app, public, index** repository is required

### File Directory ###

- **cache** - _cache file from system (ignored)_ 
- **config** - _configuration file (firebase and facebook OAuth)_
- **controllers** - _functions for each features in website_
- **views** - _user interface file in each pages_

*public and index files are contains in another repository*

### Installation ###
- delete **.git** file ( or type `rm -rf .git` in command line )
- create folder to contains web **assets** and **source** files
- download **app** repository into project folder
- download **public** repository into the project folder
- download **index** repository into the project folder then move files outside from index folder

### File Structure ###
    cpealumni
        |_ app
        |_ public
        |_ .htaccess
        |_ .htrouter.php
        |_ composer.bat
        |_ index.html
