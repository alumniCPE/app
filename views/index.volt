<!DOCTYPE html>

<html lang="en">

    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>{{webtitle}}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta content="Alumni,ศิษย์,ศิษย์เก่า,มช,วิดวะคอมมช,วิดวะมช,วิศวะคอมมช,วิศวะคอม,วิศวะ,คอม,วิดวะคอม,CPE,CMU,CPECMU,Computer,ComputerEngineering" name="keywords"/>
        <link rel="icon" type="image/png" sizes="16x16" href="../public/img/favico/favicon.ico">
        {{ getTitle() }}
        {{ assets.outputCss() }}

    <body class="">
        <!-- BEGIN HEADER -->
        {{ assets.outputJs() }}
        {{ content() }}
        <!-- END CONTAINER -->


    </body>

</html>
