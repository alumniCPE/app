<div class="profile" style="background: #fff;padding: 20px;">
    <div class="tabbable-line tabbable-full-width">
        <h2 class="font-green sbold">{{name}}</h2>
        <hr/>

        <div class="row">
          <div class="col-xs-12">
            <table>
              <thead>
                <tr>
                  <th class="col-xs-1"></th>
                  <th class="col-xs-11"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><i class="icon-prev fa fa-map-marker"></i></td>
                  <td>{{address}} <br/>  จ.{{city}}</td>
                </tr>
                <tr>
                  <td><i class="icon-prev fa fa-phone"></i></td>
                  <td>{{phone}}</td>
                </tr>
                <tr>
                  <td><i class="icon-prev fa fa-globe"></i></td>
                  <td><a>{{website}}</a></td>
                </tr>
                <tr>
                  <td><i class="icon-prev fa fa-facebook-official"></i></td>
                  <td><a>{{facebook}}</a></td>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="col-xs-12"><br>
            <strong>ประเภทของธุรกิจ</strong>
            <?php
              foreach ($work as $key => $value) {
                echo "<button type='button' class='tags btn blue btn-outline'>".$value['name']."</button> ";
              }
            ?>
          </div>

        </div>
    </div>
</div>
