<div class="profile" style="background: #fff;padding: 20px;" ng-app="mainApp" ng-controller="mainController">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> {{fname}} {{lname}} </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li>
                                <img src="{{imgProfile}}" class="img-responsive pic-bordered" alt="" />
                                {#<a href="javascript:;" class="profile-edit"> edit </a>#}
                            </li>

                            <!-- <li> <a> <i class="fa fa-birthday-cake"></i> 18 Jan 1982 </a></li> -->
                            <li> <a> <i class="fa fa-user"></i> {{gender}} </a></li>
                            <li> <a> <i class="fa fa-map-marker"></i> {{address}} จ.{{city}}</a></li>


                            <!-- <li> <a> <i class="fa fa-tasks"></i> Network </a></li>
                            <li> <a> <i class="fa fa-briefcase"></i> JohnDoe Co.Ltd </a></li>
                            <li> <a> <i class="fa fa-map-marker"></i> Kalaland </a></li> -->

                            <li> <a> <i class="fa fa-phone"></i> {{phone}} </a></li>
                            <li> <a href="http://www.facebook.com/{{facebookUid}}"> <i class="fa fa-facebook"></i> Facebook </a></li>

                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-8 profile-info">
                                <h1 class="font-green sbold uppercase">{{fname}} {{lname}} ({{efname}} {{elname}})</h1>
                                <hr>
                                <span class="font-green sbold uppercase">ชื่อเล่น : </span> {{nickname}}
                                <br/>
                                <span class="font-green sbold uppercase">เกียร์ : </span> {{engGear}}
                                <br/>
                                <span class="font-green sbold uppercase">รุ่น : </span> {{alumniType}}#{{gen}}
                                <br/>
                                <span class="font-green sbold uppercase">รหัสนักศึกษา : </span> {{studentId}}
                                <br/>
                                <span class="font-green sbold uppercase">ปีที่เริ่มการศึกษา : </span> {{entryYear}} - {{graduateYear}}
                                <br/>
                                <span class="font-green sbold uppercase">อาจารย์ที่ปรึกษา : </span> {{advisorName}}

                            </div>
                            <!--end col-md-8--
                            <div class="col-md-4">
                                <div class="portlet sale-summary">
                                    <div class="portlet-title">
                                        <div class="caption font-red sbold"> Sales Summary </div>
                                        <div class="tools">
                                            <a class="reload" href="javascript:;"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <ul class="list-unstyled">
                                            <li>
                                                <span class="sale-info"> TODAY SOLD
                                                    <i class="fa fa-img-up"></i>
                                                </span>
                                                <span class="sale-num"> 23 </span>
                                            </li>
                                            <li>
                                                <span class="sale-info"> WEEKLY SALES
                                                    <i class="fa fa-img-down"></i>
                                                </span>
                                                <span class="sale-num"> 87 </span>
                                            </li>
                                            <li>
                                                <span class="sale-info"> TOTAL SOLD </span>
                                                <span class="sale-num"> 2377 </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--end col-md-4-->
                        </div>
                        <!--end row-->
                        <hr/>
                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_11" data-toggle="tab"> ประวัติการทำงาน  </a>
                                </li>
                                <li>
                                    <a href="#tab_1_12" data-toggle="tab"> ประวัติการศึกษา  </a>
                                </li>
                                <li>
                                    <a href="#tab_1_13" data-toggle="tab"> ประกาศนียบัตร  </a>
                                </li>
                                {#<li>
                                    <a href="#tab_1_22" data-toggle="tab"> ผลงานและรางวัลที่ได้รับ </a>
                                </li>#}
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_11">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <i class="fa fa-briefcase"></i> ชื่อหน่วยงาน </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-question"></i> ลักษณะงาน </th>
                                                    <th style='text-align:center'>
                                                        <i class="fa fa-bookmark"></i> ช่วงเวลา </th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                              <?php
                                              foreach ($userJob as $key => $value) {
                                                echo "<tr>";

                                                  echo "<td><a href='javascript:;'>".$value['companyName']."</a></td>";
                                                  echo "<td>";
                                                  foreach ($value['jobs'] as $subKey => $subValue) {
                                                      if($subKey+1 == count($value['jobs']))
                                                        echo $subValue;
                                                      else
                                                        echo $subValue." ,";
                                                  }
                                                  echo "</td>";
                                                  echo "<td style='text-align:center'>".$value['startWork']." - ".$value['endWork']."</td>";

                                                echo "</tr>";
                                              }
                                              ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_1_12">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <i class="fa fa-briefcase"></i> ระดับการศึกษา </th>
                                                    <th>
                                                        <i class="fa fa-briefcase"></i> หลักสูตร/สาขา </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-question"></i> ปีที่เริ่มการศึกษา </th>
                                                    <th>
                                                        <i class="fa fa-question"></i> ปีที่จบการศึกษา </th>
                                                    <th>
                                                        <i class="fa fa-bookmark"></i> สถานศึกษา </th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                              <?php
                                              foreach ($education as $key => $value) {
                                                echo "<tr>";

                                                  echo "<td>".$value->course."</td>";
                                                  echo "<td>".$value->major."</td>";
                                                  echo "<td>".$value->startStudy."</td>";
                                                  echo "<td>".$value->endStudy."</td>";
                                                  echo "<td>".$value->university."</td>";

                                                echo "</tr>";
                                              }
                                              ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_1_13">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <i class="fa fa-briefcase"></i> ประกาศนียบัตรวิชาชีพ </th>
                                                    <th>
                                                        <i class="fa fa-bookmark"></i> ผู้ออกใบประกาศ </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-question"></i> ปีที่ได้รับ </th>
                                                    
                                                </tr>
                                            </thead>

                                            <tbody>

                                              <?php
                                              foreach ($certificate as $key => $value) {
                                                echo "<tr>";

                                                  echo "<td>".$value->certificate."</td>";
                                                  echo "<td>".$value->certifier."</td>";
                                                  echo "<td>".$value->receipt."</td>";
                                                  
                                                echo "</tr>";
                                              }
                                              ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
