<div class="portlet light" ng-app="addCompany" ng-controller="addcompanyController">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase">{{topic_header}}</span>
    </div>
</div>

<div class="portlet-body">
    <!-- BEGIN FORM-->
    <form action="addedEducation" class="form-horizontal" id="form_sample_1" method="post">
        <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">ระดับการศึกษา</label>
              <div class="col-md-4">
              <select name="course" id="work_list" class="form-control" >
                  <option hidden> เลือกระดับการศึกษา </option>
                  <option value="ตรี">ตรี</option>
                  <option value="โท">โท</option>
                  <option value="เอก">เอก</option>
                </select>
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">หลักสูตร/สาขา</label>
                <div class="col-md-4">
                    <input type="text" class="listCompany form-control" rows="3" name="major" value="">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">ปีที่เริ่มการศึกษา (พ.ศ.)</label>
              <div class="col-md-4">
              <select name="startStudy" id="work_list" class="form-control" >
                  <option hidden> เลือกปี (พ.ศ.) </option>
                  <option value="ปัจจุบัน"> ปัจจุบัน </option>
                  {% for year in yearlist %}
                    <option value="{{year}}">{{year}}</option>
                  {% endfor %}

              </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">ปีที่จบการศึกษา (พ.ศ.)</label>
              <div class="col-md-4">
              <select name="endStudy" id="work_list" class="form-control" >
                <option hidden> เลือกปี (พ.ศ.) </option>
                <option value="ปัจจุบัน"> ปัจจุบัน </option>
                {% for year in yearlist %}
                  <option value="{{year + 4}}">{{year + 4}}</option>
                {% endfor %}

              </select>
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">สถานศึกษา</label>
                <div class="col-md-4">
                    <input type="text" class="listCompany form-control" rows="3" name="university" value="">
                </div>
            </div>

            <div class="form-group">
            <label class="control-label col-md-3"></label>
              <div class="col-md-4">
                 <p id="checkedParameterText" style="color:red;"></p>
              </div>
            </div>

            <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button id="addedEducation" type="submit" class="btn green">เพิ่ม</button>
                    <button type="reset" class="btn default">รีเซ็ต</button>
                </div>
            </div>
            </div>
        </div>
    </form>
    <!-- END FORM-->
</div>
</div>
<script src="{{baseUrl}}public/js/angular/angular.min.js"></script>
<script src="{{baseUrl}}public/js/angular/angular-route.min.js"></script>
<script src="{{baseUrl}}public/js/angular/angular-sanitize.min.js"></script>
<?php $random = rand(0,99999);
  echo "<script src='".$baseUrl."public/js/admin/addCompany.js?".$random."'></script>";
?>
