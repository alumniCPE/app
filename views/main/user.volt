<div class="profile" style="background: #fff;padding: 20px;">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> {{fname}} {{lname}} </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li>
                                <img src="../assets/pages/media/profile/people19.png" class="img-responsive pic-bordered" alt="" />
                                {#<a href="javascript:;" class="profile-edit"> edit </a>#}
                            </li>

                            <!-- <li> <a> <i class="fa fa-birthday-cake"></i> 18 Jan 1982 </a></li> -->
                            <li> <a> <i class="fa fa-user"></i> {{gender}} </a></li>
                            <li> <a> <i class="fa fa-map-marker"></i> {{address}} จ.{{city}}</a></li>


                            <!-- <li> <a> <i class="fa fa-tasks"></i> Network </a></li>
                            <li> <a> <i class="fa fa-briefcase"></i> JohnDoe Co.Ltd </a></li>
                            <li> <a> <i class="fa fa-map-marker"></i> Kalaland </a></li> -->

                            <li> <a> <i class="fa fa-phone"></i> {{phone}} </a></li>
                            <li> <a> <i class="fa fa-facebook"></i> {{facebook}} </a></li>

                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-8 profile-info">
                                <h1 class="font-green sbold uppercase">{{fname}} {{lname}}</h1>
                                <hr>
                                <span class="font-green sbold uppercase">รหัสนักศึกษา : </span> {{studentId}}
                                <br/>
                                <span class="font-green sbold uppercase">ปีที่เริ่มการศึกษา : </span> {{entryYear}} - {{graduateYear}}
                                <br/>
                                <span class="font-green sbold uppercase">อาจารย์ที่ปรึกษา : </span> {{advisor}}

                            </div>
                            <!--end col-md-8--
                            <div class="col-md-4">
                                <div class="portlet sale-summary">
                                    <div class="portlet-title">
                                        <div class="caption font-red sbold"> Sales Summary </div>
                                        <div class="tools">
                                            <a class="reload" href="javascript:;"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <ul class="list-unstyled">
                                            <li>
                                                <span class="sale-info"> TODAY SOLD
                                                    <i class="fa fa-img-up"></i>
                                                </span>
                                                <span class="sale-num"> 23 </span>
                                            </li>
                                            <li>
                                                <span class="sale-info"> WEEKLY SALES
                                                    <i class="fa fa-img-down"></i>
                                                </span>
                                                <span class="sale-num"> 87 </span>
                                            </li>
                                            <li>
                                                <span class="sale-info"> TOTAL SOLD </span>
                                                <span class="sale-num"> 2377 </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--end col-md-4-->
                        </div>
                        <!--end row-->
                        <hr/>
                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_11" data-toggle="tab"> ประวัติการทำงาน  </a>
                                </li>
                                {#<li>
                                    <a href="#tab_1_22" data-toggle="tab"> ผลงานและรางวัลที่ได้รับ </a>
                                </li>#}
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_11">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <i class="fa fa-briefcase"></i> บริษัท </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-question"></i> ตำแหน่ง </th>
                                                    <th>
                                                        <i class="fa fa-bookmark"></i> ปีที่เข้าทำงาน </th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td> <a href="javascript:;"> Pixel Ltd </a> </td>
                                                    <td> Server Administrator </td>
                                                    <td> 2017 - 2027 </td>
                                                </tr>

                                                <tr>
                                                    <td> <a href="javascript:;"> Pixil Ltd </a> </td>
                                                    <td> Programmer </td>
                                                    <td> 2012 - 2017 </td>
                                                </tr>

                                                <tr>
                                                    <td> <a href="javascript:;"> Pixol Ltd </a> </td>
                                                    <td> Junior Tester </td>
                                                    <td> 2010 - 2012 </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--tab_1_2-->
            
        </div>
    </div>
</div>
