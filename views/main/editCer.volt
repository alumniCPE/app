<div class="portlet light" ng-app="addCompany" ng-controller="addcompanyController">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase">{{topic_header}}</span>
    </div>
</div>

<div class="portlet-body">
    <!-- BEGIN FORM-->
    <form action="editedCer" class="form-horizontal" id="form_sample_1" method="post">
        <div class="form-body">
            <div class="form-group">
                <label class="control-label col-md-3">ประกาศนียบัตรวิชาชีพ</label>
                <div class="col-md-4">
                    <input type="text" class="listCompany form-control" name="certificate" value="{{certificate}}">
                </div>
                <small>(เช่น MCSE, CCNA, CCIE, Security+, Cloud+, Base Programmer, ... )</small>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">หน่วยงานที่ออกใบประกาศ</label>
                <div class="col-md-4">
                    <input type="text" class="listCompany form-control" name="certifier" value="{{certifier}}">
                </div>
                <small>(เช่น Microsoft, Sisco, CompTIA, SAS, SAP, ...)</small>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">ปีที่ได้รับ (พ.ศ.)</label>
              <div class="col-md-4">
              <select name="receipt" id="work_list" class="form-control" >
                <option value="{{receipt}}">{{receipt}}</option>
                {% for year in yearlist %}
                  <option value="{{year + 1}}">{{year + 1}}</option>
                {% endfor %}
              </select>
              </div>
            </div>


            <div class="form-group">
            <label class="control-label col-md-3"></label>
              <div class="col-md-4">
                 <p id="checkedParameterText" style="color:red;"></p>
              </div>
            </div>

            <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button id="addedCer" type="submit" class="btn green">แก้ไข</button>
                    <button type="reset" class="btn default">รีเซ็ต</button>
                </div>
            </div>
            </div>
        </div>
    </form>
    <!-- END FORM-->
</div>
</div>