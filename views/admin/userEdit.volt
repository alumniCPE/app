<div class="profile" style="background: #fff;padding: 20px;" ng-app="adminApp" ng-controller="adminController">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Edit Profile </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row profile-account">
                    <div class="col-md-3">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">
                            <li class="active">
                                <a data-toggle="tab" href="#tab_1-1">
                                    <i class="fa fa-user"></i> ข้อมูลส่วนตัว </a>
                                <span class="after"> </span>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_2-2">
                                    <i class="fa fa-picture-o"></i> รูปประจำตัว </a>
                            </li>
                            <!-- <li>
                                <a data-toggle="tab" href="#tab_3-3">
                                    <i class="fa fa-file-text-o"></i> บัญชีผู้ใช้ </a>
                            </li> -->
                            <li>
                                <a data-toggle="tab" href="#tab_4-4">
                                    <i class="fa fa-briefcase"></i> การศึกษาและอาชีพ </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                <form role="form" action="editProfile?studentId={{studentId}}" method="post">
                                <h3 class="block">ข้อมูลส่วนตัว</h3>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">ชื่อ (ภาษาไทย)</label>
                                        <input type="text" class="form-control" name="fname" value="{{fname}}" /> </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">นามสกุล (ภาษาไทย)</label>
                                        <input type="text" class="form-control" name="lname" value="{{lname}}" /> </div>

                                    <div class="form-group col-md-6">
                                        <label class="control-label">ชื่อ (ภาษาอังกฤษ)</label>
                                        <input type="text" class="form-control" name="efname" value="{{efname}}" /> </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">นามสกุล (ภาษาอังกฤษ)</label>
                                        <input type="text" class="form-control" name="elname" value="{{elname}}" /> </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">ชื่อเล่น</label>
                                        <input type="text" class="form-control" name="nickname" value="{{nickname}}" /> </div>
                                    <div class="form-group col-md-6">
                                      <label class="control-label">อีเมล</label>
                                      <input type="text" class="form-control" name="email" value="{{email}}" /> </div>

                                  <div class="form-group col-md-12">
                                  <label class="control-label">เพศ</label>
                                    <div class="radio-list">
                                      <?php
                                          if($gender == "ชาย")
                                            echo "<label><input type='radio' name='gender' value='ชาย'' data-title='ชาย' checked/> ชาย </label>";
                                          else
                                            echo "<label><input type='radio' name='gender' value='ชาย'' data-title='ชาย' /> ชาย </label>";

                                          if($gender == "หญิง")
                                            echo "<label><input type='radio' name='gender' value='หญิง'' data-title='หญิง' checked/> หญิง </label>";
                                          else
                                            echo "<label><input type='radio' name='gender' value='หญิง'' data-title='หญิง' /> หญิง </label>";
                                      ?>
                                    </div>
                                    <div id="form_gender_error"> </div>
                                    </div>

                                    <div class="form-group col-xs-12">
                                        <label class="control-label">ที่อยู่ปัจจุบัน</label>
                                        <textarea class="form-control" rows="3" name="address" value="{{address}}">{{address}}</textarea>
                                    </div>

                                    <div class="form-group col-xs-12">
                                      <label class="control-label">จังหวัด</label>
                                        <select name="city" id="city_list" class="form-control" >
                                          <option value="{{city}}" selected>{{city}}</option>
                                          <option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
                                          <option value="กระบี่">กระบี่ </option>
                                          <option value="กาญจนบุรี">กาญจนบุรี </option>
                                          <option value="กาฬสินธุ์">กาฬสินธุ์ </option>
                                          <option value="กำแพงเพชร">กำแพงเพชร </option>
                                          <option value="ขอนแก่น">ขอนแก่น</option>
                                          <option value="จันทบุรี">จันทบุรี</option>
                                          <option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
                                          <option value="ชัยนาท">ชัยนาท </option>
                                          <option value="ชัยภูมิ">ชัยภูมิ </option>
                                          <option value="ชุมพร">ชุมพร </option>
                                          <option value="ชลบุรี">ชลบุรี </option>
                                          <option value="เชียงใหม่">เชียงใหม่ </option>
                                          <option value="เชียงราย">เชียงราย </option>
                                          <option value="ตรัง">ตรัง </option>
                                          <option value="ตราด">ตราด </option>
                                          <option value="ตาก">ตาก </option>
                                          <option value="นครนายก">นครนายก </option>
                                          <option value="นครปฐม">นครปฐม </option>
                                          <option value="นครพนม">นครพนม </option>
                                          <option value="นครราชสีมา">นครราชสีมา </option>
                                          <option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
                                          <option value="นครสวรรค์">นครสวรรค์ </option>
                                          <option value="นราธิวาส">นราธิวาส </option>
                                          <option value="น่าน">น่าน </option>
                                          <option value="นนทบุรี">นนทบุรี </option>
                                          <option value="บึงกาฬ">บึงกาฬ</option>
                                          <option value="บุรีรัมย์">บุรีรัมย์</option>
                                          <option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
                                          <option value="ปทุมธานี">ปทุมธานี </option>
                                          <option value="ปราจีนบุรี">ปราจีนบุรี </option>
                                          <option value="ปัตตานี">ปัตตานี </option>
                                          <option value="พะเยา">พะเยา </option>
                                          <option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
                                          <option value="พังงา">พังงา </option>
                                          <option value="พิจิตร">พิจิตร </option>
                                          <option value="พิษณุโลก">พิษณุโลก </option>
                                          <option value="เพชรบุรี">เพชรบุรี </option>
                                          <option value="เพชรบูรณ์">เพชรบูรณ์ </option>
                                          <option value="แพร่">แพร่ </option>
                                          <option value="พัทลุง">พัทลุง </option>
                                          <option value="ภูเก็ต">ภูเก็ต </option>
                                          <option value="มหาสารคาม">มหาสารคาม </option>
                                          <option value="มุกดาหาร">มุกดาหาร </option>
                                          <option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
                                          <option value="ยโสธร">ยโสธร </option>
                                          <option value="ยะลา">ยะลา </option>
                                          <option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
                                          <option value="ระนอง">ระนอง </option>
                                          <option value="ระยอง">ระยอง </option>
                                          <option value="ราชบุรี">ราชบุรี</option>
                                          <option value="ลพบุรี">ลพบุรี </option>
                                          <option value="ลำปาง">ลำปาง </option>
                                          <option value="ลำพูน">ลำพูน </option>
                                          <option value="เลย">เลย </option>
                                          <option value="ศรีสะเกษ">ศรีสะเกษ</option>
                                          <option value="สกลนคร">สกลนคร</option>
                                          <option value="สงขลา">สงขลา </option>
                                          <option value="สมุทรสาคร">สมุทรสาคร </option>
                                          <option value="สมุทรปราการ">สมุทรปราการ </option>
                                          <option value="สมุทรสงคราม">สมุทรสงคราม </option>
                                          <option value="สระแก้ว">สระแก้ว </option>
                                          <option value="สระบุรี">สระบุรี </option>
                                          <option value="สิงห์บุรี">สิงห์บุรี </option>
                                          <option value="สุโขทัย">สุโขทัย </option>
                                          <option value="สุพรรณบุรี">สุพรรณบุรี </option>
                                          <option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
                                          <option value="สุรินทร์">สุรินทร์ </option>
                                          <option value="สตูล">สตูล </option>
                                          <option value="หนองคาย">หนองคาย </option>
                                          <option value="หนองบัวลำภู">หนองบัวลำภู </option>
                                          <option value="อำนาจเจริญ">อำนาจเจริญ </option>
                                          <option value="อุดรธานี">อุดรธานี </option>
                                          <option value="อุตรดิตถ์">อุตรดิตถ์ </option>
                                          <option value="อุทัยธานี">อุทัยธานี </option>
                                          <option value="อุบลราชธานี">อุบลราชธานี</option>
                                          <option value="อ่างทอง">อ่างทอง </option>
                                        </select>
                                    </div>

                                    <div class="form-group col-xs-12">
                                        <label class="control-label">เบอร์โทรติดต่อ</label>
                                        <input type="text" class="form-control" name="phone" value="{{phone}}"/>
                                    </div>
                                    <h3 class="block">ข้อมูลการศึกษา</h3>

                                    <div class="form-group col-md-6">
                                      <label class="control-label">หลักสูตรที่จบการศึกษา</label>
                                      <select name="alumniType" class="form-control" >
                                        <option value="{{alumniType}}" selected hidden>{{alumniType}}</option>
                                        <option value="CPE"> CPE </option>
                                        <option value="ISNE"> ISNE </option>
                                        <option value="MCPE"> MCPE </option>
                                        <option value="PHDCPE"> PHDCPE </option>
                                      </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label class="control-label">รุ่น</label>
                                        <input type="text" class="form-control" name="gen" value="{{gen}}" /> </div>
                                    <div id="form_gender_error"> </div>

                                    <div class="form-group col-md-6">
                                      <label class="control-label">ปีที่จบการศึกษา</label>
                                        <select name="entryYear" id="city_list" class="form-control" >
                                          <option value="{{entryYear}}" selected>{{entryYear}}</option>
                                          <option value="2534">2534</option>
                                          <option value="2535">2535</option>
                                          <option value="2536">2536</option>
                                          <option value="2537">2537</option>
                                          <option value="2538">2538</option>
                                          <option value="2539">2539</option>
                                          <option value="2540">2540</option>
                                          <option value="2541">2541</option>
                                          <option value="2542">2542</option>
                                          <option value="2543">2543</option>
                                          <option value="2544">2544</option>
                                          <option value="2545">2545</option>
                                          <option value="2546">2546</option>
                                          <option value="2547">2547</option>
                                          <option value="2548">2548</option>
                                          <option value="2549">2549</option>
                                          <option value="2550">2550</option>
                                          <option value="2551">2551</option>
                                          <option value="2552">2552</option>
                                          <option value="2553">2553</option>
                                          <option value="2554">2554</option>
                                          <option value="2555">2555</option>
                                          <option value="2556">2556</option>
                                          <option value="2557">2557</option>
                                          <option value="2558">2558</option>
                                          <option value="2559">2559</option>
                                          <option value="2560">2560</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                      <label class="control-label">ปีที่จบการศึกษา</label>
                                        <select name="graduateYear" id="city_list" class="form-control" >
                                          <option value="{{graduateYear}}" selected>{{graduateYear}}</option>
                                          <option value="2534">2534</option>
                                          <option value="2535">2535</option>
                                          <option value="2536">2536</option>
                                          <option value="2537">2537</option>
                                          <option value="2538">2538</option>
                                          <option value="2539">2539</option>
                                          <option value="2540">2540</option>
                                          <option value="2541">2541</option>
                                          <option value="2542">2542</option>
                                          <option value="2543">2543</option>
                                          <option value="2544">2544</option>
                                          <option value="2545">2545</option>
                                          <option value="2546">2546</option>
                                          <option value="2547">2547</option>
                                          <option value="2548">2548</option>
                                          <option value="2549">2549</option>
                                          <option value="2550">2550</option>
                                          <option value="2551">2551</option>
                                          <option value="2552">2552</option>
                                          <option value="2553">2553</option>
                                          <option value="2554">2554</option>
                                          <option value="2555">2555</option>
                                          <option value="2556">2556</option>
                                          <option value="2557">2557</option>
                                          <option value="2558">2558</option>
                                          <option value="2559">2559</option>
                                          <option value="2560">2560</option>
                                        </select>
                                    </div>


                                    <div class="form-group col-md-6">
                                      <label class="control-label">อาจารย์ที่ปรึกษา</label>
                                        <select name="advisor" id="city_list" class="form-control" >
                                          <option value="{{advisor}}" selected>{{advisorName}}</option>
                                          <?php
                                          foreach ($advisorList as $key => $value) {
                                            echo "<option value='$key'>".$value->name."</option>";
                                          }
                                          ?>
                                        </select>
                                    </div>


                                    <div class="col-xs-12 margiv-top-10">
                                        <button type="submit" class="btn green uppercase">บันทึก</button>
                                        <a href="javascript:;" class="btn default"> ยกเลิก </a>
                                    </div>
                                </form>
                            </div>
                            <div id="tab_2-2" class="tab-pane">
                                <form action="editImgProfile" role="form" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> เลือกรูปภาพ </span>
                                                    <span class="fileinput-exists"> เปลี่ยนรูปภาพ </span>
                                                    <input type="file" name="imgProfile[]" ng-file-select="onFileSelect($files)"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> นำรูปภาพออก </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="margin-top-10">
                                        <a href="javascript:;" class="btn green" ng-click="uploadFile()"> ตกลง </a>
                                        <a href="javascript:;" class="btn default"> ยกเลิก </a>
                                    </div>
                                </form>
                            </div>
                            <div id="tab_3-3" class="tab-pane">
                                <form action="resetPasswoed" method="post">
                                    <div class="form-group">
                                      <label class="control-label">รหัสนักศึกษา</label>
                                      <input type="number" class="form-control" value="{{studentId}}" readonly/> </div>
                                    <div class="form-group">
                                        <label class="control-label">แก้ไขรหัสผ่าน</label>
                                        <input id="password" type="password" class="form-control" name="password" mg-model="password" onkeyup="checkedPass()"/> </div>
                                    <div class="form-group">
                                        <label class="control-label">ยืนยันรหัสผ่าน</label>
                                        <input id="repassword" type="password" class="form-control" name="repassword" mg-model="repassword" onkeyup="checkedPass()"/> </div>
                                    <div class="form-group">
                                        <p id="checkedPassText" style="color:red;"></p>
                                    </div>

                                    <div class="margin-top-10">
                                        <button id="submitReset" type="submit" class="btn green uppercase" >บันทึก</button>
                                        <a href="javascript:;" class="btn default"> ยกเลิก </a>
                                    </div>
                                </form>
                            </div>
                            <div id="tab_4-4" class="tab-pane">
                              <h3>ประวัติการทำงาน</h3>
                                <form action="#">
                                  <table class="table table-striped table-bordered table-advance table-hover table-responsive" style='text-align: center;'>
                                    <thead>
                                        <tr>
                                            <th style='text-align: center;'>
                                                <i class="fa fa-briefcase"></i> บริษัท </th>
                                            <th style='text-align: center;'>
                                                <i class="fa fa-user"></i> ประเภทของงาน </th>
                                            <th style='text-align: center;'>
                                                <i class="fa fa-calendar-plus-o"></i> ปีที่เข้าทำงาน </th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                      <tbody>
                                          <?php
                                            foreach ($userJob as $key => $value) {
                                              echo "<tr>";

                                                echo "<td><a href='javascript:;'>".$value['companyName']."</a></td>";
                                                echo "<td>";
                                                foreach ($value['jobs'] as $subKey => $subValue) {
                                                    if($subKey+1 == count($value['jobs']))
                                                      echo $subValue;
                                                    else
                                                      echo $subValue." ,";
                                                }
                                                echo "</td>";
                                                echo "<td>".$value['startWork']." - ".$value['endWork']."</td>";
                                                echo "<td>"."
                                                <a href='editCompany?key=".$value['companyProfileKey']."' class='btn yellow'> แก้ไข </a>
                                                <a href='deleteCompany?key=".$value['companyProfileKey']."' class='btn red'> ลบ </a>"."</td>";

                                              echo "</tr>";
                                            }
                                          ?>
                                      </tbody>
                                  </table>
                                    <!--end profile-settings-->
                                    <div class="margin-top-10">
                                        <a href="addCompany" class="btn green"> เพิ่ม </a>
                                    </div>
                                </form>
                              <h3>ประวัติการศึกษา</h3>
                                <form action="#">
                                  <table class="table table-striped table-bordered table-advance table-hover table-responsive" style='text-align: center;'>
                                    <thead>
                                        <tr>
                                            <th style='text-align: center;'>
                                                  <i class="fa fa-graduation-cap"></i> ระดับการศึกษา </th>
                                              <th style='text-align: center;'>
                                                  <i class="fa fa-book"></i> หลักสูตร/สาขา </th>
                                              <th style='text-align: center;'>
                                                  <i class="fa fa-calendar"></i> ปีที่ศึกษา </th>
                                              <th style='text-align: center;'>
                                                  <i class="fa fa-university"></i> สถานศึกษา </th>
                                              <th></th>
                                        </tr>
                                    </thead>

                                      <tbody>
                                        <?php
                                        foreach ($education as $key => $value) {
                                          echo "<tr>";
                                            echo "<td>".$value->course."</td>";
                                            echo "<td>".$value->major."</td>";
                                            echo "<td>".$value->startStudy." - ".$value->endStudy."</td>";
                                            echo "<td>".$value->university."</td>";
                                            echo "<td>"."
                                            <a href='editEducation?key=".$key."' class='btn yellow'> แก้ไข </a>
                                            <a href='deleteEducation?key=".$key."' class='btn red'> ลบ </a>"."</td>";
                                          echo "</tr>";
                                        }
                                        ?>
                                      </tbody>
                                  </table>
                                    <!--end profile-settings-->
                                    <div class="margin-top-10">
                                        <a href="addEducation" class="btn green"> เพิ่ม </a>
                                    </div>
                                </form>

                              <h3>ประกาศนียบัตร</h3>
                                <form action="#">
                                  <table class="table table-striped table-bordered table-advance table-hover table-responsive" style='text-align: center;'>
                                      <thead>
                                          <tr>
                                              <th style='text-align: center;'>
                                                  <i class="fa fa-certificate"></i> ประกาศนียบัตรวิชาชีพ </th>
                                                <th style='text-align: center;'>
                                                  <i class="fa fa-university"></i> ผู้ออกใบประกาศ </th>
                                              <th style='text-align: center;'>
                                                  <i class="fa fa-calendar"></i> ปีที่ได้รับ </th>

                                              <th> </th>
                                          </tr>
                                      </thead>

                                      <tbody>
                                        <?php
                                        foreach ($certificate as $key => $value) {
                                          echo "<tr>";
                                            echo "<td>".$value->certificate."</td>";
                                            echo "<td>".$value->certifier."</td>";
                                            echo "<td>".$value->receipt."</td>";
                                            echo "<td>"."
                                            <a href='editCer?key=".$key."' class='btn yellow'> แก้ไข </a>
                                            <a href='deleteCer?key=".$key."' class='btn red'> ลบ </a>"."</td>";
                                          echo "</tr>";
                                        }
                                        ?>
                                      </tbody>
                                  </table>
                                    <!--end profile-settings-->
                                    <div class="margin-top-10">
                                        <a href="addCer" class="btn green"> เพิ่ม </a>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{baseUrl}}public/js/angular/angular.min.js"></script>
<script src="{{baseUrl}}public/js/angular/angular-route.min.js"></script>
<script src="{{baseUrl}}public/js/angular/angular-sanitize.min.js"></script>
<?php $random = rand(0,99999);
  echo "<script src='".$baseUrl."public/js/admin/script.js?".$random."'></script>";
?>
<script src="https://www.gstatic.com/firebasejs/4.0.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAVeY2EVXwt8TnklcacE7ORKnJBqwOtsgU",
    authDomain: "cpe-alumni.firebaseapp.com",
    databaseURL: "https://cpe-alumni.firebaseio.com",
    projectId: "cpe-alumni",
    storageBucket: "cpe-alumni.appspot.com",
    messagingSenderId: "227546499185"
  };
  firebase.initializeApp(config);
</script>
