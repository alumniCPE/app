<div class="portlet light" ng-app="addCompany" ng-controller="addcompanyController">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase">{{topic_header}}</span>
    </div>
</div>

<div class="portlet-body">
    <!-- BEGIN FORM-->
    <form action="addedEducation" class="form-horizontal" id="form_sample_1" method="post">
        <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">ระดับการศึกษา</label>
              <div class="col-md-4">
              <select name="course" id="work_list" class="form-control" >
                  <option hidden>--------- ระดับการศึกษา ---------</option>
                  <option value="ตรี">ปริญญาตรี</option>
                  <option value="โท">ปริญญาโท</option>
                  <option value="เอก">ปริญญาเอก</option>
                </select>
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">หลักสูตร/สาขา</label>
                <div class="col-md-4">
                    <input type="text" class="listCompany form-control" rows="3" name="major" value="">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">ปีที่เริ่มการศึกษา (พ.ศ.)</label>
              <div class="col-md-4">
              <select name="startStudy" id="work_list" class="form-control" >
                  <option hidden>--------- พุทธศักราช ---------</option>
                  <option value="2534">2534</option>
                  <option value="2535">2535</option>
                  <option value="2536">2536</option>
                  <option value="2537">2537</option>
                  <option value="2538">2538</option>
                  <option value="2539">2539</option>
                  <option value="2540">2540</option>
                  <option value="2541">2541</option>
                  <option value="2542">2542</option>
                  <option value="2543">2543</option>
                  <option value="2544">2544</option>
                  <option value="2545">2545</option>
                  <option value="2546">2546</option>
                  <option value="2547">2547</option>
                  <option value="2548">2548</option>
                  <option value="2549">2549</option>
                  <option value="2550">2550</option>
                  <option value="2551">2551</option>
                  <option value="2552">2552</option>
                  <option value="2553">2553</option>
                  <option value="2554">2554</option>
                  <option value="2555">2555</option>
                  <option value="2556">2556</option>
                  <option value="2557">2557</option>
                  <option value="2558">2558</option>
                  <option value="2559">2559</option>
                  <option value="2560">2560</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">ปีที่จบการศึกษา (พ.ศ.)</label>
              <div class="col-md-4">
              <select name="endStudy" id="work_list" class="form-control" >
                  <option hidden>--------- พุทธศักราช ---------</option>
                  <option value="2534">2534</option>
                  <option value="2535">2535</option>
                  <option value="2536">2536</option>
                  <option value="2537">2537</option>
                  <option value="2538">2538</option>
                  <option value="2539">2539</option>
                  <option value="2540">2540</option>
                  <option value="2541">2541</option>
                  <option value="2542">2542</option>
                  <option value="2543">2543</option>
                  <option value="2544">2544</option>
                  <option value="2545">2545</option>
                  <option value="2546">2546</option>
                  <option value="2547">2547</option>
                  <option value="2548">2548</option>
                  <option value="2549">2549</option>
                  <option value="2550">2550</option>
                  <option value="2551">2551</option>
                  <option value="2552">2552</option>
                  <option value="2553">2553</option>
                  <option value="2554">2554</option>
                  <option value="2555">2555</option>
                  <option value="2556">2556</option>
                  <option value="2557">2557</option>
                  <option value="2558">2558</option>
                  <option value="2559">2559</option>
                  <option value="2560">2560</option>
                </select>
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">สถานศึกษา</label>
                <div class="col-md-4">
                    <input type="text" class="listCompany form-control" rows="3" name="university" value="">
                </div>
            </div>

            <div class="form-group">
            <label class="control-label col-md-3"></label>
              <div class="col-md-4">
                 <p id="checkedParameterText" style="color:red;"></p>
              </div>
            </div>

            <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button id="addedEducation" type="submit" class="btn green">เพิ่ม</button>
                    <button type="reset" class="btn default">รีเซ็ต</button>
                </div>
            </div>
            </div>
        </div>
    </form>
    <!-- END FORM-->
</div>
</div>
