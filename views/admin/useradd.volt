<div class="profile" style="background: #fff;padding: 20px;" ng-app="adminApp" ng-controller="adminController">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> เพิ่มข้อมูลศิษย์เก่า </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row profile-account">

                    <div class="col-xs-12">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                <form role="form" action="useradded" method="post">
                                <h3 class="block">ข้อมูลส่วนตัว</h3>
                                    <div class="form-group col-xs-12">
                                        <label class="control-label">รหัสนักศึกษา</label>
                                        <input type="text" class="form-control" name="studentId" value="" /> </div>

                                    <div class="form-group col-md-6">
                                        <label class="control-label">ชื่อ (ภาษาไทย)</label>
                                        <input type="text" class="form-control" name="fname" value="" /> </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">นามสกุล (ภาษาไทย)</label>
                                        <input type="text" class="form-control" name="lname" value="" /> </div>

                                    <div class="form-group col-md-6">
                                        <label class="control-label">ชื่อ (ภาษาอังกฤษ)</label>
                                        <input type="text" class="form-control" name="efname" value="" /> </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">นามสกุล (ภาษาอังกฤษ)</label>
                                        <input type="text" class="form-control" name="elname" value="" /> </div>

                                    <div class="form-group col-md-6">
                                        <label class="control-label">ชื่อเล่น</label>
                                        <input type="text" class="form-control" name="nickname" value="" /> </div>

                                  <div class="form-group col-xs-12">
                                  <label class="control-label">เพศ</label>
                                    <div class="radio-list">
                                      <label><input type='radio' name='gender' value='ชาย' data-title='ชาย' /> ชาย </label>
                                      <label><input type='radio' name='gender' value='หญิง' data-title='หญิง' /> หญิง </label>
                                    </div>
                                    <div id="form_gender_error"> </div>
                                    </div>

                                    <div class="form-group col-xs-12">
                                        <label class="control-label">ที่อยู่ปัจจุบัน</label>
                                        <textarea class="form-control" rows="3" name="address" value=""></textarea>
                                    </div>

                                    <div class="form-group col-md-6">
                                      <label class="control-label">จังหวัด</label>
                                        <select name="city" id="city_list" class="form-control" >
                                          <option hidden> เลือกจังหวัด </option>
                                          <option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
                                          <option value="กระบี่">กระบี่ </option>
                                          <option value="กาญจนบุรี">กาญจนบุรี </option>
                                          <option value="กาฬสินธุ์">กาฬสินธุ์ </option>
                                          <option value="กำแพงเพชร">กำแพงเพชร </option>
                                          <option value="ขอนแก่น">ขอนแก่น</option>
                                          <option value="จันทบุรี">จันทบุรี</option>
                                          <option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
                                          <option value="ชัยนาท">ชัยนาท </option>
                                          <option value="ชัยภูมิ">ชัยภูมิ </option>
                                          <option value="ชุมพร">ชุมพร </option>
                                          <option value="ชลบุรี">ชลบุรี </option>
                                          <option value="เชียงใหม่">เชียงใหม่ </option>
                                          <option value="เชียงราย">เชียงราย </option>
                                          <option value="ตรัง">ตรัง </option>
                                          <option value="ตราด">ตราด </option>
                                          <option value="ตาก">ตาก </option>
                                          <option value="นครนายก">นครนายก </option>
                                          <option value="นครปฐม">นครปฐม </option>
                                          <option value="นครพนม">นครพนม </option>
                                          <option value="นครราชสีมา">นครราชสีมา </option>
                                          <option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
                                          <option value="นครสวรรค์">นครสวรรค์ </option>
                                          <option value="นราธิวาส">นราธิวาส </option>
                                          <option value="น่าน">น่าน </option>
                                          <option value="นนทบุรี">นนทบุรี </option>
                                          <option value="บึงกาฬ">บึงกาฬ</option>
                                          <option value="บุรีรัมย์">บุรีรัมย์</option>
                                          <option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
                                          <option value="ปทุมธานี">ปทุมธานี </option>
                                          <option value="ปราจีนบุรี">ปราจีนบุรี </option>
                                          <option value="ปัตตานี">ปัตตานี </option>
                                          <option value="พะเยา">พะเยา </option>
                                          <option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
                                          <option value="พังงา">พังงา </option>
                                          <option value="พิจิตร">พิจิตร </option>
                                          <option value="พิษณุโลก">พิษณุโลก </option>
                                          <option value="เพชรบุรี">เพชรบุรี </option>
                                          <option value="เพชรบูรณ์">เพชรบูรณ์ </option>
                                          <option value="แพร่">แพร่ </option>
                                          <option value="พัทลุง">พัทลุง </option>
                                          <option value="ภูเก็ต">ภูเก็ต </option>
                                          <option value="มหาสารคาม">มหาสารคาม </option>
                                          <option value="มุกดาหาร">มุกดาหาร </option>
                                          <option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
                                          <option value="ยโสธร">ยโสธร </option>
                                          <option value="ยะลา">ยะลา </option>
                                          <option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
                                          <option value="ระนอง">ระนอง </option>
                                          <option value="ระยอง">ระยอง </option>
                                          <option value="ราชบุรี">ราชบุรี</option>
                                          <option value="ลพบุรี">ลพบุรี </option>
                                          <option value="ลำปาง">ลำปาง </option>
                                          <option value="ลำพูน">ลำพูน </option>
                                          <option value="เลย">เลย </option>
                                          <option value="ศรีสะเกษ">ศรีสะเกษ</option>
                                          <option value="สกลนคร">สกลนคร</option>
                                          <option value="สงขลา">สงขลา </option>
                                          <option value="สมุทรสาคร">สมุทรสาคร </option>
                                          <option value="สมุทรปราการ">สมุทรปราการ </option>
                                          <option value="สมุทรสงคราม">สมุทรสงคราม </option>
                                          <option value="สระแก้ว">สระแก้ว </option>
                                          <option value="สระบุรี">สระบุรี </option>
                                          <option value="สิงห์บุรี">สิงห์บุรี </option>
                                          <option value="สุโขทัย">สุโขทัย </option>
                                          <option value="สุพรรณบุรี">สุพรรณบุรี </option>
                                          <option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
                                          <option value="สุรินทร์">สุรินทร์ </option>
                                          <option value="สตูล">สตูล </option>
                                          <option value="หนองคาย">หนองคาย </option>
                                          <option value="หนองบัวลำภู">หนองบัวลำภู </option>
                                          <option value="อำนาจเจริญ">อำนาจเจริญ </option>
                                          <option value="อุดรธานี">อุดรธานี </option>
                                          <option value="อุตรดิตถ์">อุตรดิตถ์ </option>
                                          <option value="อุทัยธานี">อุทัยธานี </option>
                                          <option value="อุบลราชธานี">อุบลราชธานี</option>
                                          <option value="อ่างทอง">อ่างทอง </option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label class="control-label">เบอร์โทรติดต่อ</label>
                                        <input type="text" class="form-control" name="phone" value=""/>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="control-label">Facebook</label>
                                        <input type="text" class="form-control" name="fbook" value=""/>
                                    </div> -->


                                    <div class="form-group col-xs-12">
                                      <h3 class="block">ข้อมูลการศึกษา</h3>
                                      <label class="control-label"> หลักสูตรที่จบการศึกษา </label>
                                      <div class="radio-list">
                                        <label><input type='radio' name='alumniType' value='CPE' data-title='CPE' /> CPE </label>
                                        <label><input type='radio' name='alumniType' value='ISNE' data-title='ISNE' /> ISNE </label>
                                        <label><input type='radio' name='alumniType' value='MCPE' data-title='MCPE' /> MCPE </label>
                                        <label><input type='radio' name='alumniType' value='PHDCPE' data-title='PHDCPE' /> PHDCPE </label>
                                      </div>

                                      <div id="form_gender_error"> </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label class="control-label">รุ่น</label>
                                        <input type="text" class="form-control" name="gen" value="" /> </div>

                                    <div class="form-group col-md-6">
                                      <label class="control-label">อาจารย์ที่ปรึกษาโครงงาน
                                        <span class="required"> * </span>
                                      </label>
                                      <div>
                                        <select name="advisor" id="city_list" class="form-control" >
                                          <option value="" selected> เลือกอาจารย์ที่ปรึกษาโครงงาน </option>
                                          <?php
                                          foreach ($advisor as $key => $value) {
                                            echo "<option value='$key'>".$value->name."</option>";
                                          }
                                          ?>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                      <label class="control-label">ปีที่เข้าศึกษา(พ.ศ.)
                                        <span class="required"> * </span>
                                      </label>
                                      <div>
                                        <select name="entryYear" id="work_list" class="form-control"  value="{{entryYear}}">
                                          {% for year in selyear %}
                                            <option value="{{year}}"> {{year}} </option>
                                          {% endfor %}
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                      <label class="control-label">ปีที่จบการศึกษา(พ.ศ.)
                                        <span class="required"> * </span>
                                      </label>
                                      <div>
                                        <select name="graduateYear" id="work_list" class="form-control" >
                                          {% for year in selyear %}
                                            <option value="{{year+4}}"> {{year + 4}} </option>
                                          {% endfor %}
                                        </select>
                                      </div>
                                    </div>



                                    <div class="col-xs-12 margiv-top-10">
                                        <button type="submit" class="btn green uppercase">บันทึก</button>
                                        <a href="javascript:;" class="btn default"> ยกเลิก </a>
                                    </div>
                                </form>
                            </div>
                            <!-- <div id="tab_2-2" class="tab-pane">
                                <form action="editImgProfile" role="form" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> เลือกรูปภาพ </span>
                                                    <span class="fileinput-exists"> เปลี่ยนรูปภาพ </span>
                                                    <input type="file" name="imgProfile[]" ng-file-select="onFileSelect($files)"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> นำรูปภาพออก </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="margin-top-10">
                                        <a href="javascript:;" class="btn green" ng-click="uploadFile()"> ตกลง </a>
                                        <a href="javascript:;" class="btn default"> ยกเลิก </a>
                                    </div>
                                </form>
                            </div> -->
                            <!-- <div id="tab_3-3" class="tab-pane">
                                <form action="resetPasswoed" method="post">
                                    <div class="form-group">
                                      <label class="control-label">รหัสนักศึกษา</label>
                                      <input type="number" class="form-control" value="{{studentId}}" readonly/> </div>
                                    <div class="form-group">
                                        <label class="control-label">แก้ไขรหัสผ่าน</label>
                                        <input id="password" type="password" class="form-control" name="password" mg-model="password" onkeyup="checkedPass()"/> </div>
                                    <div class="form-group">
                                        <label class="control-label">ยืนยันรหัสผ่าน</label>
                                        <input id="repassword" type="password" class="form-control" name="repassword" mg-model="repassword" onkeyup="checkedPass()"/> </div>
                                    <div class="form-group">
                                        <p id="checkedPassText" style="color:red;"></p>
                                    </div>

                                    <div class="margin-top-10">
                                        <button id="submitReset" type="submit" class="btn green uppercase" >บันทึก</button>
                                        <a href="javascript:;" class="btn default"> ยกเลิก </a>
                                    </div>
                                </form>
                            </div> -->
                            <div id="tab_4-4" class="tab-pane">
                                <form action="#">
                                  <table class="table table-striped table-bordered table-advance table-hover">
                                      <thead>
                                          <tr>
                                              <th>
                                                  <i class="fa fa-briefcase"></i> บริษัท </th>
                                              <th class="hidden-xs">
                                                  <i class="fa fa-question"></i> ตำแหน่ง </th>
                                              <th>
                                                  <i class="fa fa-bookmark"></i> ปีที่เข้าทำงาน </th>
                                              <th>
                                                  <i class="fa fa-bookmark"></i>  </th>
                                          </tr>
                                      </thead>

                                      <tbody>
                                          <?php
                                            foreach ($userJob as $key => $value) {
                                              echo "<tr>";

                                                echo "<td><a href='javascript:;'>".$value['companyName']."</a></td>";
                                                echo "<td>";
                                                foreach ($value['jobs'] as $subKey => $subValue) {
                                                    if($subKey+1 == count($value['jobs']))
                                                      echo $subValue;
                                                    else
                                                      echo $subValue." ,";
                                                }
                                                echo "</td>";
                                                echo "<td>".$value['startWork']." - ".$value['endWork']."</td>";
                                                echo "<td>"."<a href='deleteCompany?key=".$value['companyProfileKey']."' class='btn green'> ลบ </a>"."</td>";

                                              echo "</tr>";
                                            }
                                          ?>
                                      </tbody>
                                  </table>
                                    <!--end profile-settings-->
                                    <div class="margin-top-10">
                                        <a href="addCompany" class="btn green"> เพิ่ม </a>
                                        <a href="javascript:;" class="btn default"> ยกเลิก </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>
            </div>
        </div>
    </div>
</div>
