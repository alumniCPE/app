<div class="portlet light" ng-app="addCompany" ng-controller="addcompanyController">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase">{{topic_header}}</span>
    </div>
</div>

<div class="portlet-body">
    <!-- BEGIN FORM-->
    <form action="editedEducation" class="form-horizontal" id="form_sample_1" method="post">
        <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">ระดับการศึกษา</label>
              <div class="col-md-4">
              <select name="course" id="work_list" class="form-control" selected>
                  <option hidden> เลือกระดับการศึกษา </option>
                  <?php 
                    foreach ($courseOption as $key => $value) {
                      echo $value;
                    }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">หลักสูตร/สาขา</label>
                <div class="col-md-4">
                    <input type="text" class="listCompany form-control" rows="3" name="major" value="{{major}}">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">ปีที่เริ่มการศึกษา (พ.ศ.)</label>
              <div class="col-md-4">
              <select name="startStudy" id="work_list" class="form-control" >
                  <option value="{{startStudy}}"> {{startStudy}} </option>
                  {% for year in yearlist %}
                    <option value="{{year}}">{{year}}</option>
                  {% endfor %}
                  <option value="ปัจจุบัน"> ปัจจุบัน </option>

              </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">ปีที่จบการศึกษา (พ.ศ.)</label>
              <div class="col-md-4">
              <select name="endStudy" id="work_list" class="form-control" >
                <option value="{{endStudy}}"> {{endStudy}} </option>
                {% for year in yearlist %}
                  <option value="{{year + 4}}">{{year + 4}}</option>
                {% endfor %}
                <option value="ปัจจุบัน"> ปัจจุบัน </option>

              </select>
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">สถานศึกษา</label>
                <div class="col-md-4">
                    <input type="text" class="listCompany form-control" rows="3" name="university" value="{{university}}">
                </div>
            </div>

            <div class="form-group">
            <label class="control-label col-md-3"></label>
              <div class="col-md-4">
                 <p id="checkedParameterText" style="color:red;"></p>
              </div>
            </div>

            <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button id="addedEducation" type="submit" class="btn green">แก้ไข</button>
                    <button type="reset" class="btn default">รีเซ็ต</button>
                </div>
            </div>
            </div>
        </div>
    </form>
    <!-- END FORM-->
</div>
</div>
