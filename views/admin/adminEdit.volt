<div class="portlet light">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase">{{topic_header}}</span>
    </div>
</div>

<div class="portlet-body">
    <!-- BEGIN FORM-->
    <form action="editedAdmin" class="form-horizontal" id="form_sample_1" method="post">
        <div class="form-body">
            <div class="form-group">
                <label class="control-label col-md-3">ชื่อ</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" rows="3" name="fname" value="{{fname}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">นามสกุล</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" rows="3" name="lname" value="{{lname}}">
                </div>
            </div>

            <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button id="addCompanyButton" type="submit" class="btn green">แก้ไข</button>
                    <button onclick="history.back()" class="btn default"> ยกเลิก </button>
                </div>
            </div>
            </div>
        </div>
    </form>
    <!-- END FORM-->
</div>
</div>
