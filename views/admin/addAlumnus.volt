<div class="portlet light">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase">{{topic_header}}</span>
    </div>
</div>

<div class="portlet-body">
    <!-- BEGIN FORM-->
    <form action="addedAlumnus" class="form-horizontal" id="form_sample_1" method="post" enctype="multipart/form-data">
        <div class="form-body">
            <div class="form-group">
                <label class="control-label col-md-3">โปรดเลือกไฟล์เพื่ออัพโหลด เฉพาะไฟล์ .csv เท่านั้น </label>
                <div class="col-md-3">
                    <input type="file"  name="file" >  
                </div> <a href="https://firebasestorage.googleapis.com/v0/b/alumnieng.appspot.com/o/Files%2Falumni_cpe_test01.csv?alt=media&token=ea474d42-13ea-4960-a6ab-a4021d43270f">คลิกเพื่อดูตัวอย่างไฟล์</a>

            </div>
            <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button id="" type="submit" class="btn green">เพิ่ม</button>
                </div>
            </div>
            </div>
        </div>
    </form>
    <!-- END FORM-->
</div>                                                
</div>
