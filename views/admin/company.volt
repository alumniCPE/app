<div class="portlet light ">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-briefcase font-green"></i>
        <span class="caption-subject bold uppercase">{{topic_header}}</span>
    </div>
</div>

<div class="portlet-body">
    <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="sample_2">
        <thead>
            <tr>
                {#class="name class="id" class="group" class="company" class="fieldgroup" class="address" class="email" class="fb" class="tel"#}
                {% for attr in tableattr %}
                  <th> {{ attr }} </th>
                {% endfor %}
            </tr>
        </thead>
        <tbody>
        <?php
            foreach ($data as $key => $value) {
                echo "<tr>";
                echo "<th></th>";
                echo "<td class='col-md-3'><a href='companyprofile?id=".$value['id']."'>".$value['name']."</a></td>";
                echo "<td>".$value['city']."</td>";
                echo "<td class='col-md-2'>";
                foreach ($value['work'] as $subKey => $subValue) {
                    // echo count($value['companyWorkJob']);
                    if($subKey+1 == count($value['work']))
                    {
                        echo $subValue['name'];
                    }else
                    {
                        echo $subValue['name']." ,";
                    }
                }
                echo "</td>";
                echo "<td class='col-md-2'><a>".$value['website']."</a></td>";
                echo "<td>".$value['phone']."</td>";
                echo "<td>";
                  echo "<a href='companyedit?id=".$value['id']."'><button type='button' class='btn yellow btn-outline' alt='แก้ไข'>
                  <span class='glyphicon glyphicon-wrench'></span></button></a>
                  <a href='companydelete?id=".$value['id']."'><button type='button' class='btn red btn-outline' alt='ลบ'>
                  <span class='glyphicon glyphicon-trash'></span></button></a>";
                echo "</td>";
                echo "</tr>";
                # code...
            }

            ?>
        </tbody>
    </table>
</div>
</div>
