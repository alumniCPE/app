<div class="portlet light ">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase">{{topic_header}}</span>
    </div>
</div>

<div class="portlet-body">
  <form action="search" class="form-horizontal" id="form_sample_1" method="get" >

  <div class="form-group">

    <label class="control-label col-md-2">หลักสูตร</label>
    <div class="col-md-3">
      <select name="type" id="city_list" class="form-control" >
        <option value=""> ทุกหลักสูตร </option>
        <option value="CPE" >CPE</option>
        <option value="ISNE" >ISNE</option>
        <option value="MCPE" >MCPE</option>
        <option value="PHDCPE" >PHDCPE</option>
      </select>
    </div>

    <label class="control-label col-md-2">ปีการศึกษา *</label>
      <div class="col-md-2">
         <select name="yearStart" id="city_list" class="form-control" >
          <?php
            for ($i=0; $i < count($yearForSearch); $i++) {

              if($i == $yearStudent)
                echo "<option value='".$yearForSearch[$i]."' selected>25".$yearForSearch[$i]."</option>";
              else
                echo "<option value='".$yearForSearch[$i]."'>25".$yearForSearch[$i]."</option>";
            }

          ?>
          </select>
      </div>

  </div>

  <div class="form-group">
    <label class="control-label col-md-2">อาจารย์ที่ปรึกษาโครงงาน</label>
     <div class="col-md-3">
       <select name="advisor" id="city_list" class="form-control" >
         <option value="{{advisor}}" selected>{{advisorName}}</option>
         <?php
         foreach ($advisorList as $key => $value) {
           echo "<option value='$key'>".$value->name."</option>";
         }
         ?>
       </select>
      </div>

    <label class="control-label col-md-2">เกียร์</label>
    <div class="col-md-2">
        <input type="text" class="form-control" value="{{gear}}" name="gear" placeholder="เกียร์">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-2">ชื่อ-นามสกุล</label>
     <div class="col-md-3">
        <input class="form-control" type="text" value="{{stdname}}" name="stdname" placeholder="กรอกชื่อหรือนามสกุล">
      </div>

    <label class="control-label col-md-2">รหัสนักศึกษา</label>
      <div class="col-md-2">
        <input class="form-control" type="text" value="{{stdcode}}" name="stdcode" placeholder="กรอกรหัสนักศึกษา">
      </div>

  </div>

  <div class="form-group">
    <label class="control-label col-md-2">ประกาศนียบัตร</label>
     <div class="col-md-3">
        <input class="form-control" type="text" value="{{certificate}}" name="certificate" placeholder="กรอกชื่อประกาศนียบัตร">
      </div>

  </div>

  <div class="form-group">
  <div class="col-md-11"></div>
  <div class="col-md-1">
    <button id="search" type="submit" class="btn green" name="search">ค้นหา</button>
  </div>
  </div>

</div>
</div>

<div class="portlet light ">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase">ผลการค้นหา</span>
    </div>
</div>
<div class="portlet-body">
 <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="sample_2">
        <thead>
                      <tr>
                          {#class="name class="id" class="group" class="company" class="fieldgroup" class="address" class="email" class="fb" class="tel"#}
                          {% for attr in tableattr %}
                              {% if attr !== "ประเภทของงาน" %}
                                <th class=" "> {{ attr }} </th>
                              {% endif  %}
                              {% if attr === "ประเภทของงาน" %}
                                <th class="sorting_asc"> {{ attr }} </th>
                              {% endif  %}
                          {% endfor %}
                      </tr>
                  </thead>
                  <tbody>

                  <?php
                  foreach ($userDatasView as $key => $value) {
                      if($value['advisor'] == null)
                      {
                        echo "<tr>";
                          echo "<td></td>";
                          echo "<td style='text-align: center;'>".$value['studentId']."</td>";
                          echo "<td>".$value['fullname']."</td>";
                          echo "<td style='text-align: center;'> - </td>";
                          echo "<td style='text-align: center;'>".$value['engGear']."</td>";
                          echo "<td style='text-align: center;'> - </td>";
                          echo "<td style='text-align: center;'> - </td>";
                          echo "<td style='text-align: center;'> - </td>";
                      echo "</tr>";
                      }else{
                        echo "<tr>";
                          echo "<td></td>";
                          echo "<td style='text-align: center;'>".$value['studentId']."</td>";
                          echo "<td><a href='profile?studentId=".$value['studentId']."'>".$value['fullname']."</a></td>";
                          echo "<td style='text-align: center;'>".$value['nickname']."</td>";
                          //echo "<td style='text-align: center;'><a href='search?gear=".$value['engGear']."'>".$value['engGear']."</a></td>";
                          echo "<td style='text-align: center;'>".$value['engGear']."</td>";
                          //echo "<td><a href='search?advisor=".$value['advisor']."'>".$value['advisorName']."</a></td>";
                          echo "<td style='text-align: center;'><a href='companyprofile?id=".$value['companyWorkId']."'>".$value['companyWorkName']."</a></td>";
                          echo "<td>".$value['jobs']."</td>";
                          echo "<td style='text-align: center;'>
                           <a href='http://www.facebook.com/".$value['facebookUid']."' target='_blank'><button type='button' class='btn green btn-outline' title='Facebook'><i class='fa fa-facebook'></button></a></td>";
                        echo "</tr>";
                      }
                      
                  }
                  ?>
                  </tbody>
    </table>
</div>
</div>
