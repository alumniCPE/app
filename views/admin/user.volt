<div class="portlet light ">
<div class="portlet-title">
    <div class="caption font-green">
        <i class="icon-graduation font-green"></i>
        <span class="caption-subject bold uppercase">{{topic_header}}</span>
    </div>
</div>

<div class="portlet-body">
    <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="sample_2">
        <thead>
            <tr>
                {#class="name class="id" class="group" class="company" class="fieldgroup" class="address" class="email" class="fb" class="tel"#}
                {% for attr in tableattr %}
                  <td> {{ attr }} </td>
                {% endfor %}
            </tr>
        </thead>
        <tbody>
        <?php
        foreach ($userDatasView as $key => $value) {
            echo "<tr>";
                echo "<th></th>";
                echo "<td>".$value['studentId']."</td>";
                echo "<td><a href='userprofile?studentId=".$value['studentId']."'>".$value['fullname']."</a></td>";
                //echo "<td><a href='search?gear=".$value['engGear']."'>#".$value['engGear']."</a></td>";
                //echo "<td><a href='search?advisor=".$value['advisor']."'>".$value['advisorName']."</a></td>";
                echo "<td>#".$value['engGear']."</td>";
                echo "<td>".$value['advisorName']."</td>";
                echo "<td><a href='companyprofile?id=".$value['companyWorkId']."'>".$value['companyWorkName']."</a></td>";
                echo "<td>".$value['jobs']."</td>";
                echo "<td style='text-align: center;'>
                 <a href='http://www.facebook.com/".$value['facebookUid']."' target='_blank'><button type='button' class='btn green btn-outline' title='Facebook'><i class='fa fa-facebook'></button></a></td>";
                echo "<td>
                  <a href='userEdit?studentId=".$value['studentId']."'><button type='button' class='btn yellow btn-outline' title='แก้ไข'>
                  <span class='glyphicon glyphicon-wrench'></span></button></a>
                  <a href='userDelete?studentId=".$value['studentId']."'><button type='button' class='btn red btn-outline' title='ลบ'>
                  <span class='glyphicon glyphicon-trash'></span></button></a>";
                  if($value['disable'])
                  {
                    echo "<a href='userDisable?studentId=".$value['studentId']."'><button type='button' class='btn dark btn-outline' title='คลิกเพื่อปิดบัญชี'>
                        <span class='glyphicon glyphicon-remove'></span></button></a>";
                  }else
                  {
                    echo "<a href='userDisable?studentId=".$value['studentId']."'><button type='button' class='btn green btn-outline' title='คลิกเพื่อเปิดบัญชี'>
                        <span class='glyphicon glyphicon-ok'></span></button></a>";
                  }
                  echo "<a href='userReset?studentId=".$value['studentId']."'><button type='button' class='btn green btn-outline' title='รีเซ็ต'>รีเซ็ต
                  </button></a>";
                echo "</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</div>
</div>
